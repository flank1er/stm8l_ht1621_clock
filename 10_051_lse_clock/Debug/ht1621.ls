   1                     ; C Compiler for STM8 (COSMIC Software)
   2                     ; Parser V4.12.6 - 16 Dec 2021
   3                     ; Generator (Limited) V4.5.4 - 16 Dec 2021
5087                     	bsct
5088  0000               L7643_digits:
5089  0000 7d            	dc.b	125
5090  0001 60            	dc.b	96
5091  0002 3e            	dc.b	62
5092  0003 7a            	dc.b	122
5093  0004 63            	dc.b	99
5094  0005 5b            	dc.b	91
5095  0006 5f            	dc.b	95
5096  0007 70            	dc.b	112
5097  0008 7f            	dc.b	127
5098  0009 7b            	dc.b	123
5099  000a               _menu:
5100  000a 00            	dc.b	0
5101  000b               _count:
5102  000b 0000          	dc.w	0
5103  000d               _offset:
5104  000d 0000          	dc.w	0
5134                     ; 32 void ht1621_init(void) {
5136                     	switch	.text
5137  0000               _ht1621_init:
5141                     ; 33     ht1621_send_cmd(BIAS);
5143  0000 a652          	ld	a,#82
5144  0002 cd00f6        	call	_ht1621_send_cmd
5146                     ; 34     ht1621_send_cmd(RC256);
5148  0005 a630          	ld	a,#48
5149  0007 cd00f6        	call	_ht1621_send_cmd
5151                     ; 35     ht1621_send_cmd(SYSDIS);
5153  000a 4f            	clr	a
5154  000b cd00f6        	call	_ht1621_send_cmd
5156                     ; 36     ht1621_send_cmd(WDTDIS1);
5158  000e a60a          	ld	a,#10
5159  0010 cd00f6        	call	_ht1621_send_cmd
5161                     ; 37     ht1621_send_cmd(SYSEN);
5163  0013 a602          	ld	a,#2
5164  0015 cd00f6        	call	_ht1621_send_cmd
5166                     ; 38     ht1621_send_cmd(LCDON);
5168  0018 a606          	ld	a,#6
5169  001a cd00f6        	call	_ht1621_send_cmd
5171                     ; 39 }
5174  001d 81            	ret
5198                     ; 41 void ht1621_off() {
5199                     	switch	.text
5200  001e               _ht1621_off:
5204                     ; 42 	ht1621_send_cmd(LCDOFF);
5206  001e a604          	ld	a,#4
5207  0020 cd00f6        	call	_ht1621_send_cmd
5209                     ; 43 }
5212  0023 81            	ret
5247                     ; 45 void ht1621_clear(void) {
5248                     	switch	.text
5249  0024               _ht1621_clear:
5251  0024 88            	push	a
5252       00000001      OFST:	set	1
5255                     ; 47 	for(i=0;i<16;i++) {
5257  0025 0f01          	clr	(OFST+0,sp)
5259  0027               L5353:
5260                     ; 48 		ht1621_send_data((i<<1),0x0);
5262  0027 7b01          	ld	a,(OFST+0,sp)
5263  0029 48            	sll	a
5264  002a 5f            	clrw	x
5265  002b 95            	ld	xh,a
5266  002c cd00d2        	call	_ht1621_send_data
5268                     ; 47 	for(i=0;i<16;i++) {
5270  002f 0c01          	inc	(OFST+0,sp)
5274  0031 7b01          	ld	a,(OFST+0,sp)
5275  0033 a110          	cp	a,#16
5276  0035 25f0          	jrult	L5353
5277                     ; 50 }
5280  0037 84            	pop	a
5281  0038 81            	ret
5346                     ; 104 static void ht1621_time(uint16_t t, uint8_t flag) {
5347                     .FLASH_CODE:	section	.text
5348  0000               L3453_ht1621_time:
5350  0000 89            	pushw	x
5351  0001 89            	pushw	x
5352       00000002      OFST:	set	2
5355                     ; 106   d=(uint8_t)(t%60);
5357  0002 a63c          	ld	a,#60
5358  0004 62            	div	x,a
5359  0005 5f            	clrw	x
5360  0006 97            	ld	xl,a
5361  0007 9f            	ld	a,xl
5362  0008 6b01          	ld	(OFST-1,sp),a
5364                     ; 107   z=(uint8_t)(d%10);
5366  000a 7b01          	ld	a,(OFST-1,sp)
5367  000c 5f            	clrw	x
5368  000d 97            	ld	xl,a
5369  000e a60a          	ld	a,#10
5370  0010 62            	div	x,a
5371  0011 5f            	clrw	x
5372  0012 97            	ld	xl,a
5373  0013 9f            	ld	a,xl
5374  0014 6b02          	ld	(OFST+0,sp),a
5376                     ; 109   PB_ODR &= ~(1<<CS);
5378  0016 72195005      	bres	_PB_ODR,#4
5379                     ; 110   ht1621_send(0xa0, 3);
5381  001a aea003        	ldw	x,#40963
5382  001d cd010e        	call	_ht1621_send
5384                     ; 111   ht1621_send(0, 6);
5386  0020 ae0006        	ldw	x,#6
5387  0023 cd010e        	call	_ht1621_send
5389                     ; 112   t=t/60;
5391  0026 1e03          	ldw	x,(OFST+1,sp)
5392  0028 a63c          	ld	a,#60
5393  002a 62            	div	x,a
5394  002b 1f03          	ldw	(OFST+1,sp),x
5395                     ; 113   if (menu == (uint8_t)0x1 && !flag) {
5397  002d b60a          	ld	a,_menu
5398  002f a101          	cp	a,#1
5399  0031 2612          	jrne	L7753
5401  0033 0d07          	tnz	(OFST+5,sp)
5402  0035 260e          	jrne	L7753
5403                     ; 114 	ht1621_send(0, 8);
5405  0037 ae0008        	ldw	x,#8
5406  003a cd010e        	call	_ht1621_send
5408                     ; 115 	ht1621_send(0, 8);	
5410  003d ae0008        	ldw	x,#8
5411  0040 cd010e        	call	_ht1621_send
5414  0043 2024          	jra	L1063
5415  0045               L7753:
5416                     ; 117 	ht1621_send(digits[z], 8);
5418  0045 7b02          	ld	a,(OFST+0,sp)
5419  0047 5f            	clrw	x
5420  0048 97            	ld	xl,a
5421  0049 e600          	ld	a,(L7643_digits,x)
5422  004b ae0008        	ldw	x,#8
5423  004e 95            	ld	xh,a
5424  004f cd010e        	call	_ht1621_send
5426                     ; 118 	z=(uint8_t)(d/10);
5428  0052 7b01          	ld	a,(OFST-1,sp)
5429  0054 5f            	clrw	x
5430  0055 97            	ld	xl,a
5431  0056 a60a          	ld	a,#10
5432  0058 62            	div	x,a
5433  0059 9f            	ld	a,xl
5434  005a 6b02          	ld	(OFST+0,sp),a
5436                     ; 119 	ht1621_send(digits[z], 8);
5438  005c 7b02          	ld	a,(OFST+0,sp)
5439  005e 5f            	clrw	x
5440  005f 97            	ld	xl,a
5441  0060 e600          	ld	a,(L7643_digits,x)
5442  0062 ae0008        	ldw	x,#8
5443  0065 95            	ld	xh,a
5444  0066 cd010e        	call	_ht1621_send
5446  0069               L1063:
5447                     ; 121   if (flag || menu)
5449  0069 0d07          	tnz	(OFST+5,sp)
5450  006b 2604          	jrne	L5063
5452  006d 3d0a          	tnz	_menu
5453  006f 271e          	jreq	L3063
5454  0071               L5063:
5455                     ; 122 	ht1621_send(MINUS, 8);
5457  0071 ae0208        	ldw	x,#520
5458  0074 cd010e        	call	_ht1621_send
5461  0077               L7063:
5462                     ; 126   if (menu == (uint8_t)0x2 && !flag) {
5464  0077 b60a          	ld	a,_menu
5465  0079 a102          	cp	a,#2
5466  007b 2619          	jrne	L1163
5468  007d 0d07          	tnz	(OFST+5,sp)
5469  007f 2615          	jrne	L1163
5470                     ; 127 	ht1621_send(0, 8);
5472  0081 ae0008        	ldw	x,#8
5473  0084 cd010e        	call	_ht1621_send
5475                     ; 128 	ht1621_send(0, 8);	
5477  0087 ae0008        	ldw	x,#8
5478  008a cd010e        	call	_ht1621_send
5481  008d 203c          	jra	L3163
5482  008f               L3063:
5483                     ; 124 	ht1621_send(0,8);
5485  008f ae0008        	ldw	x,#8
5486  0092 ad7a          	call	_ht1621_send
5488  0094 20e1          	jra	L7063
5489  0096               L1163:
5490                     ; 130 	z=(uint8_t)(t%10);
5492  0096 1e03          	ldw	x,(OFST+1,sp)
5493  0098 a60a          	ld	a,#10
5494  009a 62            	div	x,a
5495  009b 5f            	clrw	x
5496  009c 97            	ld	xl,a
5497  009d 9f            	ld	a,xl
5498  009e 6b02          	ld	(OFST+0,sp),a
5500                     ; 131 	ht1621_send(digits[z], 8);
5502  00a0 7b02          	ld	a,(OFST+0,sp)
5503  00a2 5f            	clrw	x
5504  00a3 97            	ld	xl,a
5505  00a4 e600          	ld	a,(L7643_digits,x)
5506  00a6 ae0008        	ldw	x,#8
5507  00a9 95            	ld	xh,a
5508  00aa ad62          	call	_ht1621_send
5510                     ; 132 	z=(uint8_t)(t/10);
5512  00ac 1e03          	ldw	x,(OFST+1,sp)
5513  00ae a60a          	ld	a,#10
5514  00b0 62            	div	x,a
5515  00b1 9f            	ld	a,xl
5516  00b2 6b02          	ld	(OFST+0,sp),a
5518                     ; 133 	if (t>0)
5520  00b4 1e03          	ldw	x,(OFST+1,sp)
5521  00b6 270e          	jreq	L5163
5522                     ; 134 	  ht1621_send(digits[z], 8);
5524  00b8 7b02          	ld	a,(OFST+0,sp)
5525  00ba 5f            	clrw	x
5526  00bb 97            	ld	xl,a
5527  00bc e600          	ld	a,(L7643_digits,x)
5528  00be ae0008        	ldw	x,#8
5529  00c1 95            	ld	xh,a
5530  00c2 ad4a          	call	_ht1621_send
5533  00c4 2005          	jra	L3163
5534  00c6               L5163:
5535                     ; 136 	  ht1621_send(0,8);   
5537  00c6 ae0008        	ldw	x,#8
5538  00c9 ad43          	call	_ht1621_send
5540  00cb               L3163:
5541                     ; 138   PB_ODR |= (1<<CS);
5543  00cb 72185005      	bset	_PB_ODR,#4
5544                     ; 140 }
5547  00cf 5b04          	addw	sp,#4
5548  00d1 81            	ret
5593                     ; 172 void ht1621_send_data(uint8_t adr, uint8_t value) {
5594                     	switch	.FLASH_CODE
5595  00d2               _ht1621_send_data:
5597  00d2 89            	pushw	x
5598       00000000      OFST:	set	0
5601                     ; 173 	adr <<=2;
5603  00d3 0801          	sll	(OFST+1,sp)
5604  00d5 0801          	sll	(OFST+1,sp)
5605                     ; 174     PB_ODR &= ~(1<<CS);
5607  00d7 72195005      	bres	_PB_ODR,#4
5608                     ; 175     ht1621_send(0xa0, 3);
5610  00db aea003        	ldw	x,#40963
5611  00de ad2e          	call	_ht1621_send
5613                     ; 176     ht1621_send(adr, 6);
5615  00e0 7b01          	ld	a,(OFST+1,sp)
5616  00e2 ae0006        	ldw	x,#6
5617  00e5 95            	ld	xh,a
5618  00e6 ad26          	call	_ht1621_send
5620                     ; 177     ht1621_send(value, 8);
5622  00e8 7b02          	ld	a,(OFST+2,sp)
5623  00ea ae0008        	ldw	x,#8
5624  00ed 95            	ld	xh,a
5625  00ee ad1e          	call	_ht1621_send
5627                     ; 178     PB_ODR |=  (1<<CS);
5629  00f0 72185005      	bset	_PB_ODR,#4
5630                     ; 179 }
5633  00f4 85            	popw	x
5634  00f5 81            	ret
5670                     ; 181 void ht1621_send_cmd(uint8_t cmd) {
5671                     	switch	.FLASH_CODE
5672  00f6               _ht1621_send_cmd:
5674  00f6 88            	push	a
5675       00000000      OFST:	set	0
5678                     ; 182 	PB_ODR &= ~(1<<CS);
5680  00f7 72195005      	bres	_PB_ODR,#4
5681                     ; 183 	ht1621_send(0x80,4);
5683  00fb ae8004        	ldw	x,#32772
5684  00fe ad0e          	call	_ht1621_send
5686                     ; 184 	ht1621_send(cmd,8);
5688  0100 7b01          	ld	a,(OFST+1,sp)
5689  0102 ae0008        	ldw	x,#8
5690  0105 95            	ld	xh,a
5691  0106 ad06          	call	_ht1621_send
5693                     ; 185 	PB_ODR |=  (1<<CS);
5695  0108 72185005      	bset	_PB_ODR,#4
5696                     ; 186 }
5699  010c 84            	pop	a
5700  010d 81            	ret
5753                     ; 188 void ht1621_send(uint8_t data, uint8_t len) {
5754                     	switch	.FLASH_CODE
5755  010e               _ht1621_send:
5757  010e 89            	pushw	x
5758  010f 88            	push	a
5759       00000001      OFST:	set	1
5762                     ; 190 	for(i=0;i<len;i++) {
5764  0110 0f01          	clr	(OFST+0,sp)
5767  0112 2021          	jra	L3173
5768  0114               L7073:
5769                     ; 191         PB_ODR=(data & 0x80) ? PB_ODR | (1<<DATA) : PB_ODR & ~(1<<DATA);
5771  0114 7b02          	ld	a,(OFST+1,sp)
5772  0116 a580          	bcp	a,#128
5773  0118 2707          	jreq	L22
5774  011a c65005        	ld	a,_PB_ODR
5775  011d aa40          	or	a,#64
5776  011f 2005          	jra	L42
5777  0121               L22:
5778  0121 c65005        	ld	a,_PB_ODR
5779  0124 a4bf          	and	a,#191
5780  0126               L42:
5781  0126 c75005        	ld	_PB_ODR,a
5782                     ; 192 		PB_ODR &= ~(1<<WR);
5784  0129 721b5005      	bres	_PB_ODR,#5
5785                     ; 193 		PB_ODR |= (1<<WR);
5787  012d 721a5005      	bset	_PB_ODR,#5
5788                     ; 194         data=(data<<1);
5790  0131 0802          	sll	(OFST+1,sp)
5791                     ; 190 	for(i=0;i<len;i++) {
5793  0133 0c01          	inc	(OFST+0,sp)
5795  0135               L3173:
5798  0135 7b01          	ld	a,(OFST+0,sp)
5799  0137 1103          	cp	a,(OFST+2,sp)
5800  0139 25d9          	jrult	L7073
5801                     ; 196 }
5804  013b 5b03          	addw	sp,#3
5805  013d 81            	ret
5915                     ; 199 void main_loop(void) {
5916                     	switch	.FLASH_CODE
5917  013e               _main_loop:
5919  013e 520c          	subw	sp,#12
5920       0000000c      OFST:	set	12
5923                     ; 200 	uint8_t prev=0;
5925  0140 0f04          	clr	(OFST-8,sp)
5927                     ; 201 	uint8_t btn_press=0;
5929                     ; 202 	uint16_t i=40;
5931  0142 ae0028        	ldw	x,#40
5932  0145 1f08          	ldw	(OFST-4,sp),x
5934                     ; 203 	uint8_t btn=0;	
5936                     ; 204     uint16_t tick=0;
5938  0147 5f            	clrw	x
5939  0148 1f06          	ldw	(OFST-6,sp),x
5941                     ; 205 	uint16_t time=1000;
5943  014a ae03e8        	ldw	x,#1000
5944  014d 1f0b          	ldw	(OFST-1,sp),x
5946                     ; 206 	uint8_t blink=0;
5948  014f 0f05          	clr	(OFST-7,sp)
5950                     ; 207 	ht1621_init();	
5952  0151 cd0000        	call	_ht1621_init
5954                     ; 208     ht1621_clear();  
5956  0154 cd0024        	call	_ht1621_clear
5958                     ; 211     CLK_SWCR |= (1<<1);                     // set SWEN flag
5960  0157 721250c9      	bset	_CLK_SWCR,#1
5961                     ; 212     CLK_ECKCR |= (1<<2);                    // set LSEON flag
5963  015b 721450c6      	bset	_CLK_ECKCR,#2
5965  015f               L5773:
5966                     ; 213     while (!(CLK_ECKCR & 0x08));            // wait LSERDY flag
5968  015f c650c6        	ld	a,_CLK_ECKCR
5969  0162 a508          	bcp	a,#8
5970  0164 27f9          	jreq	L5773
5971                     ; 214     CLK_SWR= 0x08;                          // set LSE
5973  0166 350850c8      	mov	_CLK_SWR,#8
5975  016a               L5004:
5976                     ; 215 	while (CLK_SWCR & 0x01);				// wait to reset SWBSY flag
5978  016a c650c9        	ld	a,_CLK_SWCR
5979  016d a501          	bcp	a,#1
5980  016f 26f9          	jrne	L5004
5981                     ; 216     CLK_ICKCR &= ~(1<<0);                   // reset HSION flag
5983  0171 721150c2      	bres	_CLK_ICKCR,#0
5984                     ; 217 	CLK_DIVR = 0;         					// SYSCLK=LSE
5986  0175 725f50c0      	clr	_CLK_DIVR
5987                     ; 219     FLASH_CR1 |= (1<<3);                    // set EEPM flag
5989  0179 72165050      	bset	_FLASH_CR1,#3
5991  017d               L3104:
5992                     ; 220     while (--i > 0);                        // wait after switch off flash memory
5994  017d 1e08          	ldw	x,(OFST-4,sp)
5995  017f 1d0001        	subw	x,#1
5996  0182 1f08          	ldw	(OFST-4,sp),x
5998  0184 26f7          	jrne	L3104
5999                     ; 222     CLK_REGCSR |= (1<<1);                   // set REGOFF flag
6001  0186 721250cf      	bset	_CLK_REGCSR,#1
6002                     ; 225 	TIM4_CR1  |= 0x01;
6004  018a 721052e0      	bset	_TIM4_CR1,#0
6005                     ; 226 	ht1621_time(time,0);
6007  018e 4b00          	push	#0
6008  0190 1e0c          	ldw	x,(OFST+0,sp)
6009  0192 cd0000        	call	L3453_ht1621_time
6011  0195 84            	pop	a
6012  0196               L7104:
6013                     ; 229 	  btn=PB_IDR;
6015  0196 c65006        	ld	a,_PB_IDR
6016  0199 6b0a          	ld	(OFST-2,sp),a
6018                     ; 230 	  btn &= 0x2;
6020  019b 7b0a          	ld	a,(OFST-2,sp)
6021  019d a402          	and	a,#2
6022  019f 6b0a          	ld	(OFST-2,sp),a
6024                     ; 231 	  if (btn == 0) {
6026  01a1 0d0a          	tnz	(OFST-2,sp)
6027  01a3 2637          	jrne	L3204
6028                     ; 232 		menu = (menu<2) ? menu +1 : 0;
6030  01a5 b60a          	ld	a,_menu
6031  01a7 a102          	cp	a,#2
6032  01a9 2405          	jruge	L03
6033  01ab b60a          	ld	a,_menu
6034  01ad 4c            	inc	a
6035  01ae 2001          	jra	L23
6036  01b0               L03:
6037  01b0 4f            	clr	a
6038  01b1               L23:
6039  01b1 b70a          	ld	_menu,a
6040                     ; 233 		TIM2_CNTRL=0;
6042  01b3 725f525d      	clr	_TIM2_CNTRL
6043                     ; 234 		TIM2_CNTRH=0;				
6045  01b7 725f525c      	clr	_TIM2_CNTRH
6046  01bb               L5204:
6047                     ; 237 		  btn=PB_IDR;
6049  01bb c65006        	ld	a,_PB_IDR
6050  01be 6b0a          	ld	(OFST-2,sp),a
6052                     ; 238 		  btn &= 0x2;		  
6054  01c0 7b0a          	ld	a,(OFST-2,sp)
6055  01c2 a402          	and	a,#2
6056  01c4 6b0a          	ld	(OFST-2,sp),a
6058                     ; 239 		  for(i=0; i<300;i++) {
6060  01c6 5f            	clrw	x
6061  01c7 1f08          	ldw	(OFST-4,sp),x
6063  01c9               L3304:
6064                     ; 240 			_asm("nop");
6067  01c9 9d            nop
6069                     ; 239 		  for(i=0; i<300;i++) {
6071  01ca 1e08          	ldw	x,(OFST-4,sp)
6072  01cc 1c0001        	addw	x,#1
6073  01cf 1f08          	ldw	(OFST-4,sp),x
6077  01d1 1e08          	ldw	x,(OFST-4,sp)
6078  01d3 a3012c        	cpw	x,#300
6079  01d6 25f1          	jrult	L3304
6080                     ; 242 		} while (btn == 0);
6082  01d8 0d0a          	tnz	(OFST-2,sp)
6083  01da 27df          	jreq	L5204
6084  01dc               L3204:
6085                     ; 246 	  if (menu) {
6087  01dc 3d0a          	tnz	_menu
6088  01de 2733          	jreq	L1404
6089                     ; 247 		count=(uint16_t)TIM2_CNTRH;
6091  01e0 c6525c        	ld	a,_TIM2_CNTRH
6092  01e3 5f            	clrw	x
6093  01e4 97            	ld	xl,a
6094  01e5 bf0b          	ldw	_count,x
6095                     ; 248 		count = count <<8;
6097  01e7 be0b          	ldw	x,_count
6098  01e9 4f            	clr	a
6099  01ea 02            	rlwa	x,a
6100  01eb bf0b          	ldw	_count,x
6101                     ; 249 		count |=(uint16_t)TIM2_CNTRL;
6103  01ed c6525d        	ld	a,_TIM2_CNTRL
6104  01f0 5f            	clrw	x
6105  01f1 97            	ld	xl,a
6106  01f2 1f01          	ldw	(OFST-11,sp),x
6108  01f4 be0b          	ldw	x,_count
6109  01f6 01            	rrwa	x,a
6110  01f7 1a02          	or	a,(OFST-10,sp)
6111  01f9 01            	rrwa	x,a
6112  01fa 1a01          	or	a,(OFST-11,sp)
6113  01fc 01            	rrwa	x,a
6114  01fd bf0b          	ldw	_count,x
6115                     ; 250 		count = count >>1;
6117  01ff be0b          	ldw	x,_count
6118  0201 57            	sraw	x
6119  0202 bf0b          	ldw	_count,x
6120                     ; 252 		if (count >= 125) {
6122  0204 9c            	rvf
6123  0205 be0b          	ldw	x,_count
6124  0207 a3007d        	cpw	x,#125
6125  020a 2f07          	jrslt	L1404
6126                     ; 253 		  count = (count - 250);
6128  020c be0b          	ldw	x,_count
6129  020e 1d00fa        	subw	x,#250
6130  0211 bf0b          	ldw	_count,x
6131  0213               L1404:
6132                     ; 257 	  if (menu==2 && prev == 1) {
6134  0213 b60a          	ld	a,_menu
6135  0215 a102          	cp	a,#2
6136  0217 260c          	jrne	L5404
6138  0219 7b04          	ld	a,(OFST-8,sp)
6139  021b a101          	cp	a,#1
6140  021d 2606          	jrne	L5404
6141                     ; 258 		time =offset;	
6143  021f be0d          	ldw	x,_offset
6144  0221 1f0b          	ldw	(OFST-1,sp),x
6146                     ; 259 		time=time;
6149  0223 203c          	jra	L7404
6150  0225               L5404:
6151                     ; 260 	  } else if (menu == 0 && prev == 2){
6153  0225 3d0a          	tnz	_menu
6154  0227 261f          	jrne	L1504
6156  0229 7b04          	ld	a,(OFST-8,sp)
6157  022b a102          	cp	a,#2
6158  022d 2619          	jrne	L1504
6159                     ; 261 		TIM4_CR1&=~(0x01);	// disable timer
6161  022f 721152e0      	bres	_TIM4_CR1,#0
6162                     ; 262 		TIM4_PSCR  = 0x0c;
6164  0233 350c52e8      	mov	_TIM4_PSCR,#12
6165                     ; 263 		TIM4_CNTR=0;
6167  0237 725f52e7      	clr	_TIM4_CNTR
6168                     ; 264 		TIM4_CR1 |= (0x01);	// enable timer
6170  023b 721052e0      	bset	_TIM4_CR1,#0
6171                     ; 265 		time =offset;
6173  023f be0d          	ldw	x,_offset
6174  0241 1f0b          	ldw	(OFST-1,sp),x
6176                     ; 266 		tick=0;
6178  0243 5f            	clrw	x
6179  0244 1f06          	ldw	(OFST-6,sp),x
6182  0246 2019          	jra	L7404
6183  0248               L1504:
6184                     ; 267 	  } else if (menu == 1 && prev == 0) {
6186  0248 b60a          	ld	a,_menu
6187  024a a101          	cp	a,#1
6188  024c 2613          	jrne	L7404
6190  024e 0d04          	tnz	(OFST-8,sp)
6191  0250 260f          	jrne	L7404
6192                     ; 268 		TIM4_CR1&=~(0x01);	// disable timer
6194  0252 721152e0      	bres	_TIM4_CR1,#0
6195                     ; 269 		TIM4_PSCR  = 0x0b;
6197  0256 350b52e8      	mov	_TIM4_PSCR,#11
6198                     ; 270 		TIM4_CR1 |= (0x01);	// enable timer
6200  025a 721052e0      	bset	_TIM4_CR1,#0
6201                     ; 271 		tick=0;
6203  025e 5f            	clrw	x
6204  025f 1f06          	ldw	(OFST-6,sp),x
6206  0261               L7404:
6207                     ; 275 	  if (!(++tick%4)) {		// last 1 sec
6209  0261 1e06          	ldw	x,(OFST-6,sp)
6210  0263 1c0001        	addw	x,#1
6211  0266 1f06          	ldw	(OFST-6,sp),x
6213  0268 7b07          	ld	a,(OFST-5,sp)
6214  026a a503          	bcp	a,#3
6215  026c 2703cc02ef    	jrne	L7504
6216                     ; 276 		blink=!blink;
6218  0271 0d05          	tnz	(OFST-7,sp)
6219  0273 2604          	jrne	L43
6220  0275 a601          	ld	a,#1
6221  0277 2001          	jra	L63
6222  0279               L43:
6223  0279 4f            	clr	a
6224  027a               L63:
6225  027a 6b05          	ld	(OFST-7,sp),a
6227                     ; 277 		switch (menu) {
6229  027c b60a          	ld	a,_menu
6231                     ; 293 		  default:
6231                     ; 294 			ht1621_time(time,blink);	
6232  027e 4a            	dec	a
6233  027f 275b          	jreq	L1273
6234  0281 4a            	dec	a
6235  0282 270b          	jreq	L7173
6236  0284               L3273:
6239  0284 7b05          	ld	a,(OFST-7,sp)
6240  0286 88            	push	a
6241  0287 1e0c          	ldw	x,(OFST+0,sp)
6242  0289 cd0000        	call	L3453_ht1621_time
6244  028c 84            	pop	a
6245  028d 2060          	jra	L7504
6246  028f               L7173:
6247                     ; 278 		  case 2:
6247                     ; 279 		    offset = time/60;
6249  028f 1e0b          	ldw	x,(OFST-1,sp)
6250  0291 a63c          	ld	a,#60
6251  0293 62            	div	x,a
6252  0294 bf0d          	ldw	_offset,x
6253                     ; 280 			offset += count;
6255  0296 be0d          	ldw	x,_offset
6256  0298 72bb000b      	addw	x,_count
6257  029c bf0d          	ldw	_offset,x
6258                     ; 281 			offset = get_abs(offset);
6260  029e 9c            	rvf
6261  029f be0d          	ldw	x,_offset
6262  02a1 2e05          	jrsge	L04
6263  02a3 be0d          	ldw	x,_offset
6264  02a5 50            	negw	x
6265  02a6 2002          	jra	L24
6266  02a8               L04:
6267  02a8 be0d          	ldw	x,_offset
6268  02aa               L24:
6269  02aa bf0d          	ldw	_offset,x
6270                     ; 282 			offset = (offset<<4)-offset;
6272  02ac be0d          	ldw	x,_offset
6273  02ae 58            	sllw	x
6274  02af 58            	sllw	x
6275  02b0 58            	sllw	x
6276  02b1 58            	sllw	x
6277  02b2 72b0000d      	subw	x,_offset
6278  02b6 bf0d          	ldw	_offset,x
6279                     ; 283 			offset <<=2;
6281  02b8 be0d          	ldw	x,_offset
6282  02ba 58            	sllw	x
6283  02bb 58            	sllw	x
6284  02bc bf0d          	ldw	_offset,x
6285                     ; 284 			offset += (time%60);			
6287  02be 1e0b          	ldw	x,(OFST-1,sp)
6288  02c0 a63c          	ld	a,#60
6289  02c2 62            	div	x,a
6290  02c3 5f            	clrw	x
6291  02c4 97            	ld	xl,a
6292  02c5 1f01          	ldw	(OFST-11,sp),x
6294  02c7 be0d          	ldw	x,_offset
6295  02c9 72fb01        	addw	x,(OFST-11,sp)
6296  02cc bf0d          	ldw	_offset,x
6297                     ; 285 			ht1621_time(offset,blink);
6299  02ce 7b05          	ld	a,(OFST-7,sp)
6300  02d0 88            	push	a
6301  02d1 be0d          	ldw	x,_offset
6302  02d3 cd0000        	call	L3453_ht1621_time
6304  02d6 84            	pop	a
6305                     ; 286 			tick=0;
6307  02d7 5f            	clrw	x
6308  02d8 1f06          	ldw	(OFST-6,sp),x
6310                     ; 287 			break;
6312  02da 2013          	jra	L7504
6313  02dc               L1273:
6314                     ; 288 		  case 1:
6314                     ; 289 			offset = time+count;			
6316  02dc be0b          	ldw	x,_count
6317  02de 72fb0b        	addw	x,(OFST-1,sp)
6318  02e1 bf0d          	ldw	_offset,x
6319                     ; 290 			ht1621_time(offset,blink);
6321  02e3 7b05          	ld	a,(OFST-7,sp)
6322  02e5 88            	push	a
6323  02e6 be0d          	ldw	x,_offset
6324  02e8 cd0000        	call	L3453_ht1621_time
6326  02eb 84            	pop	a
6327                     ; 291 			tick=0;
6329  02ec 5f            	clrw	x
6330  02ed 1f06          	ldw	(OFST-6,sp),x
6332                     ; 292 			break;
6334  02ef               L3604:
6335  02ef               L7504:
6336                     ; 301 	  if (!menu) {	  
6338  02ef 3d0a          	tnz	_menu
6339  02f1 261b          	jrne	L5604
6340                     ; 302 		if (tick == 240) {		// if 1 minute
6342  02f3 1e06          	ldw	x,(OFST-6,sp)
6343  02f5 a300f0        	cpw	x,#240
6344  02f8 260a          	jrne	L7604
6345                     ; 304 		  ++time;
6347  02fa 1e0b          	ldw	x,(OFST-1,sp)
6348  02fc 1c0001        	addw	x,#1
6349  02ff 1f0b          	ldw	(OFST-1,sp),x
6351                     ; 305 		  tick=0;
6353  0301 5f            	clrw	x
6354  0302 1f06          	ldw	(OFST-6,sp),x
6356  0304               L7604:
6357                     ; 307 		if (time == 1440)		// if 24 hour
6359  0304 1e0b          	ldw	x,(OFST-1,sp)
6360  0306 a305a0        	cpw	x,#1440
6361  0309 2603          	jrne	L5604
6362                     ; 308 		  time=0;		  
6364  030b 5f            	clrw	x
6365  030c 1f0b          	ldw	(OFST-1,sp),x
6367  030e               L5604:
6368                     ; 312 	  _asm("wfe");
6371  030e 728f          wfe
6373                     ; 313 	  TIM4_SR1 =0;	
6375  0310 725f52e5      	clr	_TIM4_SR1
6376                     ; 315 	  prev=menu; 
6378  0314 b60a          	ld	a,_menu
6379  0316 6b04          	ld	(OFST-8,sp),a
6382  0318 ac960196      	jpf	L7104
6434                     	xdef	_ht1621_clear
6435                     	xdef	_ht1621_off
6436                     	xdef	_ht1621_init
6437                     	xdef	_ht1621_send_data
6438                     	xdef	_ht1621_send
6439                     	xdef	_ht1621_send_cmd
6440                     	xdef	_offset
6441                     	xdef	_count
6442                     	xdef	_menu
6443                     	xdef	_main_loop
6462                     	end
