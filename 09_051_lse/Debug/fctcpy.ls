   1                     ;	MOVEABLE CODE INITIALIZATION
   2                     ;	Copyright (c) 2006 by COSMIC Software
   3                     ;
   4                     	xdef	__fctcpy
   5                     	xref	__idesc__
   6                     	xref.b	c_x
   8                     ;
   9  0000               __fctcpy:
  10  0000 a41f          	and	a,#$1F			; convert byte code
  11  0002 b702          	ld	c_x+2,a			; keep it
  12  0004 ae0000        	ldw	x,#__idesc__		; start address
  13  0007               ibcl:
  14  0007 e602          	ld	a,(2,x)			; test flag byte
  15  0009 2727          	jreq	cend			; no more segment
  16  000b a560          	bcp	a,#$60			; test for moveable code segment
  17  000d 2604          	jrne	next			; no, continue
  18  000f b102          	cp	a,c_x+2			; compare with argument
  19  0011 2705          	jreq	cseg			; yes, copy it
  20  0013               next:
  21  0013 1c0005        	addw	x,#5			; next descriptor
  22  0016 20ef          	jra	ibcl			; and loop
  23  0018               cseg:
  24  0018 9093          	ldw	y,x			; move end
  25  001a 90ee05        	ldw	y,(5,y)			; address in
  26  001d 90bf00        	ldw	c_x,y			; memory
  27  0020 9093          	ldw	y,x
  28  0022 90ee03        	ldw	y,(3,y)			; ram address
  29  0025 fe            	ldw	x,(x)			; start address
  30  0026               dbcl:
  31  0026 f6            	ld	a,(x)			; transfer
  32  0027 90f7          	ld	(y),a			; byte
  33  0029 905c          	incw	y			; next
  34  002b 5c            	incw	x			; byte
  35  002c b300          	cpw	x,c_x			; source
  36  002e 26f6          	jrne	dbcl			; address
  37  0030 a601          	ld	a,#1			; non zero result
  38  0032               cend:
  39  0032 5f            	clrw	x			; complete
  40  0033 97            	ld	xl,a			; result
  41  0034 81            	ret				; and return
  42                     ;
  43                     	end
