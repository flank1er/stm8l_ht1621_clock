   1                     ; C Compiler for STM8 (COSMIC Software)
   2                     ; Parser V4.12.6 - 16 Dec 2021
   3                     ; Generator (Limited) V4.5.4 - 16 Dec 2021
5087                     	bsct
5088  0000               L7643_digits:
5089  0000 7d            	dc.b	125
5090  0001 60            	dc.b	96
5091  0002 3e            	dc.b	62
5092  0003 7a            	dc.b	122
5093  0004 63            	dc.b	99
5094  0005 5b            	dc.b	91
5095  0006 5f            	dc.b	95
5096  0007 70            	dc.b	112
5097  0008 7f            	dc.b	127
5098  0009 7b            	dc.b	123
5128                     ; 41 void ht1621_init(void) {
5130                     	switch	.text
5131  0000               _ht1621_init:
5135                     ; 42     ht1621_send_cmd(BIAS);
5137  0000 a652          	ld	a,#82
5138  0002 cd0093        	call	_ht1621_send_cmd
5140                     ; 43     ht1621_send_cmd(RC256);
5142  0005 a630          	ld	a,#48
5143  0007 cd0093        	call	_ht1621_send_cmd
5145                     ; 44     ht1621_send_cmd(SYSDIS);
5147  000a 4f            	clr	a
5148  000b cd0093        	call	_ht1621_send_cmd
5150                     ; 45     ht1621_send_cmd(WDTDIS1);
5152  000e a60a          	ld	a,#10
5153  0010 cd0093        	call	_ht1621_send_cmd
5155                     ; 46     ht1621_send_cmd(SYSEN);
5157  0013 a602          	ld	a,#2
5158  0015 cd0093        	call	_ht1621_send_cmd
5160                     ; 47     ht1621_send_cmd(LCDON);
5162  0018 a606          	ld	a,#6
5163  001a cd0093        	call	_ht1621_send_cmd
5165                     ; 48 }
5168  001d 81            	ret
5192                     ; 50 void ht1621_off() {
5193                     	switch	.text
5194  001e               _ht1621_off:
5198                     ; 51 	ht1621_send_cmd(LCDOFF);
5200  001e a604          	ld	a,#4
5201  0020 cd0093        	call	_ht1621_send_cmd
5203                     ; 52 }
5206  0023 81            	ret
5241                     ; 54 void ht1621_clear(void) {
5242                     	switch	.text
5243  0024               _ht1621_clear:
5245  0024 88            	push	a
5246       00000001      OFST:	set	1
5249                     ; 56 	for(i=0;i<16;i++) {
5251  0025 0f01          	clr	(OFST+0,sp)
5253  0027               L5353:
5254                     ; 57 		ht1621_send_data((i<<1),0x0);
5256  0027 7b01          	ld	a,(OFST+0,sp)
5257  0029 48            	sll	a
5258  002a 5f            	clrw	x
5259  002b 95            	ld	xh,a
5260  002c cd006f        	call	_ht1621_send_data
5262                     ; 56 	for(i=0;i<16;i++) {
5264  002f 0c01          	inc	(OFST+0,sp)
5268  0031 7b01          	ld	a,(OFST+0,sp)
5269  0033 a110          	cp	a,#16
5270  0035 25f0          	jrult	L5353
5271                     ; 59 }
5274  0037 84            	pop	a
5275  0038 81            	ret
5340                     ; 137 void ht1621_print(int num) {
5341                     .FLASH_CODE:	section	.text
5342  0000               _ht1621_print:
5344  0000 89            	pushw	x
5345  0001 5204          	subw	sp,#4
5346       00000004      OFST:	set	4
5349                     ; 139     uint16_t value=get_abs(num);
5351  0003 9c            	rvf
5352  0004 a30000        	cpw	x,#0
5353  0007 2e03          	jrsge	L41
5354  0009 50            	negw	x
5355  000a 2000          	jra	L61
5356  000c               L41:
5357  000c               L61:
5358  000c 1f03          	ldw	(OFST-1,sp),x
5360                     ; 140     uint8_t first=1;
5362  000e a601          	ld	a,#1
5363  0010 6b02          	ld	(OFST-2,sp),a
5365                     ; 142 	if (num <= 0) 
5367  0012 9c            	rvf
5368  0013 1e05          	ldw	x,(OFST+1,sp)
5369  0015 2c03          	jrsgt	L5753
5370                     ; 143 	  ht1621_clear();
5372  0017 cd0024        	call	_ht1621_clear
5374  001a               L5753:
5375                     ; 145 	PB_ODR &= ~(1<<CS);
5377  001a 72195005      	bres	_PB_ODR,#4
5378  001e               L7753:
5379                     ; 147         uint8_t rm=value % 10;
5381  001e 1e03          	ldw	x,(OFST-1,sp)
5382  0020 a60a          	ld	a,#10
5383  0022 62            	div	x,a
5384  0023 5f            	clrw	x
5385  0024 97            	ld	xl,a
5386  0025 01            	rrwa	x,a
5387  0026 6b01          	ld	(OFST-3,sp),a
5388  0028 02            	rlwa	x,a
5390                     ; 149 		if (first) {
5392  0029 0d02          	tnz	(OFST-2,sp)
5393  002b 271a          	jreq	L5063
5394                     ; 150 		  ht1621_send(0xa0, 3);
5396  002d aea003        	ldw	x,#40963
5397  0030 ad79          	call	_ht1621_send
5399                     ; 151 		  ht1621_send(0, 6);
5401  0032 ae0006        	ldw	x,#6
5402  0035 ad74          	call	_ht1621_send
5404                     ; 152 		  ht1621_send(digits[rm], 8);		 
5406  0037 7b01          	ld	a,(OFST-3,sp)
5407  0039 5f            	clrw	x
5408  003a 97            	ld	xl,a
5409  003b e600          	ld	a,(L7643_digits,x)
5410  003d ae0008        	ldw	x,#8
5411  0040 95            	ld	xh,a
5412  0041 ad68          	call	_ht1621_send
5414                     ; 153 		  first=0;
5416  0043 0f02          	clr	(OFST-2,sp)
5419  0045 200c          	jra	L7063
5420  0047               L5063:
5421                     ; 155 		  ht1621_send(digits[rm], 8);		 
5423  0047 7b01          	ld	a,(OFST-3,sp)
5424  0049 5f            	clrw	x
5425  004a 97            	ld	xl,a
5426  004b e600          	ld	a,(L7643_digits,x)
5427  004d ae0008        	ldw	x,#8
5428  0050 95            	ld	xh,a
5429  0051 ad58          	call	_ht1621_send
5431  0053               L7063:
5432                     ; 157         value=value/10;
5434  0053 1e03          	ldw	x,(OFST-1,sp)
5435  0055 a60a          	ld	a,#10
5436  0057 62            	div	x,a
5437  0058 1f03          	ldw	(OFST-1,sp),x
5439                     ; 158     } while (value>0);  
5441  005a 1e03          	ldw	x,(OFST-1,sp)
5442  005c 26c0          	jrne	L7753
5443                     ; 160 	if (num < 0) 
5445  005e 9c            	rvf
5446  005f 1e05          	ldw	x,(OFST+1,sp)
5447  0061 2e05          	jrsge	L1163
5448                     ; 161 	  ht1621_send(MINUS,8);  
5450  0063 ae0208        	ldw	x,#520
5451  0066 ad43          	call	_ht1621_send
5453  0068               L1163:
5454                     ; 164 	PB_ODR |=  (1<<CS);
5456  0068 72185005      	bset	_PB_ODR,#4
5457                     ; 165 }
5460  006c 5b06          	addw	sp,#6
5461  006e 81            	ret
5506                     ; 167 void ht1621_send_data(uint8_t adr, uint8_t value) {
5507                     	switch	.FLASH_CODE
5508  006f               _ht1621_send_data:
5510  006f 89            	pushw	x
5511       00000000      OFST:	set	0
5514                     ; 168 	adr <<=2;
5516  0070 0801          	sll	(OFST+1,sp)
5517  0072 0801          	sll	(OFST+1,sp)
5518                     ; 169     PB_ODR &= ~(1<<CS);
5520  0074 72195005      	bres	_PB_ODR,#4
5521                     ; 170     ht1621_send(0xa0, 3);
5523  0078 aea003        	ldw	x,#40963
5524  007b ad2e          	call	_ht1621_send
5526                     ; 171     ht1621_send(adr, 6);
5528  007d 7b01          	ld	a,(OFST+1,sp)
5529  007f ae0006        	ldw	x,#6
5530  0082 95            	ld	xh,a
5531  0083 ad26          	call	_ht1621_send
5533                     ; 172     ht1621_send(value, 8);
5535  0085 7b02          	ld	a,(OFST+2,sp)
5536  0087 ae0008        	ldw	x,#8
5537  008a 95            	ld	xh,a
5538  008b ad1e          	call	_ht1621_send
5540                     ; 173     PB_ODR |=  (1<<CS);
5542  008d 72185005      	bset	_PB_ODR,#4
5543                     ; 174 }
5546  0091 85            	popw	x
5547  0092 81            	ret
5583                     ; 176 void ht1621_send_cmd(uint8_t cmd) {
5584                     	switch	.FLASH_CODE
5585  0093               _ht1621_send_cmd:
5587  0093 88            	push	a
5588       00000000      OFST:	set	0
5591                     ; 177 	PB_ODR &= ~(1<<CS);
5593  0094 72195005      	bres	_PB_ODR,#4
5594                     ; 178 	ht1621_send(0x80,4);
5596  0098 ae8004        	ldw	x,#32772
5597  009b ad0e          	call	_ht1621_send
5599                     ; 179 	ht1621_send(cmd,8);
5601  009d 7b01          	ld	a,(OFST+1,sp)
5602  009f ae0008        	ldw	x,#8
5603  00a2 95            	ld	xh,a
5604  00a3 ad06          	call	_ht1621_send
5606                     ; 180 	PB_ODR |=  (1<<CS);
5608  00a5 72185005      	bset	_PB_ODR,#4
5609                     ; 181 }
5612  00a9 84            	pop	a
5613  00aa 81            	ret
5666                     ; 183 void ht1621_send(uint8_t data, uint8_t len) {
5667                     	switch	.FLASH_CODE
5668  00ab               _ht1621_send:
5670  00ab 89            	pushw	x
5671  00ac 88            	push	a
5672       00000001      OFST:	set	1
5675                     ; 185 	for(i=0;i<len;i++) {
5677  00ad 0f01          	clr	(OFST+0,sp)
5680  00af 2021          	jra	L5073
5681  00b1               L1073:
5682                     ; 186         PB_ODR=(data & 0x80) ? PB_ODR | (1<<DATA) : PB_ODR & ~(1<<DATA);
5684  00b1 7b02          	ld	a,(OFST+1,sp)
5685  00b3 a580          	bcp	a,#128
5686  00b5 2707          	jreq	L62
5687  00b7 c65005        	ld	a,_PB_ODR
5688  00ba aa40          	or	a,#64
5689  00bc 2005          	jra	L03
5690  00be               L62:
5691  00be c65005        	ld	a,_PB_ODR
5692  00c1 a4bf          	and	a,#191
5693  00c3               L03:
5694  00c3 c75005        	ld	_PB_ODR,a
5695                     ; 187 		PB_ODR &= ~(1<<WR);
5697  00c6 721b5005      	bres	_PB_ODR,#5
5698                     ; 188 		PB_ODR |= (1<<WR);
5700  00ca 721a5005      	bset	_PB_ODR,#5
5701                     ; 189         data=(data<<1);
5703  00ce 0802          	sll	(OFST+1,sp)
5704                     ; 185 	for(i=0;i<len;i++) {
5706  00d0 0c01          	inc	(OFST+0,sp)
5708  00d2               L5073:
5711  00d2 7b01          	ld	a,(OFST+0,sp)
5712  00d4 1103          	cp	a,(OFST+2,sp)
5713  00d6 25d9          	jrult	L1073
5714                     ; 191 }
5717  00d8 5b03          	addw	sp,#3
5718  00da 81            	ret
5774                     ; 194 void main_loop(void) {
5775                     	switch	.FLASH_CODE
5776  00db               _main_loop:
5778  00db 5203          	subw	sp,#3
5779       00000003      OFST:	set	3
5782                     ; 195     uint16_t tick=0;
5784  00dd 5f            	clrw	x
5785  00de 1f02          	ldw	(OFST-1,sp),x
5787                     ; 196 	uint8_t i=30;
5789  00e0 a61e          	ld	a,#30
5790  00e2 6b01          	ld	(OFST-2,sp),a
5792                     ; 197 	ht1621_init();	
5794  00e4 cd0000        	call	_ht1621_init
5796                     ; 198     ht1621_clear();  
5798  00e7 cd0024        	call	_ht1621_clear
5800                     ; 199 	ht1621_print(0);
5802  00ea 5f            	clrw	x
5803  00eb cd0000        	call	_ht1621_print
5805                     ; 201     CLK_SWCR |= (1<<1);                     // set SWEN flag
5807  00ee 721250c9      	bset	_CLK_SWCR,#1
5808                     ; 202     CLK_ECKCR |= (1<<2);                    // set LSEON flag
5810  00f2 721450c6      	bset	_CLK_ECKCR,#2
5812  00f6               L5373:
5813                     ; 203     while (!(CLK_ECKCR & 0x08));            // wait LSERDY flag
5815  00f6 c650c6        	ld	a,_CLK_ECKCR
5816  00f9 a508          	bcp	a,#8
5817  00fb 27f9          	jreq	L5373
5818                     ; 204     CLK_SWR= 0x08;                          // set LSE
5820  00fd 350850c8      	mov	_CLK_SWR,#8
5822  0101               L5473:
5823                     ; 205 	while (CLK_SWCR & 0x01);				// wait to reset SWBSY flag
5825  0101 c650c9        	ld	a,_CLK_SWCR
5826  0104 a501          	bcp	a,#1
5827  0106 26f9          	jrne	L5473
5828                     ; 206     CLK_ICKCR &= ~(1<<0);                   // reset HSION flag
5830  0108 721150c2      	bres	_CLK_ICKCR,#0
5831                     ; 207 	CLK_DIVR = 0;         					// SYSCLK=LSE
5833  010c 725f50c0      	clr	_CLK_DIVR
5834                     ; 209     FLASH_CR1 |= (1<<3);                    // set EEPM flag
5836  0110 72165050      	bset	_FLASH_CR1,#3
5838  0114               L3573:
5839                     ; 210     while (--i > 0);                        // wait after switch off flash memory
5841  0114 0a01          	dec	(OFST-2,sp)
5843  0116 26fc          	jrne	L3573
5844                     ; 212     CLK_REGCSR |= (1<<1);                   // set REGOFF flag
5846  0118 721250cf      	bset	_CLK_REGCSR,#1
5847                     ; 215 	TIM4_CR1  |= 0x01;
5849  011c 721052e0      	bset	_TIM4_CR1,#0
5850  0120               L7573:
5851                     ; 217 	  _asm("wfe");
5854  0120 728f          wfe
5856                     ; 218 	  TIM4_SR1 =0;	 
5858  0122 725f52e5      	clr	_TIM4_SR1
5859                     ; 309 	  tick++;
5861  0126 1e02          	ldw	x,(OFST-1,sp)
5862  0128 1c0001        	addw	x,#1
5863  012b 1f02          	ldw	(OFST-1,sp),x
5865                     ; 310 	  ht1621_print(tick);
5867  012d 1e02          	ldw	x,(OFST-1,sp)
5868  012f cd0000        	call	_ht1621_print
5871  0132 20ec          	jra	L7573
5896                     	xdef	_ht1621_off
5897                     	xdef	_ht1621_send_data
5898                     	xdef	_ht1621_send
5899                     	xdef	_ht1621_send_cmd
5900                     	xdef	_ht1621_init
5901                     	xdef	_ht1621_clear
5902                     	xdef	_ht1621_print
5903                     	xdef	_main_loop
5922                     	end
