#include <stdint.h>
#include "iostm8l152x.h"
#include "ht1621.h"

#define num_len 10
#define MINUS	0x02

const uint8_t digits[10] = {0x7d,0x60,0x3e,0x7a,0x63,0x5b,0x5f,0x70,0x7f,0x7b};

void ht1621_send_cmd(uint8_t cmd);
void ht1621_send(uint8_t data, uint8_t len);
void ht1621_send_data(uint8_t adr, uint8_t value);
uint8_t ht1621_char_to_seg_bits(uint8_t ch);



void ht1621_init(void) {
    ht1621_send_cmd(BIAS);
    ht1621_send_cmd(RC256);
    ht1621_send_cmd(SYSDIS);
    ht1621_send_cmd(WDTDIS1);
    ht1621_send_cmd(SYSEN);
    ht1621_send_cmd(LCDON);
}

void ht1621_off() {
	ht1621_send_cmd(LCDOFF);
}

void ht1621_clear(void) {
    char i;
	for(i=0;i<16;i++) {
		ht1621_send_data((i<<1),0x0);
	}
}

void ht1621_print_num(uint16_t num){
	uint8_t pos=0;
    do {
        uint8_t rm=num % 10;
		ht1621_send_digit(pos++,rm);
        num=num/10;
    } while (num>0);
}

void ht1621_send_digit(uint8_t pos,uint8_t digit) {
	ht1621_send_data((pos<<1),digits[digit]);
}

void ht1621_minute(uint16_t t, uint8_t flag) {
  if (flag) {
	ht1621_time(t,flag);
	return;
  }
  
  t=t/60;
  ////////////////////
  PB_ODR &= ~(1<<CS);
  ht1621_send(0xa0, 3);
  ht1621_send(0, 6);
  ht1621_send(0, 8);
  ht1621_send(0, 8);
  ht1621_send(MINUS, 8);
  ht1621_send(digits[(t%10)], 8);
  t=t/10;
  if (t>0)
	ht1621_send(digits[(t)], 8);
  else 
	ht1621_send(0,8);      
  PB_ODR |= (1<<CS);
  //////////////////////
}

void ht1621_hour(uint16_t t, uint8_t flag) {
  uint8_t d;
  d=(uint8_t)(t%60);
  if (flag) {
	ht1621_time(t,flag);
	return;
  }
  ////////////////////
  PB_ODR &= ~(1<<CS);
  ht1621_send(0xa0, 3);
  ht1621_send(0, 6);
  ht1621_send(digits[(d%10)], 8);
  ht1621_send(digits[(d/10)], 8);
  ht1621_send(MINUS, 8);
  ht1621_send(0, 8);
  ht1621_send(0, 8);      
  PB_ODR |= (1<<CS);
  //////////////////////
}

void ht1621_time(uint16_t t, uint8_t flag) {
  uint8_t d;
  d=(uint8_t)(t%60);
  t=t/60;
  ////////////////////
  PB_ODR &= ~(1<<CS);
  ht1621_send(0xa0, 3);
  ht1621_send(0, 6);
  ht1621_send(digits[(d%10)], 8);
  ht1621_send(digits[(d/10)], 8);
  if (flag)
	ht1621_send(MINUS, 8);
  else 
	ht1621_send(0,8);
  ht1621_send(digits[(t%10)], 8);
  t=t/10;
  if (t>0)
	ht1621_send(digits[(t)], 8);
  else 
	ht1621_send(0,8);      
  PB_ODR |= (1<<CS);
  //////////////////////
}

void ht1621_print(int num) {
    uint16_t value=get_abs(num);
    uint8_t first=1;
	
	if (num <= 0) 
	  ht1621_clear();
	  
	PB_ODR &= ~(1<<CS);
	do {
        uint8_t rm=value % 10;

		if (first) {
		  ht1621_send(0xa0, 3);
		  ht1621_send(0, 6);
		  ht1621_send(digits[rm], 8);		 
		  first=0;
		} else {
		  ht1621_send(digits[rm], 8);		 
		}
        value=value/10;
    } while (value>0);  
	
	if (num < 0) 
	  ht1621_send(MINUS,8);  

	  
	PB_ODR |=  (1<<CS);
}

void ht1621_send_data(uint8_t adr, uint8_t value) {
	adr <<=2;
    PB_ODR &= ~(1<<CS);
    ht1621_send(0xa0, 3);
    ht1621_send(adr, 6);
    ht1621_send(value, 8);
    PB_ODR |=  (1<<CS);
}

void ht1621_send_cmd(uint8_t cmd) {
	PB_ODR &= ~(1<<CS);
	ht1621_send(0x80,4);
	ht1621_send(cmd,8);
	PB_ODR |=  (1<<CS);
}

void ht1621_send(uint8_t data, uint8_t len) {
	char i;
	for(i=0;i<len;i++) {
        PB_ODR=(data & 0x80) ? PB_ODR | (1<<DATA) : PB_ODR & ~(1<<DATA);
		PB_ODR &= ~(1<<WR);
		PB_ODR |= (1<<WR);
        data=(data<<1);
	}
}

/*
uint8_t ht1621_char_to_seg_bits(uint8_t ch) {
    switch (ch) {
    case '*': // For degree for now
        return 0b0110011;
    case '|':
        return 0b0000101;
    case '-':
        return 0b0000010;
    case '_':
        return 0b0001000;
    case '0':
        return 0b1111101;
    case '1':
        return 0b1100000;
    case '2':
        return 0b111110;
    case '3':
        return 0b1111010;
    case '4':
        return 0b1100011;
    case '5':
        return 0b1011011;
    case '6':
        return 0b1011111;
    case '7':
        return 0b1110000;
    case '8':
        return 0b1111111;
    case '9':
        return 0b1111011;
     case 'A':
    case 'a':
        return 0b1110111;
    case 'b':
    case 'B':
        return 0b1001111;
    case 'c':
    //  return 0b0001110;
    case 'C':
        return 0b0011101;
    case 'd':
    case 'D':
        return 0b1101110;
    case 'e':
    //  return 0b0001110;
    case 'E':
        return 0b0011111;
    case 'f':
    //  return 0b0000111;
    case 'F':
        return 0b0010111;
    case 'G':
    case 'g':
        return 0b1011101;
    case 'h':
    //  return 0b1000111;
    case 'H':
        return 0b1100111;
    case 'i':
    //  return 0b1000000;
    case 'I':
        return 0b1100000;
    case 'J':
    case 'j':
        return 0b1101000;
    case 'l':
    //  return 0b1100000;
    case 'L':
        return 0b0001101;
    case 'm':
    case 'M':
        return 0b1010100;
    case 'n':
    case 'N':
        return 0b1000110;
    case 'o':
        return 0b1001110;
    case 'P':
    case 'p':
        return 0b0110111;
    case 'q':
    case 'Q':
        return 0b1110011;
    case 'r':
    case 'R':
        return 0b0000110;
    case 'S':
    case 's':
        return 0b1011011;
    case 't':
    case 'T':
        return 0b0001111;
    case 'u':
    //  return 0b1001100;
    case 'U':
        return 0b1101101;
    case 'Y':
    case 'y':
        return 0b1101011;
    case 'z':
    case 'Z':
        return 0b0111110;
    case ' ':
    default:
        return 0b0000000;
    }
}
*/
