   1                         switch .text
   2                         ; ------------------
   3                         xref _delay_ms
   4                     	xref _btn_press
   5                     	xdef _tim1_handler
   6                     	xdef _exti1_handler
   1                     ; STM8L151C.h 
   2                     
   3                     ; Copyright (c) 2003-2017 STMicroelectronics 
   4                     
   5                     ;#ifndef __STM8L151C__
   6                     ;#define __STM8L151C__
   7                     
   8                     ; STM8L151C 
   9                     
  10                     	; Check MCU name 
  11                     ;	#ifdef MCU_NAME
  12                     ;		#define STM8L151C 1
  13                     ;		#if (MCU_NAME != STM8L151C)
  14                     ;		#error "wrong include file STM8L151C.h for chosen MCU!"
  15                     ;		#endif
  16                     ;	#endif
  17                     
  18                     ;	#ifdef __CSMC__
  19                     ;		#define .equ NAME,ADDRESS) volatile unsigned char NAME @ADDRESS
  20                     ;		#define .equ NAME,ADDRESS) volatile unsigned int NAME @ADDRESS
  21                     ;	#endif
  22                     
  23                     ;	#ifdef __RAISONANCE__
  24                     ;		#define .equ NAME,ADDRESS) at ADDRESS hreg NAME
  25                     ;		#define .equ NAME,ADDRESS) at ADDRESS hreg16 NAME
  26                     ;	#endif
  27                     
  28                     ; Port A 
  29                     ;***************************************************************
  30                     
  31       00000000      P0: equ 0
  32       00000001      P1: equ 1
  33       00000002      P2: equ 2
  34       00000003      P3: equ 3
  35       00000004      P4: equ 4
  36       00000005      P5: equ 5
  37       00000006      P6: equ 6
  38       00000007      P7: equ 7
  39                     
  40                     
  41                     ; Port A data output latch register 
  42       00005000      PA_ODR: equ $5000
  43                     
  44                     ; Port A input pin value register 
  45       00005001      PA_IDR: equ $5001
  46                     
  47                     ; Port A data direction register 
  48       00005002      PA_DDR: equ $5002
  49                     
  50                     ; Port A control register 1 
  51       00005003      PA_CR1: equ $5003
  52                     
  53                     ; Port A control register 2 
  54       00005004      PA_CR2: equ $5004
  55                     
  56                     ; Port B 
  57                     ;***************************************************************
  58                     
  59                     ; Port B data output latch register 
  60       00005005      PB_ODR: equ $5005
  61                     
  62                     ; Port B input pin value register 
  63       00005006      PB_IDR: equ $5006
  64                     
  65                     ; Port B data direction register 
  66       00005007      PB_DDR: equ $5007
  67                     
  68                     ; Port B control register 1 
  69       00005008      PB_CR1: equ $5008
  70                     
  71                     ; Port B control register 2 
  72       00005009      PB_CR2: equ $5009
  73                     
  74                     ; Port C 
  75                     ;***************************************************************
  76                     
  77                     ; Port C data output latch register 
  78       0000500a      PC_ODR: equ $500a
  79                     
  80                     ; Port C input pin value register 
  81       0000500b      PC_IDR: equ $500b
  82                     
  83                     ; Port C data direction register 
  84       0000500c      PC_DDR: equ $500c
  85                     
  86                     ; Port C control register 1 
  87       0000500d      PC_CR1: equ $500d
  88                     
  89                     ; Port C control register 2 
  90       0000500e      PC_CR2: equ $500e
  91                     
  92                     ; Port D 
  93                     ;***************************************************************
  94                     
  95                     ; Port D data output latch register 
  96       0000500f      PD_ODR: equ $500f
  97                     
  98                     ; Port D input pin value register 
  99       00005010      PD_IDR: equ $5010
 100                     
 101                     ; Port D data direction register 
 102       00005011      PD_DDR: equ $5011
 103                     
 104                     ; Port D control register 1 
 105       00005012      PD_CR1: equ $5012
 106                     
 107                     ; Port D control register 2 
 108       00005013      PD_CR2: equ $5013
 109                     
 110                     ; Port E 
 111                     ;***************************************************************
 112                     
 113                     ; Port E data output latch register 
 114       00005014      PE_ODR: equ $5014
 115                     
 116                     ; Port E input pin value register 
 117       00005015      PE_IDR: equ $5015
 118                     
 119                     ; Port E data direction register 
 120       00005016      PE_DDR: equ $5016
 121                     
 122                     ; Port E control register 1 
 123       00005017      PE_CR1: equ $5017
 124                     
 125                     ; Port E control register 2 
 126       00005018      PE_CR2: equ $5018
 127                     
 128                     ; Port F 
 129                     ;***************************************************************
 130                     
 131                     ; Port F data output latch register 
 132       00005019      PF_ODR: equ $5019
 133                     
 134                     ; Port F input pin value register 
 135       0000501a      PF_IDR: equ $501a
 136                     
 137                     ; Port F data direction register 
 138       0000501b      PF_DDR: equ $501b
 139                     
 140                     ; Port F control register 1 
 141       0000501c      PF_CR1: equ $501c
 142                     
 143                     ; Port F control register 2 
 144       0000501d      PF_CR2: equ $501d
 145                     
 146                     ; Flash 
 147                     ;***************************************************************
 148                     
 149                     ; Flash control register 1 
 150       00005050      FLASH_CR1: equ $5050
 151                     
 152                     ; Flash control register 2 
 153       00005051      FLASH_CR2: equ $5051
 154                     
 155                     ; Flash Program memory unprotection register 
 156       00005052      FLASH_PUKR: equ $5052
 157                     
 158                     ; Data EEPROM unprotection register 
 159       00005053      FLASH_DUKR: equ $5053
 160                     
 161                     ; Flash in-application programming status register 
 162       00005054      FLASH_IAPSR: equ $5054
 163                     
 164                     ; Direct memory access controller 1 (DMA1) 
 165                     ;***************************************************************
 166                     
 167                     ; DMA1 global configuration & status register 
 168       00005070      DMA1_GCSR: equ $5070
 169                     
 170                     ; DMA1 global interrupt register 1 
 171       00005071      DMA1_GIR1: equ $5071
 172                     
 173                     ; DMA1 channel 0 configuration register 
 174       00005075      DMA1_C0CR: equ $5075
 175                     
 176                     ; DMA1 channel 0 status & priority register 
 177       00005076      DMA1_C0SPR: equ $5076
 178                     
 179                     ; DMA1 number of data to transfer register (channel 0) 
 180       00005077      DMA1_C0NDTR: equ $5077
 181                     
 182                     ; DMA1 peripheral address register (channel 0) 
 183       00005078      DMA1_C0PAR: equ $5078
 184                     ; DMA peripheral address high register (channel 0) 
 185       00005078      DMA1_C0PARH: equ $5078
 186                     ; DMA peripheral address low register (channel 0) 
 187       00005079      DMA1_C0PARL: equ $5079
 188                     
 189                     ; DMA1 memory 0 address register (channel 0) 
 190       0000507b      DMA1_C0M0AR: equ $507b
 191                     ; DMA memory address high register (channel 0) 
 192       0000507b      DMA1_C0M0ARH: equ $507b
 193                     ; DMA memory address low register (channel 0) 
 194       0000507c      DMA1_C0M0ARL: equ $507c
 195                     
 196                     ; DMA1 channel 1 configuration register 
 197       0000507f      DMA1_C1CR: equ $507f
 198                     
 199                     ; DMA1 channel 1 status & priority register 
 200       00005080      DMA1_C1SPR: equ $5080
 201                     
 202                     ; DMA1 number of data to transfer register (channel 1) 
 203       00005081      DMA1_C1NDTR: equ $5081
 204                     
 205                     ; DMA1 peripheral address register (channel 1) 
 206       00005082      DMA1_C1PAR: equ $5082
 207                     ; DMA peripheral address high register (channel 1) 
 208       00005082      DMA1_C1PARH: equ $5082
 209                     ; DMA peripheral address low register (channel 1) 
 210       00005083      DMA1_C1PARL: equ $5083
 211                     
 212                     ; DMA1 memory 0 address register (channel 1) 
 213       00005085      DMA1_C1M0AR: equ $5085
 214                     ; DMA memory address high register (channel 1) 
 215       00005085      DMA1_C1M0ARH: equ $5085
 216                     ; DMA memory address low register (channel 1) 
 217       00005086      DMA1_C1M0ARL: equ $5086
 218                     
 219                     ; DMA1 channel 2 configuration register 
 220       00005089      DMA1_C2CR: equ $5089
 221                     
 222                     ; DMA1 channel 2 status & priority register 
 223       0000508a      DMA1_C2SPR: equ $508a
 224                     
 225                     ; DMA1 number of data to transfer register (channel 2) 
 226       0000508b      DMA1_C2NDTR: equ $508b
 227                     
 228                     ; DMA1 peripheral address register (channel 2) 
 229       0000508c      DMA1_C2PAR: equ $508c
 230                     ; DMA peripheral address high register (channel 2) 
 231       0000508c      DMA1_C2PARH: equ $508c
 232                     ; DMA peripheral address low register (channel 2) 
 233       0000508d      DMA1_C2PARL: equ $508d
 234                     
 235                     ; DMA1 memory 0 address register (channel 2) 
 236       0000508f      DMA1_C2M0AR: equ $508f
 237                     ; DMA memory address high register (channel 2) 
 238       0000508f      DMA1_C2M0ARH: equ $508f
 239                     ; DMA memory address low register (channel 2) 
 240       00005090      DMA1_C2M0ARL: equ $5090
 241                     
 242                     ; DMA1 channel 3 configuration register 
 243       00005093      DMA1_C3CR: equ $5093
 244                     
 245                     ; DMA1 channel 3 status & priority register 
 246       00005094      DMA1_C3SPR: equ $5094
 247                     
 248                     ; DMA1 number of data to transfer register (channel 3) 
 249       00005095      DMA1_C3NDTR: equ $5095
 250                     
 251                     ; DMA1 peripheral address register (channel 3) 
 252       00005096      DMA1_C3PAR_C3M1AR: equ $5096
 253                     ; DMA1 peripheral address high register (channel 3) 
 254       00005096      DMA1_C3PARH_C3M1ARH: equ $5096
 255                     ; DMA1 peripheral address low register (channel 3) 
 256       00005097      DMA1_C3PARL_C3M1ARL: equ $5097
 257                     
 258                     ; DMA channel 3 memory 0 extended address register 
 259       00005098      DMA_C3M0EAR: equ $5098
 260                     
 261                     ; DMA1 memory 0 address register (channel 3) 
 262       00005099      DMA1_C3M0AR: equ $5099
 263                     ; DMA memory address high register (channel 3) 
 264       00005099      DMA1_C3M0ARH: equ $5099
 265                     ; DMA memory address low register (channel 3) 
 266       0000509a      DMA1_C3M0ARL: equ $509a
 267                     
 268                     ; System configuration (SYSCFG) 
 269                     ;***************************************************************
 270                     
 271                     ; Remapping register 3 
 272       0000509d      SYSCFG_RMPCR3: equ $509d
 273                     
 274                     ; Remapping register 1 
 275       0000509e      SYSCFG_RMPCR1: equ $509e
 276                     
 277                     ; Remapping register 2 
 278       0000509f      SYSCFG_RMPCR2: equ $509f
 279                     
 280                     ; External Interrupt Control Register (ITC) 
 281                     ;***************************************************************
 282                     
 283                     ; External interrupt control register 1 
 284       000050a0      EXTI_CR1: equ $50a0
 285                     
 286                     ; External interrupt control register 2 
 287       000050a1      EXTI_CR2: equ $50a1
 288                     
 289                     ; External interrupt control register 3 
 290       000050a2      EXTI_CR3: equ $50a2
 291                     
 292                     ; External interrupt status register 1 
 293       000050a3      EXTI_SR1: equ $50a3
 294                     
 295                     ; External interrupt status register 2 
 296       000050a4      EXTI_SR2: equ $50a4
 297                     
 298                     ; External interrupt port select register 1 
 299       000050a5      EXTI_CONF1: equ $50a5
 300                     
 301                     ; Wait For Event (WFE) 
 302                     ;***************************************************************
 303                     
 304                     ; WFE control register 1 
 305       000050a6      WFE_CR1: equ $50a6
 306                     
 307                     ; WFE control register 2 
 308       000050a7      WFE_CR2: equ $50a7
 309                     
 310                     ; WFE control register 3 
 311       000050a8      WFE_CR3: equ $50a8
 312                     
 313                     ; WFE control register 4 
 314       000050a9      WFE_CR4: equ $50a9
 315                     
 316                     ; External Interrupt Control Register (ITC) 
 317                     ;***************************************************************
 318                     
 319                     ; External interrupt control register 4 
 320       000050aa      EXTI_CR4: equ $50aa
 321                     
 322                     ; External interrupt port select register 2 
 323       000050ab      EXTI_CONF2: equ $50ab
 324                     
 325                     ; Reset (RST) 
 326                     ;***************************************************************
 327                     
 328                     ; Reset control register 
 329       000050b0      RST_CR: equ $50b0
 330                     
 331                     ; Reset status register 
 332       000050b1      RST_SR: equ $50b1
 333                     
 334                     ; Power control (PWR) 
 335                     ;***************************************************************
 336                     
 337                     ; Power control and status register 1 
 338       000050b2      PWR_CSR1: equ $50b2
 339                     
 340                     ; Power control and status register 2 
 341       000050b3      PWR_CSR2: equ $50b3
 342                     
 343                     ; Clock Control (CLK) 
 344                     ;***************************************************************
 345                     
 346                     ; System clock divider register 
 347       000050c0      CLK_CKDIVR: equ $50c0
 348                     
 349                     ; Clock RTC register 
 350       000050c1      CLK_CRTCR: equ $50c1
 351                     
 352                     ; Internal clock control register 
 353       000050c2      CLK_ICKCR: equ $50c2
 354                     
 355                     ; Peripheral clock gating register 1 
 356       000050c3      CLK_PCKENR1: equ $50c3
 357                     
 358                     ; Peripheral clock gating register 2 
 359       000050c4      CLK_PCKENR2: equ $50c4
 360                     
 361                     ; Configurable clock control register 
 362       000050c5      CLK_CCOR: equ $50c5
 363                     
 364                     ; External clock control register 
 365       000050c6      CLK_ECKCR: equ $50c6
 366                     
 367                     ; System clock status register 
 368       000050c7      CLK_SCSR: equ $50c7
 369                     
 370                     ; System clock switch register 
 371       000050c8      CLK_SWR: equ $50c8
 372                     
 373                     ; Clock switch control register 
 374       000050c9      CLK_SWCR: equ $50c9
 375                     
 376                     ; Clock security system register 
 377       000050ca      CLK_CSSR: equ $50ca
 378                     
 379                     ; Clock BEEP register 
 380       000050cb      CLK_CBEEPR: equ $50cb
 381                     
 382                     ; HSI calibration register 
 383       000050cc      CLK_HSICALR: equ $50cc
 384                     
 385                     ; HSI clock calibration trimming register 
 386       000050cd      CLK_HSITRIMR: equ $50cd
 387                     
 388                     ; HSI unlock register 
 389       000050ce      CLK_HSIUNLCKR: equ $50ce
 390                     
 391                     ; Main regulator control status register 
 392       000050cf      CLK_REGCSR: equ $50cf
 393                     
 394                     ; Peripheral clock gating register 3 
 395       000050d0      CLK_PCKENR3: equ $50d0
 396                     
 397                     ; Window Watchdog (WWDG) 
 398                     ;***************************************************************
 399                     
 400                     ; WWDG Control Register 
 401       000050d3      WWDG_CR: equ $50d3
 402                     
 403                     ; WWDR Window Register 
 404       000050d4      WWDG_WR: equ $50d4
 405                     
 406                     ; Independent Watchdog (IWDG) 
 407                     ;***************************************************************
 408                     
 409                     ; IWDG Key Register 
 410       000050e0      IWDG_KR: equ $50e0
 411                     
 412                     ; IWDG Prescaler Register 
 413       000050e1      IWDG_PR: equ $50e1
 414                     
 415                     ; IWDG Reload Register 
 416       000050e2      IWDG_RLR: equ $50e2
 417                     
 418                     ; Beeper (BEEP) 
 419                     ;***************************************************************
 420                     
 421                     ; BEEP Control/Status Register 1 
 422       000050f0      BEEP_CSR1: equ $50f0
 423                     
 424                     ; BEEP Control/Status Register 2 
 425       000050f3      BEEP_CSR2: equ $50f3
 426                     
 427                     ; Real-time clock (RTC) 
 428                     ;***************************************************************
 429                     
 430                     ; Time Register 1 
 431       00005140      RTC_TR1: equ $5140
 432                     
 433                     ; Time Register 2 
 434       00005141      RTC_TR2: equ $5141
 435                     
 436                     ; Time Register 3 
 437       00005142      RTC_TR3: equ $5142
 438                     
 439                     ; Date Register 1 
 440       00005144      RTC_DR1: equ $5144
 441                     
 442                     ; Date Register 2 
 443       00005145      RTC_DR2: equ $5145
 444                     
 445                     ; Date Register 3 
 446       00005146      RTC_DR3: equ $5146
 447                     
 448                     ; Control Register 1 
 449       00005148      RTC_CR1: equ $5148
 450                     
 451                     ; Control Register 2 
 452       00005149      RTC_CR2: equ $5149
 453                     
 454                     ; Control Register 3 
 455       0000514a      RTC_CR3: equ $514a
 456                     
 457                     ; Initialization and Status Register 1 
 458       0000514c      RTC_ISR1: equ $514c
 459                     
 460                     ; Initialization and Status Register 2 
 461       0000514d      RTC_ISR2: equ $514d
 462                     
 463                     ; Synchronous Prescaler Register 
 464       00005150      RTC_SPRER: equ $5150
 465                     ; Synchronous Prescaler Register High 
 466       00005150      RTC_SPRERH: equ $5150
 467                     ; Synchronous Prescaler Register Low 
 468       00005151      RTC_SPRERL: equ $5151
 469                     
 470                     ; Asynchronous Prescaler Register 
 471       00005152      RTC_APRER: equ $5152
 472                     
 473                     ; Wakeup Timer Register 
 474       00005154      RTC_WUTR: equ $5154
 475                     ; Wakeup Timer Register High 
 476       00005154      RTC_WUTRH: equ $5154
 477                     ; Wakeup Timer Register Low 
 478       00005155      RTC_WUTRL: equ $5155
 479                     
 480                     ; Subsecond Register 
 481       00005157      RTC_SSR: equ $5157
 482                     ; Subsecond Register High 
 483       00005157      RTC_SSRH: equ $5157
 484                     ; Subsecond Register Low 
 485       00005158      RTC_SSRL: equ $5158
 486                     
 487                     ; Write Protection Register 
 488       00005159      RTC_WPR: equ $5159
 489                     
 490                     ; Shift Register 
 491       0000515a      RTC_SHIFTR: equ $515a
 492                     ; Shift Register High 
 493       0000515a      RTC_SHIFTRH: equ $515a
 494                     ; Shift Register Low 
 495       0000515b      RTC_SHIFTRL: equ $515b
 496                     
 497                     ; Alarm A Register 1 
 498       0000515c      RTC_ALRMAR1: equ $515c
 499                     
 500                     ; Alarm A Register 2 
 501       0000515d      RTC_ALRMAR2: equ $515d
 502                     
 503                     ; Alarm A Register 3 
 504       0000515e      RTC_ALRMAR3: equ $515e
 505                     
 506                     ; Alarm A Register 4 
 507       0000515f      RTC_ALRMAR4: equ $515f
 508                     
 509                     ; Alarm A subsecond Register 
 510       00005164      RTC_ALRMASSR: equ $5164
 511                     ; Shift Register High 
 512       00005164      RTC_ALRMASSRH: equ $5164
 513                     ; Shift Register Low 
 514       00005165      RTC_ALRMASSRL: equ $5165
 515                     
 516                     ; Alarm A masking Register 
 517       00005166      RTC_ALRMASSMSKR: equ $5166
 518                     
 519                     ; Calibration Register 
 520       0000516a      RTC_CALR: equ $516a
 521                     ; Shift Register High 
 522       0000516a      RTC_CALRH: equ $516a
 523                     ; Shift Register Low 
 524       0000516b      RTC_CALRL: equ $516b
 525                     
 526                     ; Tamper Control Register 1 
 527       0000516c      RTC_TCR1: equ $516c
 528                     
 529                     ; Tamper Control Register 2 
 530       0000516d      RTC_TCR2: equ $516d
 531                     
 532                     ; Clock Security System (LSE_CSS) 
 533                     ;***************************************************************
 534                     
 535                     ; CSS on LSE Control and Status Register 
 536       00005190      CSS_LSE_CSR: equ $5190
 537                     
 538                     ; Serial Peripheral Interface 1 (SPI1) 
 539                     ;***************************************************************
 540                     
 541                     ; SPI1 Control Register 1 
 542       00005200      SPI1_CR1: equ $5200
 543                     
 544                     ; SPI1 Control Register 2 
 545       00005201      SPI1_CR2: equ $5201
 546                     
 547                     ; SPI1 Interrupt Control Register 
 548       00005202      SPI1_ICR: equ $5202
 549                     
 550                     ; SPI1 Status Register 
 551       00005203      SPI1_SR: equ $5203
 552                     
 553                     ; SPI1 Data Register 
 554       00005204      SPI1_DR: equ $5204
 555                     
 556                     ; SPI1 CRC Polynomial Register 
 557       00005205      SPI1_CRCPR: equ $5205
 558                     
 559                     ; SPI1 Rx CRC Register 
 560       00005206      SPI1_RXCRCR: equ $5206
 561                     
 562                     ; SPI1 Tx CRC Register 
 563       00005207      SPI1_TXCRCR: equ $5207
 564                     
 565                     ; I2C Bus Interface 1 (I2C1) 
 566                     ;***************************************************************
 567                     
 568                     ; I2C1 control register 1 
 569       00005210      I2C1_CR1: equ $5210
 570                     
 571                     ; I2C1 control register 2 
 572       00005211      I2C1_CR2: equ $5211
 573                     
 574                     ; I2C1 frequency register 
 575       00005212      I2C1_FREQR: equ $5212
 576                     
 577                     ; I2C1 Own address register low 
 578       00005213      I2C1_OARL: equ $5213
 579                     
 580                     ; I2C1 Own address register high 
 581       00005214      I2C1_OARH: equ $5214
 582                     
 583                     ; I2C1 Own address register for dual mode 
 584       00005215      I2C1_OAR2: equ $5215
 585                     
 586                     ; I2C1 data register 
 587       00005216      I2C1_DR: equ $5216
 588                     
 589                     ; I2C1 status register 1 
 590       00005217      I2C1_SR1: equ $5217
 591                     
 592                     ; I2C1 status register 2 
 593       00005218      I2C1_SR2: equ $5218
 594                     
 595                     ; I2C1 status register 3 
 596       00005219      I2C1_SR3: equ $5219
 597                     
 598                     ; I2C1 interrupt control register 
 599       0000521a      I2C1_ITR: equ $521a
 600                     
 601                     ; I2C1 Clock control register low 
 602       0000521b      I2C1_CCRL: equ $521b
 603                     
 604                     ; I2C1 Clock control register high 
 605       0000521c      I2C1_CCRH: equ $521c
 606                     
 607                     ; I2C1 TRISE register 
 608       0000521d      I2C1_TRISER: equ $521d
 609                     
 610                     ; I2C1 packet error checking register 
 611       0000521e      I2C1_PECR: equ $521e
 612                     
 613                     ; Universal synch/asynch receiver transmitter 1 (USART1) 
 614                     ;***************************************************************
 615                     
 616                     ; USART1 Status Register 
 617       00005230      USART1_SR: equ $5230
 618                     
 619                     ; USART1 Data Register 
 620       00005231      USART1_DR: equ $5231
 621                     
 622                     ; USART1 Baud Rate Register 1 
 623       00005232      USART1_BRR1: equ $5232
 624                     
 625                     ; USART1 Baud Rate Register 2 
 626       00005233      USART1_BRR2: equ $5233
 627                     
 628                     ; USART1 Control Register 1 
 629       00005234      USART1_CR1: equ $5234
 630                     
 631                     ; USART1 Control Register 2 
 632       00005235      USART1_CR2: equ $5235
 633                     
 634                     ; USART1 Control Register 3 
 635       00005236      USART1_CR3: equ $5236
 636                     
 637                     ; USART1 Control Register 4 
 638       00005237      USART1_CR4: equ $5237
 639                     
 640                     ; USART1 Control Register 5 
 641       00005238      USART1_CR5: equ $5238
 642                     
 643                     ; USART1 Guard time Register 
 644       00005239      USART1_GTR: equ $5239
 645                     
 646                     ; USART1 Prescaler Register 
 647       0000523a      USART1_PSCR: equ $523a
 648                     
 649                     ; 16-Bit Timer 2 (TIM2) 
 650                     ;***************************************************************
 651                     
 652                     ; TIM2 Control register 1 
 653       00005250      TIM2_CR1: equ $5250
 654                     
 655                     ; TIM2 Control register 2 
 656       00005251      TIM2_CR2: equ $5251
 657                     
 658                     ; TIM2 Slave Mode Control register 
 659       00005252      TIM2_SMCR: equ $5252
 660                     
 661                     ; TIM2 External trigger register 
 662       00005253      TIM2_ETR: equ $5253
 663                     
 664                     ; TIM2 DMA request enable register 
 665       00005254      TIM2_DER: equ $5254
 666                     
 667                     ; TIM2 Interrupt enable register 
 668       00005255      TIM2_IER: equ $5255
 669                     
 670                     ; TIM2 Status register 1 
 671       00005256      TIM2_SR1: equ $5256
 672                     
 673                     ; TIM2 Status register 2 
 674       00005257      TIM2_SR2: equ $5257
 675                     
 676                     ; TIM2 Event Generation register 
 677       00005258      TIM2_EGR: equ $5258
 678                     
 679                     ; TIM2 Capture/Compare mode register 1 
 680       00005259      TIM2_CCMR1: equ $5259
 681                     
 682                     ; TIM2 Capture/Compare mode register 2 
 683       0000525a      TIM2_CCMR2: equ $525a
 684                     
 685                     ; TIM2 Capture/Compare enable register 1 
 686       0000525b      TIM2_CCER1: equ $525b
 687                     
 688                     ; TIM2 Counter 
 689       0000525c      TIM2_CNTR: equ $525c
 690                     ; TIM2 Counter High 
 691       0000525c      TIM2_CNTRH: equ $525c
 692                     ; TIM2 Counter Low 
 693       0000525d      TIM2_CNTRL: equ $525d
 694                     
 695                     ; TIM2 Prescaler register 
 696       0000525e      TIM2_PSCR: equ $525e
 697                     
 698                     ; TIM2 Auto-reload register 
 699       0000525f      TIM2_ARR: equ $525f
 700                     ; TIM2 Auto-Reload Register High 
 701       0000525f      TIM2_ARRH: equ $525f
 702                     ; TIM2 Auto-Reload Register Low 
 703       00005260      TIM2_ARRL: equ $5260
 704                     
 705                     ; TIM2 Capture/Compare register 1 
 706       00005261      TIM2_CCR1: equ $5261
 707                     ; TIM2 Capture/Compare Register 1 High 
 708       00005261      TIM2_CCR1H: equ $5261
 709                     ; TIM2 Capture/Compare Register 1 Low 
 710       00005262      TIM2_CCR1L: equ $5262
 711                     
 712                     ; TIM2 Capture/Compare register 2 
 713       00005263      TIM2_CCR2: equ $5263
 714                     ; TIM2 Capture/Compare Register 2 High 
 715       00005263      TIM2_CCR2H: equ $5263
 716                     ; TIM2 Capture/Compare Register 2 Low 
 717       00005264      TIM2_CCR2L: equ $5264
 718                     
 719                     ; TIM2 Break register 
 720       00005265      TIM2_BKR: equ $5265
 721                     
 722                     ; TIM2 Output idle state register 
 723       00005266      TIM2_OISR: equ $5266
 724                     
 725                     ; 16-Bit Timer 3 (TIM3) 
 726                     ;***************************************************************
 727                     
 728                     ; TIM3 Control register 1 
 729       00005280      TIM3_CR1: equ $5280
 730                     
 731                     ; TIM3 Control register 2 
 732       00005281      TIM3_CR2: equ $5281
 733                     
 734                     ; TIM3 Slave Mode Control register 
 735       00005282      TIM3_SMCR: equ $5282
 736                     
 737                     ; TIM3 External trigger register 
 738       00005283      TIM3_ETR: equ $5283
 739                     
 740                     ; TIM3 DMA request enable register 
 741       00005284      TIM3_DER: equ $5284
 742                     
 743                     ; TIM3 Interrupt enable register 
 744       00005285      TIM3_IER: equ $5285
 745                     
 746                     ; TIM3 Status register 1 
 747       00005286      TIM3_SR1: equ $5286
 748                     
 749                     ; TIM3 Status register 2 
 750       00005287      TIM3_SR2: equ $5287
 751                     
 752                     ; TIM3 Event Generation register 
 753       00005288      TIM3_EGR: equ $5288
 754                     
 755                     ; TIM3 Capture/Compare mode register 1 
 756       00005289      TIM3_CCMR1: equ $5289
 757                     
 758                     ; TIM3 Capture/Compare mode register 2 
 759       0000528a      TIM3_CCMR2: equ $528a
 760                     
 761                     ; TIM3 Capture/Compare enable register 1 
 762       0000528b      TIM3_CCER1: equ $528b
 763                     
 764                     ; TIM3 Counter 
 765       0000528c      TIM3_CNTR: equ $528c
 766                     ; TIM3 Counter High 
 767       0000528c      TIM3_CNTRH: equ $528c
 768                     ; TIM3 Counter Low 
 769       0000528d      TIM3_CNTRL: equ $528d
 770                     
 771                     ; TIM3 Prescaler register 
 772       0000528e      TIM3_PSCR: equ $528e
 773                     
 774                     ; TIM3 Auto-reload register 
 775       0000528f      TIM3_ARR: equ $528f
 776                     ; TIM3 Auto-Reload Register High 
 777       0000528f      TIM3_ARRH: equ $528f
 778                     ; TIM3 Auto-Reload Register Low 
 779       00005290      TIM3_ARRL: equ $5290
 780                     
 781                     ; TIM3 Capture/Compare register 1 
 782       00005291      TIM3_CCR1: equ $5291
 783                     ; TIM3 Capture/Compare Register 1 High 
 784       00005291      TIM3_CCR1H: equ $5291
 785                     ; TIM3 Capture/Compare Register 1 Low 
 786       00005292      TIM3_CCR1L: equ $5292
 787                     
 788                     ; TIM3 Capture/Compare register 2 
 789       00005293      TIM3_CCR2: equ $5293
 790                     ; TIM3 Capture/Compare Register 2 High 
 791       00005293      TIM3_CCR2H: equ $5293
 792                     ; TIM3 Capture/Compare Register 2 Low 
 793       00005294      TIM3_CCR2L: equ $5294
 794                     
 795                     ; TIM3 Break register 
 796       00005295      TIM3_BKR: equ $5295
 797                     
 798                     ; TIM3 Output idle state register 
 799       00005296      TIM3_OISR: equ $5296
 800                     
 801                     ; 16-Bit Timer 1 (TIM1) 
 802                     ;***************************************************************
 803                     
 804                     ; TIM1 Control register 1 
 805       000052b0      TIM1_CR1: equ $52b0
 806                     
 807                     ; TIM1 Control register 2 
 808       000052b1      TIM1_CR2: equ $52b1
 809                     
 810                     ; TIM1 Slave Mode Control register 
 811       000052b2      TIM1_SMCR: equ $52b2
 812                     
 813                     ; TIM1 external trigger register 
 814       000052b3      TIM1_ETR: equ $52b3
 815                     
 816                     ; TIM1 DMA request enable register 
 817       000052b4      TIM1_DER: equ $52b4
 818                     
 819                     ; TIM1 Interrupt enable register 
 820       000052b5      TIM1_IER: equ $52b5
 821                     
 822                     ; TIM1 Status register 1 
 823       000052b6      TIM1_SR1: equ $52b6
 824                     
 825                     ; TIM1 Status register 2 
 826       000052b7      TIM1_SR2: equ $52b7
 827                     
 828                     ; TIM1 Event Generation register 
 829       000052b8      TIM1_EGR: equ $52b8
 830                     
 831                     ; TIM1 Capture/Compare mode register 1 
 832       000052b9      TIM1_CCMR1: equ $52b9
 833                     
 834                     ; TIM1 Capture/Compare mode register 2 
 835       000052ba      TIM1_CCMR2: equ $52ba
 836                     
 837                     ; TIM1 Capture/Compare mode register 3 
 838       000052bb      TIM1_CCMR3: equ $52bb
 839                     
 840                     ; TIM1 Capture/Compare mode register 4 
 841       000052bc      TIM1_CCMR4: equ $52bc
 842                     
 843                     ; TIM1 Capture/Compare enable register 1 
 844       000052bd      TIM1_CCER1: equ $52bd
 845                     
 846                     ; TIM1 Capture/Compare enable register 2 
 847       000052be      TIM1_CCER2: equ $52be
 848                     
 849                     ; TIM1 Counter 
 850       000052bf      TIM1_CNTR: equ $52bf
 851                     ; TIM1 Counter High 
 852       000052bf      TIM1_CNTRH: equ $52bf
 853                     ; TIM1 Counter Low 
 854       000052c0      TIM1_CNTRL: equ $52c0
 855                     
 856                     ; TIM1 Prescaler register 
 857       000052c1      TIM1_PSCR: equ $52c1
 858                     ; TIM1 Prescaler Register High 
 859       000052c1      TIM1_PSCRH: equ $52c1
 860                     ; TIM1 Prescaler Register Low 
 861       000052c2      TIM1_PSCRL: equ $52c2
 862                     
 863                     ; TIM1 Auto-reload register 
 864       000052c3      TIM1_ARR: equ $52c3
 865                     ; TIM1 Auto-Reload Register High 
 866       000052c3      TIM1_ARRH: equ $52c3
 867                     ; TIM1 Auto-Reload Register Low 
 868       000052c4      TIM1_ARRL: equ $52c4
 869                     
 870                     ; TIM1 Repetition counter register 
 871       000052c5      TIM1_RCR: equ $52c5
 872                     
 873                     ; TIM1 Capture/Compare register 1 
 874       000052c6      TIM1_CCR1: equ $52c6
 875                     ; TIM1 Capture/Compare Register 1 High 
 876       000052c6      TIM1_CCR1H: equ $52c6
 877                     ; TIM1 Capture/Compare Register 1 Low 
 878       000052c7      TIM1_CCR1L: equ $52c7
 879                     
 880                     ; TIM1 Capture/Compare register 2 
 881       000052c8      TIM1_CCR2: equ $52c8
 882                     ; TIM1 Capture/Compare Register 2 High 
 883       000052c8      TIM1_CCR2H: equ $52c8
 884                     ; TIM1 Capture/Compare Register 2 Low 
 885       000052c9      TIM1_CCR2L: equ $52c9
 886                     
 887                     ; TIM1 Capture/Compare register 3 
 888       000052ca      TIM1_CCR3: equ $52ca
 889                     ; TIM1 Capture/Compare Register 3 High 
 890       000052ca      TIM1_CCR3H: equ $52ca
 891                     ; TIM1 Capture/Compare Register 3 Low 
 892       000052cb      TIM1_CCR3L: equ $52cb
 893                     
 894                     ; TIM1 Capture/Compare register 4 
 895       000052cc      TIM1_CCR4: equ $52cc
 896                     ; TIM1 Capture/Compare Register 4 High 
 897       000052cc      TIM1_CCR4H: equ $52cc
 898                     ; TIM1 Capture/Compare Register 4 Low 
 899       000052cd      TIM1_CCR4L: equ $52cd
 900                     
 901                     ; TIM1 Break register 
 902       000052ce      TIM1_BKR: equ $52ce
 903                     
 904                     ; TIM1 Dead-time register 
 905       000052cf      TIM1_DTR: equ $52cf
 906                     
 907                     ; TIM1 Output idle state register 
 908       000052d0      TIM1_OISR: equ $52d0
 909                     
 910                     ; TIM1 DMA1 control register 1 
 911       000052d1      TIM1_DCR1: equ $52d1
 912                     
 913                     ; TIM1 DMA1 control register 2 
 914       000052d2      TIM1_DCR2: equ $52d2
 915                     
 916                     ; TIM1 DMA1 address for burst mode 
 917       000052d3      TIM1_DMA1R: equ $52d3
 918                     
 919                     ; 8-Bit  Timer 4 (TIM4) 
 920                     ;***************************************************************
 921                     
 922                     ; TIM4 Control Register 1 
 923       000052e0      TIM4_CR1: equ $52e0
 924                     
 925                     ; TIM4 Control Register 2 
 926       000052e1      TIM4_CR2: equ $52e1
 927                     
 928                     ; TIM4 Slave Mode Control Register 
 929       000052e2      TIM4_SMCR: equ $52e2
 930                     
 931                     ; TIM4 DMA request Enable Register 
 932       000052e3      TIM4_DER: equ $52e3
 933                     
 934                     ; TIM4 Interrupt Enable Register 
 935       000052e4      TIM4_IER: equ $52e4
 936                     
 937                     ; TIM4 Status Register 1 
 938       000052e5      TIM4_SR1: equ $52e5
 939                     
 940                     ; TIM4 Event Generation Register 
 941       000052e6      TIM4_EGR: equ $52e6
 942                     
 943                     ; TIM4 Counter 
 944       000052e7      TIM4_CNTR: equ $52e7
 945                     
 946                     ; TIM4 Prescaler Register 
 947       000052e8      TIM4_PSCR: equ $52e8
 948                     
 949                     ; TIM4 Auto-Reload Register 
 950       000052e9      TIM4_ARR: equ $52e9
 951                     
 952                     ; Infra Red Interface (IR) 
 953                     ;***************************************************************
 954                     
 955                     ; Infra-red control register 
 956       000052ff      IR_CR: equ $52ff
 957                     
 958                     ; 16-Bit  Timer 5 (TIM5) 
 959                     ;***************************************************************
 960                     
 961                     ; TIM5 Control register 1 
 962       00005300      TIM5_CR1: equ $5300
 963                     
 964                     ; TIM5 Control register 2 
 965       00005301      TIM5_CR2: equ $5301
 966                     
 967                     ; TIM5 Slave Mode Control register 
 968       00005302      TIM5_SMCR: equ $5302
 969                     
 970                     ; TIM5 External trigger register 
 971       00005303      TIM5_ETR: equ $5303
 972                     
 973                     ; TIM5 DMA request enable register 
 974       00005304      TIM5_DER: equ $5304
 975                     
 976                     ; TIM5 Interrupt enable register 
 977       00005305      TIM5_IER: equ $5305
 978                     
 979                     ; TIM5 Status register 1 
 980       00005306      TIM5_SR1: equ $5306
 981                     
 982                     ; TIM5 Status register 2 
 983       00005307      TIM5_SR2: equ $5307
 984                     
 985                     ; TIM5 Event Generation register 
 986       00005308      TIM5_EGR: equ $5308
 987                     
 988                     ; TIM5 Capture/Compare mode register 1 
 989       00005309      TIM5_CCMR1: equ $5309
 990                     
 991                     ; TIM5 Capture/Compare mode register 2 
 992       0000530a      TIM5_CCMR2: equ $530a
 993                     
 994                     ; TIM5 Capture/Compare enable register 1 
 995       0000530b      TIM5_CCER1: equ $530b
 996                     
 997                     ; TIM5 Counter 
 998       0000530c      TIM5_CNTR: equ $530c
 999                     ; TIM5 Counter High 
1000       0000530c      TIM5_CNTRH: equ $530c
1001                     ; TIM5 Counter Low 
1002       0000530d      TIM5_CNTRL: equ $530d
1003                     
1004                     ; TIM5 Prescaler register 
1005       0000530e      TIM5_PSCR: equ $530e
1006                     
1007                     ; TIM5 Auto-reload register 
1008       0000530f      TIM5_ARR: equ $530f
1009                     ; TIM5 Auto-Reload Register High 
1010       0000530f      TIM5_ARRH: equ $530f
1011                     ; TIM5 Auto-Reload Register Low 
1012       00005310      TIM5_ARRL: equ $5310
1013                     
1014                     ; TIM5 Capture/Compare register 1 
1015       00005311      TIM5_CCR1: equ $5311
1016                     ; TIM5 Capture/Compare Register 1 High 
1017       00005311      TIM5_CCR1H: equ $5311
1018                     ; TIM5 Capture/Compare Register 1 Low 
1019       00005312      TIM5_CCR1L: equ $5312
1020                     
1021                     ; TIM5 Capture/Compare register 2 
1022       00005313      TIM5_CCR2: equ $5313
1023                     ; TIM5 Capture/Compare Register 2 High 
1024       00005313      TIM5_CCR2H: equ $5313
1025                     ; TIM5 Capture/Compare Register 2 Low 
1026       00005314      TIM5_CCR2L: equ $5314
1027                     
1028                     ; TIM5 Break register 
1029       00005315      TIM5_BKR: equ $5315
1030                     
1031                     ; TIM5 Output idle state register 
1032       00005316      TIM5_OISR: equ $5316
1033                     
1034                     ; Analog to digital converter 1 (ADC1) 
1035                     ;***************************************************************
1036                     
1037                     ; ADC1 Configuration register 1 
1038       00005340      ADC1_CR1: equ $5340
1039                     
1040                     ; ADC1 Configuration register 2 
1041       00005341      ADC1_CR2: equ $5341
1042                     
1043                     ; ADC1 Configuration register 3 
1044       00005342      ADC1_CR3: equ $5342
1045                     
1046                     ; ADC1 status register 
1047       00005343      ADC1_SR: equ $5343
1048                     
1049                     ; ADC1 Data register 
1050       00005344      ADC1_DR: equ $5344
1051                     ; ADC Data Register High 
1052       00005344      ADC1_DRH: equ $5344
1053                     ; ADC Data Register Low 
1054       00005345      ADC1_DRL: equ $5345
1055                     
1056                     ; ADC1 high threshold register 
1057       00005346      ADC1_HTR: equ $5346
1058                     ; ADC High Threshold Register High 
1059       00005346      ADC1_HTRH: equ $5346
1060                     ; ADC High Threshold Register Low 
1061       00005347      ADC1_HTRL: equ $5347
1062                     
1063                     ; ADC1 low threshold register 
1064       00005348      ADC1_LTR: equ $5348
1065                     ; ADC Low Threshold Register High 
1066       00005348      ADC1_LTRH: equ $5348
1067                     ; ADC Low Threshold Register Low 
1068       00005349      ADC1_LTRL: equ $5349
1069                     
1070                     ; ADC1 channel sequence 1 register 
1071       0000534a      ADC1_SQR1: equ $534a
1072                     
1073                     ; ADC1 channel sequence 2 register 
1074       0000534b      ADC1_SQR2: equ $534b
1075                     
1076                     ; ADC1 channel sequence 3 register 
1077       0000534c      ADC1_SQR3: equ $534c
1078                     
1079                     ; ADC1 channel sequence 4 register 
1080       0000534d      ADC1_SQR4: equ $534d
1081                     
1082                     ; ADC1 Trigger disable 1 
1083       0000534e      ADC1_TRIGR1: equ $534e
1084                     
1085                     ; ADC1 Trigger disable 2 
1086       0000534f      ADC1_TRIGR2: equ $534f
1087                     
1088                     ; ADC1 Trigger disable 3 
1089       00005350      ADC1_TRIGR3: equ $5350
1090                     
1091                     ; ADC1 Trigger disable 4 
1092       00005351      ADC1_TRIGR4: equ $5351
1093                     
1094                     ; Digital to analog converter (DAC) 
1095                     ;***************************************************************
1096                     
1097                     ; DAC channel 1 control register 1 
1098       00005380      DAC_CH1CR1: equ $5380
1099                     
1100                     ; DAC channel 1 control register 2 
1101       00005381      DAC_CH1CR2: equ $5381
1102                     
1103                     ; DAC channel 2 control register 1 
1104       00005382      DAC_CH2CR1: equ $5382
1105                     
1106                     ; DAC channel 2 control register 2 
1107       00005383      DAC_CH2CR2: equ $5383
1108                     
1109                     ; DAC software trigger register 
1110       00005384      DAC_SWTRIGR: equ $5384
1111                     
1112                     ; DAC status register 
1113       00005385      DAC_SR: equ $5385
1114                     
1115                     ; DAC channel 1 right aligned data holding register 
1116       00005388      DAC_CH1RDHR: equ $5388
1117                     ; DAC channel 1 right aligned data holding register high 
1118       00005388      DAC_CH1RDHRH: equ $5388
1119                     ; DAC channel 1 right aligned data holding register low 
1120       00005389      DAC_CH1RDHRL: equ $5389
1121                     
1122                     ; DAC channel 1 left aligned data holding register 
1123       0000538c      DAC_CH1LDHR: equ $538c
1124                     ; DAC channel 1 left aligned data holding register high 
1125       0000538c      DAC_CH1LDHRH: equ $538c
1126                     ; DAC channel 1 left aligned data holding register low 
1127       0000538d      DAC_CH1LDHRL: equ $538d
1128                     
1129                     ; DAC channel 1 8-bit data holding register 
1130       00005390      DAC_CH1DHR8: equ $5390
1131                     
1132                     ; DAC channel 2 right aligned data holding register 
1133       00005394      DAC_CH2RDHR: equ $5394
1134                     ; DAC channel 2 right aligned data holding register high 
1135       00005394      DAC_CH2RDHRH: equ $5394
1136                     ; DAC channel 2 right aligned data holding register low 
1137       00005395      DAC_CH2RDHRL: equ $5395
1138                     
1139                     ; DAC channel 2 left aligned data holding register 
1140       00005398      DAC_CH2LDHR: equ $5398
1141                     ; DAC channel 2 left aligned data holding register high 
1142       00005398      DAC_CH2LDHRH: equ $5398
1143                     ; DAC channel 2 left aligned data holding register low 
1144       00005399      DAC_CH2LDHRL: equ $5399
1145                     
1146                     ; DAC channel 2 8-bit data holding register 
1147       0000539c      DAC_CH2DHR8: equ $539c
1148                     
1149                     ; DAC channel 1 right aligned data holding register 
1150       000053a0      DAC_DCH1RDHR: equ $53a0
1151                     ; DAC channel 1 right aligned data holding register high 
1152       000053a0      DAC_DCH1RDHRH: equ $53a0
1153                     ; DAC channel 1 right aligned data holding register low 
1154       000053a1      DAC_DCH1RDHRL: equ $53a1
1155                     
1156                     ; DAC channel 2 right aligned data holding register 
1157       000053a2      DAC_DCH2RDHR: equ $53a2
1158                     ; DAC channel 2 right aligned data holding register high 
1159       000053a2      DAC_DCH2RDHRH: equ $53a2
1160                     ; DAC channel 2 right aligned data holding register low 
1161       000053a3      DAC_DCH2RDHRL: equ $53a3
1162                     
1163                     ; DAC channel 1 left aligned data holding register 
1164       000053a4      DAC_DCH1LDHR: equ $53a4
1165                     ; DAC channel 1 left aligned data holding register high 
1166       000053a4      DAC_DCH1LDHRH: equ $53a4
1167                     ; DAC channel 1 left aligned data holding register low 
1168       000053a5      DAC_DCH1LDHRL: equ $53a5
1169                     
1170                     ; DAC channel 2 left aligned data holding register 
1171       000053a6      DAC_DCH2LDHR: equ $53a6
1172                     ; DAC channel 2 left aligned data holding register high 
1173       000053a6      DAC_DCH2LDHRH: equ $53a6
1174                     ; DAC channel 2 left aligned data holding register low 
1175       000053a7      DAC_DCH2LDHRL: equ $53a7
1176                     
1177                     ; DAC channel 1 8-bit data holding register 
1178       000053a8      DAC_DCH1DHR8: equ $53a8
1179                     
1180                     ; DAC channel 2 8-bit data holding register 
1181       000053a9      DAC_DCH2DHR8: equ $53a9
1182                     
1183                     ; DAC channel 1 data output register 
1184       000053ac      DAC_CH1DOR: equ $53ac
1185                     ; DAC channel 1 data output register high 
1186       000053ac      DAC_CH1DORH: equ $53ac
1187                     ; DAC channel 1 data output register low 
1188       000053ad      DAC_CH1DORL: equ $53ad
1189                     
1190                     ; DAC channel 2 data output register 
1191       000053b0      DAC_CH2DOR: equ $53b0
1192                     ; DAC channel 2 data output register high 
1193       000053b0      DAC_CH2DORH: equ $53b0
1194                     ; DAC channel 2 data output register low 
1195       000053b1      DAC_CH2DORL: equ $53b1
1196                     
1197                     ; Serial Peripheral Interface 2 (SPI2) 
1198                     ;***************************************************************
1199                     
1200                     ; SPI2 Control Register 1 
1201       000053c0      SPI2_CR1: equ $53c0
1202                     
1203                     ; SPI2 Control Register 2 
1204       000053c1      SPI2_CR2: equ $53c1
1205                     
1206                     ; SPI2 Interrupt Control Register 
1207       000053c2      SPI2_ICR: equ $53c2
1208                     
1209                     ; SPI2 Status Register 
1210       000053c3      SPI2_SR: equ $53c3
1211                     
1212                     ; SPI2 Data Register 
1213       000053c4      SPI2_DR: equ $53c4
1214                     
1215                     ; SPI2 CRC Polynomial Register 
1216       000053c5      SPI2_CRCPR: equ $53c5
1217                     
1218                     ; SPI2 Rx CRC Register 
1219       000053c6      SPI2_RXCRCR: equ $53c6
1220                     
1221                     ; SPI2 Tx CRC Register 
1222       000053c7      SPI2_TXCRCR: equ $53c7
1223                     
1224                     ; Universal synch/asynch receiver transmitter 2 (USART2) 
1225                     ;***************************************************************
1226                     
1227                     ; USART2 Status Register 
1228       000053e0      USART2_SR: equ $53e0
1229                     
1230                     ; USART2 Data Register 
1231       000053e1      USART2_DR: equ $53e1
1232                     
1233                     ; USART2 Baud Rate Register 1 
1234       000053e2      USART2_BRR1: equ $53e2
1235                     
1236                     ; USART2 Baud Rate Register 2 
1237       000053e3      USART2_BRR2: equ $53e3
1238                     
1239                     ; USART2 Control Register 1 
1240       000053e4      USART2_CR1: equ $53e4
1241                     
1242                     ; USART2 Control Register 2 
1243       000053e5      USART2_CR2: equ $53e5
1244                     
1245                     ; USART2 Control Register 3 
1246       000053e6      USART2_CR3: equ $53e6
1247                     
1248                     ; USART2 Control Register 4 
1249       000053e7      USART2_CR4: equ $53e7
1250                     
1251                     ; USART2 Control Register 5 
1252       000053e8      USART2_CR5: equ $53e8
1253                     
1254                     ; USART2 Guard time Register 
1255       000053e9      USART2_GTR: equ $53e9
1256                     
1257                     ; USART2 Prescaler Register 
1258       000053ea      USART2_PSCR: equ $53ea
1259                     
1260                     ; Universal synch/asynch receiver transmitter 3 (USART3) 
1261                     ;***************************************************************
1262                     
1263                     ; USART3 Status Register 
1264       000053f0      USART3_SR: equ $53f0
1265                     
1266                     ; USART3 Data Register 
1267       000053f1      USART3_DR: equ $53f1
1268                     
1269                     ; USART3 Baud Rate Register 1 
1270       000053f2      USART3_BRR1: equ $53f2
1271                     
1272                     ; USART3 Baud Rate Register 2 
1273       000053f3      USART3_BRR2: equ $53f3
1274                     
1275                     ; USART3 Control Register 1 
1276       000053f4      USART3_CR1: equ $53f4
1277                     
1278                     ; USART3 Control Register 2 
1279       000053f5      USART3_CR2: equ $53f5
1280                     
1281                     ; USART3 Control Register 3 
1282       000053f6      USART3_CR3: equ $53f6
1283                     
1284                     ; USART3 Control Register 4 
1285       000053f7      USART3_CR4: equ $53f7
1286                     
1287                     ; USART3 Control Register 5 
1288       000053f8      USART3_CR5: equ $53f8
1289                     
1290                     ; USART3 Guard time Register 
1291       000053f9      USART3_GTR: equ $53f9
1292                     
1293                     ; USART3 Prescaler Register 
1294       000053fa      USART3_PSCR: equ $53fa
1295                     
1296                     ; Routing interface (RI) 
1297                     ;***************************************************************
1298                     
1299                     ; Timer input capture routing register 1 
1300       00005431      RI_ICR1: equ $5431
1301                     
1302                     ; Timer input capture routing register 2 
1303       00005432      RI_ICR2: equ $5432
1304                     
1305                     ; I/O input register 1 
1306       00005433      RI_IOIR1: equ $5433
1307                     
1308                     ; I/O input register 2 
1309       00005434      RI_IOIR2: equ $5434
1310                     
1311                     ; I/O input register 3 
1312       00005435      RI_IOIR3: equ $5435
1313                     
1314                     ; I/O control mode register 1 
1315       00005436      RI_IOCMR1: equ $5436
1316                     
1317                     ; I/O control mode register 2 
1318       00005437      RI_IOCMR2: equ $5437
1319                     
1320                     ; I/O control mode register 3 
1321       00005438      RI_IOCMR3: equ $5438
1322                     
1323                     ; I/O switch register 1 
1324       00005439      RI_IOSR1: equ $5439
1325                     
1326                     ; I/O switch register 2 
1327       0000543a      RI_IOSR2: equ $543a
1328                     
1329                     ; I/O switch register 3 
1330       0000543b      RI_IOSR3: equ $543b
1331                     
1332                     ; I/O group control register 
1333       0000543c      RI_IOGCR: equ $543c
1334                     
1335                     ; Analog switch register 1 
1336       0000543d      RI_ASCR1: equ $543d
1337                     
1338                     ; Analog switch register 2 
1339       0000543e      RI_ASCR2: equ $543e
1340                     
1341                     ; Resistor control register 
1342       0000543f      RI_RCR: equ $543f
1343                     
1344                     ; Comparators (COMP) 
1345                     ;***************************************************************
1346                     
1347                     ; Comparator control and status register 1 
1348       00005440      COMP_CSR1: equ $5440
1349                     
1350                     ; Comparator control and status register 2 
1351       00005441      COMP_CSR2: equ $5441
1352                     
1353                     ; Comparator control and status register 3 
1354       00005442      COMP_CSR3: equ $5442
1355                     
1356                     ; Comparator control and status register 4 
1357       00005443      COMP_CSR4: equ $5443
1358                     
1359                     ; Comparator control and status register 5 
1360       00005444      COMP_CSR5: equ $5444
1361                     
1362                     ; Global configuration register (CFG) 
1363                     ;***************************************************************
1364                     
1365                     ; CFG Global configuration register 
1366       00007f60      CFG_GCR: equ $7f60
1367                     
1368                     ; Interrupt Software Priority Registers (ITC) 
1369                     ;***************************************************************
1370                     
1371                     ; Interrupt Software priority register 1 
1372       00007f70      ITC_SPR1: equ $7f70
1373                     
1374                     ; Interrupt Software priority register 2 
1375       00007f71      ITC_SPR2: equ $7f71
1376                     
1377                     ; Interrupt Software priority register 3 
1378       00007f72      ITC_SPR3: equ $7f72
1379                     
1380                     ; Interrupt Software priority register 4 
1381       00007f73      ITC_SPR4: equ $7f73
1382                     
1383                     ; Interrupt Software priority register 5 
1384       00007f74      ITC_SPR5: equ $7f74
1385                     
1386                     ; Interrupt Software priority register 6 
1387       00007f75      ITC_SPR6: equ $7f75
1388                     
1389                     ; Interrupt Software priority register 7 
1390       00007f76      ITC_SPR7: equ $7f76
1391                     
1392                     ; Interrupt Software priority register 8 
1393       00007f77      ITC_SPR8: equ $7f77
1394                     
1395                     ;#endif ; __STM8L151C__ 
   8                     	
   9  0000               _delay_ms:
  10  0000 89                pushw x
  11  0001 9089              pushw y
  12  0003               start_delay:
  13  0003 90ae00fa          ldw y, #250
  14  0007               loop:
  15  0007 72a20001          subw y,#1
  16  000b 26fa              jrne loop
  17  000d 5a                decw x
  18  000e 26f3              jrne start_delay
  19  0010 9085              popw y
  20  0012 85                popw x
  21  0013 81                ret
  22                     
  23                         end
