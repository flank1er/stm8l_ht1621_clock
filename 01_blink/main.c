#include <stdint.h>
#include "iostm8l152x.h"
#include "ht1621.h"
#include "main.h"

extern void delay_ms(uint16_t value);

main()
{
  	uint16_t tick=0;
    // ------- CLK Setup ---------------
    CLK_DIVR = 4;         // SYSCLK=1MHz
	CLK_PCKENR2 |=  0x02;                   // Enable TIM1
	// parking GPIO
    // to ground Port A
    PA_DDR = 0xFF; PA_CR1 = 0xFF; PA_ODR = 0;
    // to ground Port B
    PB_DDR = 0xFF; PB_CR1 = 0xFF; PB_ODR = 0;
    // to ground Port C
    PC_DDR = 0xFF; PC_CR1 = 0xFF; PC_ODR = 0;
    // to ground Port D
    PD_DDR = 0xFF; PD_CR1 = 0xFF; PD_ODR = 0;
    // to ground Port E
    PE_DDR = 0xFF; PE_CR1 = 0xFF; PE_ODR = 0;
    // to ground Port F
    PF_DDR = 0xFF; PF_CR1 = 0xFF; PF_ODR = 0;
    // ------- GPIO setup --------------
	// Soft SPI GPIO Setup
	PB_DDR = ((1<<CS) | (1<<WR) | (1<<DATA)); // Push-Pull Mode
    PB_CR1 = ((1<<CS) | (1<<WR) | (1<<DATA)); //
    PB_CR2 = ((1<<CS) | (1<<WR) | (1<<DATA)); // Speed up 10 MHz
	// LED
    PB_DDR |= (1<<LED);
    PB_CR1 |= (1<<LED);
	//====== TIM1 Setup ==========
    TIM1_SR1   = 0x0;                       // Clear Pending Bit
    TIM1_CR1   = 0x0;                       // Clear TIM1_CR1
    TIM1_CR2   = 0x0;                       // Clear TIM1_CR2
    TIM1_PSCRH = 0x0;  TIM1_PSCRL = 63;     // Prescaler = 64
    TIM1_ARRH  = 0x7a; TIM1_ARRL  = 0x12;   // (2*10^6)/prescaler(=64) =31250 -> 0x7A12 -> freq Timer IRQ =1Hz
    TIM1_IER   = 0x01;                      // set UIE flag, enable interrupt
    TIM1_CR1  |= 0x01;                      // set CEN flag, start timer
    // main loop
    ht1621_init();	
    ht1621_clear();

    for(;;) {
        PB_ODR ^=(1<<LED);
        //delay_ms(1000);
		ht1621_print(tick++);
		_asm("wfi");
    }
}