   1                     ; C Compiler for STM8 (COSMIC Software)
   2                     ; Parser V4.12.6 - 16 Dec 2021
   3                     ; Generator (Limited) V4.5.4 - 16 Dec 2021
5158                     ; 8 main()
5158                     ; 9 {
5160                     	switch	.text
5161  0000               _main:
5163  0000 89            	pushw	x
5164       00000002      OFST:	set	2
5167                     ; 10   	uint16_t tick=0;
5169  0001 5f            	clrw	x
5170  0002 1f01          	ldw	(OFST-1,sp),x
5172                     ; 12     CLK_DIVR = 4;         // SYSCLK=1MHz
5174  0004 350450c0      	mov	_CLK_DIVR,#4
5175                     ; 13 	CLK_PCKENR2 |=  0x02;                   // Enable TIM1
5177  0008 721250c4      	bset	_CLK_PCKENR2,#1
5178                     ; 16     PA_DDR = 0xFF; PA_CR1 = 0xFF; PA_ODR = 0;
5180  000c 35ff5002      	mov	_PA_DDR,#255
5183  0010 35ff5003      	mov	_PA_CR1,#255
5186  0014 725f5000      	clr	_PA_ODR
5187                     ; 18     PB_DDR = 0xFF; PB_CR1 = 0xFF; PB_ODR = 0;
5189  0018 35ff5007      	mov	_PB_DDR,#255
5192  001c 35ff5008      	mov	_PB_CR1,#255
5195  0020 725f5005      	clr	_PB_ODR
5196                     ; 20     PC_DDR = 0xFF; PC_CR1 = 0xFF; PC_ODR = 0;
5198  0024 35ff500c      	mov	_PC_DDR,#255
5201  0028 35ff500d      	mov	_PC_CR1,#255
5204  002c 725f500a      	clr	_PC_ODR
5205                     ; 22     PD_DDR = 0xFF; PD_CR1 = 0xFF; PD_ODR = 0;
5207  0030 35ff5011      	mov	_PD_DDR,#255
5210  0034 35ff5012      	mov	_PD_CR1,#255
5213  0038 725f500f      	clr	_PD_ODR
5214                     ; 24     PE_DDR = 0xFF; PE_CR1 = 0xFF; PE_ODR = 0;
5216  003c 35ff5016      	mov	_PE_DDR,#255
5219  0040 35ff5017      	mov	_PE_CR1,#255
5222  0044 725f5014      	clr	_PE_ODR
5223                     ; 26     PF_DDR = 0xFF; PF_CR1 = 0xFF; PF_ODR = 0;
5225  0048 35ff501b      	mov	_PF_DDR,#255
5228  004c 35ff501c      	mov	_PF_CR1,#255
5231  0050 725f5019      	clr	_PF_ODR
5232                     ; 29 	PB_DDR = ((1<<CS) | (1<<WR) | (1<<DATA)); // Push-Pull Mode
5234  0054 35705007      	mov	_PB_DDR,#112
5235                     ; 30     PB_CR1 = ((1<<CS) | (1<<WR) | (1<<DATA)); //
5237  0058 35705008      	mov	_PB_CR1,#112
5238                     ; 31     PB_CR2 = ((1<<CS) | (1<<WR) | (1<<DATA)); // Speed up 10 MHz
5240  005c 35705009      	mov	_PB_CR2,#112
5241                     ; 33     PB_DDR |= (1<<LED);
5243  0060 72105007      	bset	_PB_DDR,#0
5244                     ; 34     PB_CR1 |= (1<<LED);
5246  0064 72105008      	bset	_PB_CR1,#0
5247                     ; 36     TIM1_SR1   = 0x0;                       // Clear Pending Bit
5249  0068 725f52b6      	clr	_TIM1_SR1
5250                     ; 37     TIM1_CR1   = 0x0;                       // Clear TIM1_CR1
5252  006c 725f52b0      	clr	_TIM1_CR1
5253                     ; 38     TIM1_CR2   = 0x0;                       // Clear TIM1_CR2
5255  0070 725f52b1      	clr	_TIM1_CR2
5256                     ; 39     TIM1_PSCRH = 0x0;  TIM1_PSCRL = 63;     // Prescaler = 64
5258  0074 725f52c1      	clr	_TIM1_PSCRH
5261  0078 353f52c2      	mov	_TIM1_PSCRL,#63
5262                     ; 40     TIM1_ARRH  = 0x7a; TIM1_ARRL  = 0x12;   // (2*10^6)/prescaler(=64) =31250 -> 0x7A12 -> freq Timer IRQ =1Hz
5264  007c 357a52c3      	mov	_TIM1_ARRH,#122
5267  0080 351252c4      	mov	_TIM1_ARRL,#18
5268                     ; 41     TIM1_IER   = 0x01;                      // set UIE flag, enable interrupt
5270  0084 350152b5      	mov	_TIM1_IER,#1
5271                     ; 42     TIM1_CR1  |= 0x01;                      // set CEN flag, start timer
5273  0088 721052b0      	bset	_TIM1_CR1,#0
5274                     ; 44     ht1621_init();	
5276  008c cd0000        	call	_ht1621_init
5278                     ; 45     ht1621_clear();
5280  008f cd0000        	call	_ht1621_clear
5282  0092               L3153:
5283                     ; 48         PB_ODR ^=(1<<LED);
5285  0092 90105005      	bcpl	_PB_ODR,#0
5286                     ; 50 		ht1621_print(tick++);
5288  0096 1e01          	ldw	x,(OFST-1,sp)
5289  0098 1c0001        	addw	x,#1
5290  009b 1f01          	ldw	(OFST-1,sp),x
5291  009d 1d0001        	subw	x,#1
5293  00a0 cd0000        	call	_ht1621_print
5295                     ; 51 		_asm("wfi");
5298  00a3 8f            wfi
5301  00a4 20ec          	jra	L3153
5314                     	xdef	_main
5315                     	xref	_ht1621_init
5316                     	xref	_ht1621_clear
5317                     	xref	_ht1621_print
5336                     	end
