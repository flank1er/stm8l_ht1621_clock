   1                     ; C Compiler for STM8 (COSMIC Software)
   2                     ; Parser V4.12.6 - 16 Dec 2021
   3                     ; Generator (Limited) V4.5.4 - 16 Dec 2021
5087                     .const:	section	.text
5088  0000               _digits:
5089  0000 7d            	dc.b	125
5090  0001 60            	dc.b	96
5091  0002 3e            	dc.b	62
5092  0003 7a            	dc.b	122
5093  0004 63            	dc.b	99
5094  0005 5b            	dc.b	91
5095  0006 5f            	dc.b	95
5096  0007 70            	dc.b	112
5097  0008 7f            	dc.b	127
5098  0009 7b            	dc.b	123
5128                     ; 14 void ht1621_init(void) {
5130                     	switch	.text
5131  0000               _ht1621_init:
5135                     ; 15     ht1621_send_cmd(BIAS);
5137  0000 a652          	ld	a,#82
5138  0002 cd00e7        	call	_ht1621_send_cmd
5140                     ; 16     ht1621_send_cmd(RC256);
5142  0005 a630          	ld	a,#48
5143  0007 cd00e7        	call	_ht1621_send_cmd
5145                     ; 17     ht1621_send_cmd(SYSDIS);
5147  000a 4f            	clr	a
5148  000b cd00e7        	call	_ht1621_send_cmd
5150                     ; 18     ht1621_send_cmd(WDTDIS1);
5152  000e a60a          	ld	a,#10
5153  0010 cd00e7        	call	_ht1621_send_cmd
5155                     ; 19     ht1621_send_cmd(SYSEN);
5157  0013 a602          	ld	a,#2
5158  0015 cd00e7        	call	_ht1621_send_cmd
5160                     ; 20     ht1621_send_cmd(LCDON);
5162  0018 a606          	ld	a,#6
5163  001a cd00e7        	call	_ht1621_send_cmd
5165                     ; 21 }
5168  001d 81            	ret
5192                     ; 23 void ht1621_off() {
5193                     	switch	.text
5194  001e               _ht1621_off:
5198                     ; 24 	ht1621_send_cmd(LCDOFF);
5200  001e a604          	ld	a,#4
5201  0020 cd00e7        	call	_ht1621_send_cmd
5203                     ; 25 }
5206  0023 81            	ret
5241                     ; 27 void ht1621_clear(void) {
5242                     	switch	.text
5243  0024               _ht1621_clear:
5245  0024 88            	push	a
5246       00000001      OFST:	set	1
5249                     ; 29 	for(i=0;i<16;i++) {
5251  0025 0f01          	clr	(OFST+0,sp)
5253  0027               L3353:
5254                     ; 30 		ht1621_send_data((i<<1),0x0);
5256  0027 7b01          	ld	a,(OFST+0,sp)
5257  0029 48            	sll	a
5258  002a 5f            	clrw	x
5259  002b 95            	ld	xh,a
5260  002c cd00c3        	call	_ht1621_send_data
5262                     ; 29 	for(i=0;i<16;i++) {
5264  002f 0c01          	inc	(OFST+0,sp)
5268  0031 7b01          	ld	a,(OFST+0,sp)
5269  0033 a110          	cp	a,#16
5270  0035 25f0          	jrult	L3353
5271                     ; 32 }
5274  0037 84            	pop	a
5275  0038 81            	ret
5328                     ; 34 void ht1621_print_num(uint16_t num){
5329                     	switch	.text
5330  0039               _ht1621_print_num:
5332  0039 89            	pushw	x
5333  003a 89            	pushw	x
5334       00000002      OFST:	set	2
5337                     ; 35 	uint8_t pos=0;
5339  003b 0f02          	clr	(OFST+0,sp)
5341  003d               L7653:
5342                     ; 37         uint8_t rm=num % 10;
5344  003d 1e03          	ldw	x,(OFST+1,sp)
5345  003f a60a          	ld	a,#10
5346  0041 62            	div	x,a
5347  0042 5f            	clrw	x
5348  0043 97            	ld	xl,a
5349  0044 01            	rrwa	x,a
5350  0045 6b01          	ld	(OFST-1,sp),a
5351  0047 02            	rlwa	x,a
5353                     ; 38 		ht1621_send_digit(pos++,rm);
5355  0048 7b01          	ld	a,(OFST-1,sp)
5356  004a 97            	ld	xl,a
5357  004b 7b02          	ld	a,(OFST+0,sp)
5358  004d 0c02          	inc	(OFST+0,sp)
5360  004f 95            	ld	xh,a
5361  0050 ad0e          	call	_ht1621_send_digit
5363                     ; 39         num=num/10;
5365  0052 1e03          	ldw	x,(OFST+1,sp)
5366  0054 a60a          	ld	a,#10
5367  0056 62            	div	x,a
5368  0057 1f03          	ldw	(OFST+1,sp),x
5369                     ; 40     } while (num>0);
5371  0059 1e03          	ldw	x,(OFST+1,sp)
5372  005b 26e0          	jrne	L7653
5373                     ; 41 }
5376  005d 5b04          	addw	sp,#4
5377  005f 81            	ret
5422                     ; 43 void ht1621_send_digit(uint8_t pos,uint8_t digit) {
5423                     	switch	.text
5424  0060               _ht1621_send_digit:
5426  0060 89            	pushw	x
5427       00000000      OFST:	set	0
5430                     ; 44 	ht1621_send_data((pos<<1),digits[digit]);
5432  0061 9f            	ld	a,xl
5433  0062 5f            	clrw	x
5434  0063 97            	ld	xl,a
5435  0064 d60000        	ld	a,(_digits,x)
5436  0067 97            	ld	xl,a
5437  0068 7b01          	ld	a,(OFST+1,sp)
5438  006a 48            	sll	a
5439  006b 95            	ld	xh,a
5440  006c ad55          	call	_ht1621_send_data
5442                     ; 45 }
5445  006e 85            	popw	x
5446  006f 81            	ret
5501                     ; 47 void ht1621_print(uint16_t num) {
5502                     	switch	.text
5503  0070               _ht1621_print:
5505  0070 89            	pushw	x
5506  0071 89            	pushw	x
5507       00000002      OFST:	set	2
5510                     ; 48     uint8_t first=1;
5512  0072 a601          	ld	a,#1
5513  0074 6b02          	ld	(OFST+0,sp),a
5515                     ; 49 	PB_ODR &= ~(1<<CS);
5517  0076 72195005      	bres	_PB_ODR,#4
5518  007a               L5463:
5519                     ; 51         uint8_t rm=num % 10;
5521  007a 1e03          	ldw	x,(OFST+1,sp)
5522  007c a60a          	ld	a,#10
5523  007e 62            	div	x,a
5524  007f 5f            	clrw	x
5525  0080 97            	ld	xl,a
5526  0081 01            	rrwa	x,a
5527  0082 6b01          	ld	(OFST-1,sp),a
5528  0084 02            	rlwa	x,a
5530                     ; 53 		if (first) {
5532  0085 0d02          	tnz	(OFST+0,sp)
5533  0087 271b          	jreq	L3563
5534                     ; 54 		  ht1621_send(0xa0, 3);
5536  0089 aea003        	ldw	x,#40963
5537  008c ad71          	call	_ht1621_send
5539                     ; 55 		  ht1621_send(0, 6);
5541  008e ae0006        	ldw	x,#6
5542  0091 ad6c          	call	_ht1621_send
5544                     ; 56 		  ht1621_send(digits[rm], 8);		 
5546  0093 7b01          	ld	a,(OFST-1,sp)
5547  0095 5f            	clrw	x
5548  0096 97            	ld	xl,a
5549  0097 d60000        	ld	a,(_digits,x)
5550  009a ae0008        	ldw	x,#8
5551  009d 95            	ld	xh,a
5552  009e ad5f          	call	_ht1621_send
5554                     ; 57 		  first=0;
5556  00a0 0f02          	clr	(OFST+0,sp)
5559  00a2 200d          	jra	L5563
5560  00a4               L3563:
5561                     ; 59 		  ht1621_send(digits[rm], 8);		 
5563  00a4 7b01          	ld	a,(OFST-1,sp)
5564  00a6 5f            	clrw	x
5565  00a7 97            	ld	xl,a
5566  00a8 d60000        	ld	a,(_digits,x)
5567  00ab ae0008        	ldw	x,#8
5568  00ae 95            	ld	xh,a
5569  00af ad4e          	call	_ht1621_send
5571  00b1               L5563:
5572                     ; 61         num=num/10;
5574  00b1 1e03          	ldw	x,(OFST+1,sp)
5575  00b3 a60a          	ld	a,#10
5576  00b5 62            	div	x,a
5577  00b6 1f03          	ldw	(OFST+1,sp),x
5578                     ; 62     } while (num>0);  
5580  00b8 1e03          	ldw	x,(OFST+1,sp)
5581  00ba 26be          	jrne	L5463
5582                     ; 63 	PB_ODR |=  (1<<CS);
5584  00bc 72185005      	bset	_PB_ODR,#4
5585                     ; 64 }
5588  00c0 5b04          	addw	sp,#4
5589  00c2 81            	ret
5634                     ; 66 void ht1621_send_data(uint8_t adr, uint8_t value) {
5635                     	switch	.text
5636  00c3               _ht1621_send_data:
5638  00c3 89            	pushw	x
5639       00000000      OFST:	set	0
5642                     ; 67 	adr <<=2;
5644  00c4 0801          	sll	(OFST+1,sp)
5645  00c6 0801          	sll	(OFST+1,sp)
5646                     ; 68     PB_ODR &= ~(1<<CS);
5648  00c8 72195005      	bres	_PB_ODR,#4
5649                     ; 69     ht1621_send(0xa0, 3);
5651  00cc aea003        	ldw	x,#40963
5652  00cf ad2e          	call	_ht1621_send
5654                     ; 70     ht1621_send(adr, 6);
5656  00d1 7b01          	ld	a,(OFST+1,sp)
5657  00d3 ae0006        	ldw	x,#6
5658  00d6 95            	ld	xh,a
5659  00d7 ad26          	call	_ht1621_send
5661                     ; 71     ht1621_send(value, 8);
5663  00d9 7b02          	ld	a,(OFST+2,sp)
5664  00db ae0008        	ldw	x,#8
5665  00de 95            	ld	xh,a
5666  00df ad1e          	call	_ht1621_send
5668                     ; 72     PB_ODR |=  (1<<CS);
5670  00e1 72185005      	bset	_PB_ODR,#4
5671                     ; 73 }
5674  00e5 85            	popw	x
5675  00e6 81            	ret
5711                     ; 75 void ht1621_send_cmd(uint8_t cmd) {
5712                     	switch	.text
5713  00e7               _ht1621_send_cmd:
5715  00e7 88            	push	a
5716       00000000      OFST:	set	0
5719                     ; 76 	PB_ODR &= ~(1<<CS);
5721  00e8 72195005      	bres	_PB_ODR,#4
5722                     ; 77 	ht1621_send(0x80,4);
5724  00ec ae8004        	ldw	x,#32772
5725  00ef ad0e          	call	_ht1621_send
5727                     ; 78 	ht1621_send(cmd,8);
5729  00f1 7b01          	ld	a,(OFST+1,sp)
5730  00f3 ae0008        	ldw	x,#8
5731  00f6 95            	ld	xh,a
5732  00f7 ad06          	call	_ht1621_send
5734                     ; 79 	PB_ODR |=  (1<<CS);
5736  00f9 72185005      	bset	_PB_ODR,#4
5737                     ; 80 }
5740  00fd 84            	pop	a
5741  00fe 81            	ret
5794                     ; 82 void ht1621_send(uint8_t data, uint8_t len) {
5795                     	switch	.text
5796  00ff               _ht1621_send:
5798  00ff 89            	pushw	x
5799  0100 88            	push	a
5800       00000001      OFST:	set	1
5803                     ; 84 	for(i=0;i<len;i++) {
5805  0101 0f01          	clr	(OFST+0,sp)
5808  0103 2021          	jra	L1573
5809  0105               L5473:
5810                     ; 85         PB_ODR=(data & 0x80) ? PB_ODR | (1<<DATA) : PB_ODR & ~(1<<DATA);
5812  0105 7b02          	ld	a,(OFST+1,sp)
5813  0107 a580          	bcp	a,#128
5814  0109 2707          	jreq	L62
5815  010b c65005        	ld	a,_PB_ODR
5816  010e aa40          	or	a,#64
5817  0110 2005          	jra	L03
5818  0112               L62:
5819  0112 c65005        	ld	a,_PB_ODR
5820  0115 a4bf          	and	a,#191
5821  0117               L03:
5822  0117 c75005        	ld	_PB_ODR,a
5823                     ; 86 		PB_ODR &= ~(1<<WR);
5825  011a 721b5005      	bres	_PB_ODR,#5
5826                     ; 87 		PB_ODR |= (1<<WR);
5828  011e 721a5005      	bset	_PB_ODR,#5
5829                     ; 88         data=(data<<1);
5831  0122 0802          	sll	(OFST+1,sp)
5832                     ; 84 	for(i=0;i<len;i++) {
5834  0124 0c01          	inc	(OFST+0,sp)
5836  0126               L1573:
5839  0126 7b01          	ld	a,(OFST+0,sp)
5840  0128 1103          	cp	a,(OFST+2,sp)
5841  012a 25d9          	jrult	L5473
5842                     ; 90 }
5845  012c 5b03          	addw	sp,#3
5846  012e 81            	ret
5871                     	xdef	_ht1621_off
5872                     	xdef	_ht1621_send_data
5873                     	xdef	_ht1621_send
5874                     	xdef	_ht1621_send_cmd
5875                     	xdef	_digits
5876                     	xdef	_ht1621_print_num
5877                     	xdef	_ht1621_send_digit
5878                     	xdef	_ht1621_init
5879                     	xdef	_ht1621_clear
5880                     	xdef	_ht1621_print
5899                     	end
