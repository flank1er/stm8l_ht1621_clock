#include <stdint.h>
#include "iostm8l152x.h"
#include "ht1621.h"

#define num_len 10
#define MINUS	0x02

#define get_abs(n) ((n) < 0 ? -(n) : (n))

#define  BIAS     0x52             //0b1000 0101 0010  1/3duty 4com
#define  SYSDIS   0X00             //0b1000 0000 0000  ???????LCD?????
#define  SYSEN    0X02             //0b1000 0000 0010 ???????
#define  LCDOFF   0X04             //0b1000 0000 0100  ?LCD??
#define  LCDON    0X06             //0b1000 0000 0110  ??LCD??
#define  XTAL     0x28             //0b1000 0010 1000 ?????
#define  RC256    0X30             //0b1000 0011 0000  ????
#define  TONEON   0X12             //0b1000 0001 0010  ??????
#define  TONEOFF  0X10             //0b1000 0001 0000 ??????
#define  WDTDIS1  0X0A             //0b1000 0000 1010  ?????
#define  BUFFERSIZE 12

static uint8_t digits[10] = {0x7d,0x60,0x3e,0x7a,0x63,0x5b,0x5f,0x70,0x7f,0x7b};

void ht1621_send_cmd(uint8_t cmd);
void ht1621_send(uint8_t data, uint8_t len);
void ht1621_send_data(uint8_t adr, uint8_t value);
uint8_t ht1621_char_to_seg_bits(uint8_t ch);

void ht1621_init(void) {
    ht1621_send_cmd(BIAS);
    ht1621_send_cmd(RC256);
    ht1621_send_cmd(SYSDIS);
    ht1621_send_cmd(WDTDIS1);
    ht1621_send_cmd(SYSEN);
    ht1621_send_cmd(LCDON);
}

void ht1621_off() {
	ht1621_send_cmd(LCDOFF);
}

void ht1621_clear(void) {
    char i;
	for(i=0;i<16;i++) {
		ht1621_send_data((i<<1),0x0);
	}
}


#pragma section(FLASH_CODE)
void ht1621_print(int num) {

    uint16_t value=get_abs(num);
    uint8_t first=1;
	
	if (num <= 0) 
	  ht1621_clear();
	  
	PB_ODR &= ~(1<<CS);
	do {
        uint8_t rm=value % 10;

		if (first) {
		  ht1621_send(0xa0, 3);
		  ht1621_send(0, 6);
		  ht1621_send(digits[rm], 8);		 
		  first=0;
		} else {
		  ht1621_send(digits[rm], 8);		 
		}
        value=value/10;
    } while (value>0);  
	
	if (num < 0) 
	  ht1621_send(MINUS,8);  

	  
	PB_ODR |=  (1<<CS);
}

void ht1621_send_data(uint8_t adr, uint8_t value) {
	adr <<=2;
    PB_ODR &= ~(1<<CS);
    ht1621_send(0xa0, 3);
    ht1621_send(adr, 6);
    ht1621_send(value, 8);
    PB_ODR |=  (1<<CS);
}

void ht1621_send_cmd(uint8_t cmd) {
	PB_ODR &= ~(1<<CS);
	ht1621_send(0x80,4);
	ht1621_send(cmd,8);
	PB_ODR |=  (1<<CS);
}

void ht1621_send(uint8_t data, uint8_t len) {
	char i;
	for(i=0;i<len;i++) {
        PB_ODR=(data & 0x80) ? PB_ODR | (1<<DATA) : PB_ODR & ~(1<<DATA);
		PB_ODR &= ~(1<<WR);
		PB_ODR |= (1<<WR);
        data=(data<<1);
	}
}


void main_loop(void) {
    uint16_t tick=0;
	uint8_t i=30;
	ht1621_init();	
    ht1621_clear();  
	ht1621_print(0);
    // Switch to LSE (32,768 kHz)
    CLK_SWCR |= (1<<1);                     // set SWEN flag
    CLK_ECKCR |= (1<<2);                    // set LSEON flag
    while (!(CLK_ECKCR & 0x08));            // wait LSERDY flag
    CLK_SWR= 0x08;                          // set LSE
	while (CLK_SWCR & 0x01);				// wait to reset SWBSY flag
    CLK_ICKCR &= ~(1<<0);                   // reset HSION flag
	CLK_DIVR = 0;         					// SYSCLK=LSE
    //==== FLASH Switch Off =======
    FLASH_CR1 |= (1<<3);                    // set EEPM flag
    while (--i > 0);                        // wait after switch off flash memory
    // === MVR Swtch Off ==========
    CLK_REGCSR |= (1<<1);                   // set REGOFF flag
	// === main loop =========
	//FLASH_CR1 |= (1<<2); // set WAITM flag
	TIM4_CR1  |= 0x01;
    for(;;) {
	  _asm("wfe");
	  TIM4_SR1 =0;	 

	  tick++;
	  ht1621_print(tick);
    }
}
#pragma section()
