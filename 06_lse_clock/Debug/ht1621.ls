   1                     ; C Compiler for STM8 (COSMIC Software)
   2                     ; Parser V4.12.6 - 16 Dec 2021
   3                     ; Generator (Limited) V4.5.4 - 16 Dec 2021
5087                     	bsct
5088  0000               L7643_digits:
5089  0000 7d            	dc.b	125
5090  0001 60            	dc.b	96
5091  0002 3e            	dc.b	62
5092  0003 7a            	dc.b	122
5093  0004 63            	dc.b	99
5094  0005 5b            	dc.b	91
5095  0006 5f            	dc.b	95
5096  0007 70            	dc.b	112
5097  0008 7f            	dc.b	127
5098  0009 7b            	dc.b	123
5099  000a               _menu:
5100  000a 00            	dc.b	0
5101  000b               _count:
5102  000b 0000          	dc.w	0
5103  000d               _offset:
5104  000d 0000          	dc.w	0
5134                     ; 32 void ht1621_init(void) {
5136                     	switch	.text
5137  0000               _ht1621_init:
5141                     ; 33     ht1621_send_cmd(BIAS);
5143  0000 a652          	ld	a,#82
5144  0002 cd0169        	call	_ht1621_send_cmd
5146                     ; 34     ht1621_send_cmd(RC256);
5148  0005 a630          	ld	a,#48
5149  0007 cd0169        	call	_ht1621_send_cmd
5151                     ; 35     ht1621_send_cmd(SYSDIS);
5153  000a 4f            	clr	a
5154  000b cd0169        	call	_ht1621_send_cmd
5156                     ; 36     ht1621_send_cmd(WDTDIS1);
5158  000e a60a          	ld	a,#10
5159  0010 cd0169        	call	_ht1621_send_cmd
5161                     ; 37     ht1621_send_cmd(SYSEN);
5163  0013 a602          	ld	a,#2
5164  0015 cd0169        	call	_ht1621_send_cmd
5166                     ; 38     ht1621_send_cmd(LCDON);
5168  0018 a606          	ld	a,#6
5169  001a cd0169        	call	_ht1621_send_cmd
5171                     ; 39 }
5174  001d 81            	ret
5198                     ; 41 void ht1621_off() {
5199                     	switch	.text
5200  001e               _ht1621_off:
5204                     ; 42 	ht1621_send_cmd(LCDOFF);
5206  001e a604          	ld	a,#4
5207  0020 cd0169        	call	_ht1621_send_cmd
5209                     ; 43 }
5212  0023 81            	ret
5247                     ; 45 void ht1621_clear(void) {
5248                     	switch	.text
5249  0024               _ht1621_clear:
5251  0024 88            	push	a
5252       00000001      OFST:	set	1
5255                     ; 47 	for(i=0;i<16;i++) {
5257  0025 0f01          	clr	(OFST+0,sp)
5259  0027               L5353:
5260                     ; 48 		ht1621_send_data((i<<1),0x0);
5262  0027 7b01          	ld	a,(OFST+0,sp)
5263  0029 48            	sll	a
5264  002a 5f            	clrw	x
5265  002b 95            	ld	xh,a
5266  002c cd0145        	call	_ht1621_send_data
5268                     ; 47 	for(i=0;i<16;i++) {
5270  002f 0c01          	inc	(OFST+0,sp)
5274  0031 7b01          	ld	a,(OFST+0,sp)
5275  0033 a110          	cp	a,#16
5276  0035 25f0          	jrult	L5353
5277                     ; 50 }
5280  0037 84            	pop	a
5281  0038 81            	ret
5346                     ; 104 static void ht1621_time(uint16_t t, uint8_t flag) {
5347                     .FLASH_CODE:	section	.text
5348  0000               L3453_ht1621_time:
5350  0000 89            	pushw	x
5351  0001 89            	pushw	x
5352       00000002      OFST:	set	2
5355                     ; 106   d=(uint8_t)(t%60);
5357  0002 a63c          	ld	a,#60
5358  0004 62            	div	x,a
5359  0005 5f            	clrw	x
5360  0006 97            	ld	xl,a
5361  0007 9f            	ld	a,xl
5362  0008 6b01          	ld	(OFST-1,sp),a
5364                     ; 107   z=(uint8_t)(d%10);
5366  000a 7b01          	ld	a,(OFST-1,sp)
5367  000c 5f            	clrw	x
5368  000d 97            	ld	xl,a
5369  000e a60a          	ld	a,#10
5370  0010 62            	div	x,a
5371  0011 5f            	clrw	x
5372  0012 97            	ld	xl,a
5373  0013 9f            	ld	a,xl
5374  0014 6b02          	ld	(OFST+0,sp),a
5376                     ; 109   PB_ODR &= ~(1<<CS);
5378  0016 72195005      	bres	_PB_ODR,#4
5379                     ; 110   ht1621_send(0xa0, 3);
5381  001a aea003        	ldw	x,#40963
5382  001d cd0181        	call	_ht1621_send
5384                     ; 111   ht1621_send(0, 6);
5386  0020 ae0006        	ldw	x,#6
5387  0023 cd0181        	call	_ht1621_send
5389                     ; 112   t=t/60;
5391  0026 1e03          	ldw	x,(OFST+1,sp)
5392  0028 a63c          	ld	a,#60
5393  002a 62            	div	x,a
5394  002b 1f03          	ldw	(OFST+1,sp),x
5395                     ; 113   if (menu == (uint8_t)0x1 && !flag) {
5397  002d b60a          	ld	a,_menu
5398  002f a101          	cp	a,#1
5399  0031 2612          	jrne	L7753
5401  0033 0d07          	tnz	(OFST+5,sp)
5402  0035 260e          	jrne	L7753
5403                     ; 114 	ht1621_send(0, 8);
5405  0037 ae0008        	ldw	x,#8
5406  003a cd0181        	call	_ht1621_send
5408                     ; 115 	ht1621_send(0, 8);	
5410  003d ae0008        	ldw	x,#8
5411  0040 cd0181        	call	_ht1621_send
5414  0043 2024          	jra	L1063
5415  0045               L7753:
5416                     ; 117 	ht1621_send(digits[z], 8);
5418  0045 7b02          	ld	a,(OFST+0,sp)
5419  0047 5f            	clrw	x
5420  0048 97            	ld	xl,a
5421  0049 e600          	ld	a,(L7643_digits,x)
5422  004b ae0008        	ldw	x,#8
5423  004e 95            	ld	xh,a
5424  004f cd0181        	call	_ht1621_send
5426                     ; 118 	z=(uint8_t)(d/10);
5428  0052 7b01          	ld	a,(OFST-1,sp)
5429  0054 5f            	clrw	x
5430  0055 97            	ld	xl,a
5431  0056 a60a          	ld	a,#10
5432  0058 62            	div	x,a
5433  0059 9f            	ld	a,xl
5434  005a 6b02          	ld	(OFST+0,sp),a
5436                     ; 119 	ht1621_send(digits[z], 8);
5438  005c 7b02          	ld	a,(OFST+0,sp)
5439  005e 5f            	clrw	x
5440  005f 97            	ld	xl,a
5441  0060 e600          	ld	a,(L7643_digits,x)
5442  0062 ae0008        	ldw	x,#8
5443  0065 95            	ld	xh,a
5444  0066 cd0181        	call	_ht1621_send
5446  0069               L1063:
5447                     ; 121   if (flag || menu)
5449  0069 0d07          	tnz	(OFST+5,sp)
5450  006b 2604          	jrne	L5063
5452  006d 3d0a          	tnz	_menu
5453  006f 271e          	jreq	L3063
5454  0071               L5063:
5455                     ; 122 	ht1621_send(MINUS, 8);
5457  0071 ae0208        	ldw	x,#520
5458  0074 cd0181        	call	_ht1621_send
5461  0077               L7063:
5462                     ; 126   if (menu == (uint8_t)0x2 && !flag) {
5464  0077 b60a          	ld	a,_menu
5465  0079 a102          	cp	a,#2
5466  007b 261a          	jrne	L1163
5468  007d 0d07          	tnz	(OFST+5,sp)
5469  007f 2616          	jrne	L1163
5470                     ; 127 	ht1621_send(0, 8);
5472  0081 ae0008        	ldw	x,#8
5473  0084 cd0181        	call	_ht1621_send
5475                     ; 128 	ht1621_send(0, 8);	
5477  0087 ae0008        	ldw	x,#8
5478  008a cd0181        	call	_ht1621_send
5481  008d 2040          	jra	L3163
5482  008f               L3063:
5483                     ; 124 	ht1621_send(0,8);
5485  008f ae0008        	ldw	x,#8
5486  0092 cd0181        	call	_ht1621_send
5488  0095 20e0          	jra	L7063
5489  0097               L1163:
5490                     ; 130 	z=(uint8_t)(t%10);
5492  0097 1e03          	ldw	x,(OFST+1,sp)
5493  0099 a60a          	ld	a,#10
5494  009b 62            	div	x,a
5495  009c 5f            	clrw	x
5496  009d 97            	ld	xl,a
5497  009e 9f            	ld	a,xl
5498  009f 6b02          	ld	(OFST+0,sp),a
5500                     ; 131 	ht1621_send(digits[z], 8);
5502  00a1 7b02          	ld	a,(OFST+0,sp)
5503  00a3 5f            	clrw	x
5504  00a4 97            	ld	xl,a
5505  00a5 e600          	ld	a,(L7643_digits,x)
5506  00a7 ae0008        	ldw	x,#8
5507  00aa 95            	ld	xh,a
5508  00ab cd0181        	call	_ht1621_send
5510                     ; 132 	z=(uint8_t)(t/10);
5512  00ae 1e03          	ldw	x,(OFST+1,sp)
5513  00b0 a60a          	ld	a,#10
5514  00b2 62            	div	x,a
5515  00b3 9f            	ld	a,xl
5516  00b4 6b02          	ld	(OFST+0,sp),a
5518                     ; 133 	if (t>0)
5520  00b6 1e03          	ldw	x,(OFST+1,sp)
5521  00b8 270f          	jreq	L5163
5522                     ; 134 	  ht1621_send(digits[z], 8);
5524  00ba 7b02          	ld	a,(OFST+0,sp)
5525  00bc 5f            	clrw	x
5526  00bd 97            	ld	xl,a
5527  00be e600          	ld	a,(L7643_digits,x)
5528  00c0 ae0008        	ldw	x,#8
5529  00c3 95            	ld	xh,a
5530  00c4 cd0181        	call	_ht1621_send
5533  00c7 2006          	jra	L3163
5534  00c9               L5163:
5535                     ; 136 	  ht1621_send(0,8);   
5537  00c9 ae0008        	ldw	x,#8
5538  00cc cd0181        	call	_ht1621_send
5540  00cf               L3163:
5541                     ; 138   PB_ODR |= (1<<CS);
5543  00cf 72185005      	bset	_PB_ODR,#4
5544                     ; 140 }
5547  00d3 5b04          	addw	sp,#4
5548  00d5 81            	ret
5613                     ; 142 void ht1621_print(int num) {
5614                     	switch	.FLASH_CODE
5615  00d6               _ht1621_print:
5617  00d6 89            	pushw	x
5618  00d7 5204          	subw	sp,#4
5619       00000004      OFST:	set	4
5622                     ; 144     uint16_t value=get_abs(num);
5624  00d9 9c            	rvf
5625  00da a30000        	cpw	x,#0
5626  00dd 2e03          	jrsge	L61
5627  00df 50            	negw	x
5628  00e0 2000          	jra	L02
5629  00e2               L61:
5630  00e2               L02:
5631  00e2 1f03          	ldw	(OFST-1,sp),x
5633                     ; 145     uint8_t first=1;
5635  00e4 a601          	ld	a,#1
5636  00e6 6b02          	ld	(OFST-2,sp),a
5638                     ; 147 	if (num <= 0) 
5640  00e8 9c            	rvf
5641  00e9 1e05          	ldw	x,(OFST+1,sp)
5642  00eb 2c03          	jrsgt	L3563
5643                     ; 148 	  ht1621_clear();
5645  00ed cd0024        	call	_ht1621_clear
5647  00f0               L3563:
5648                     ; 150 	PB_ODR &= ~(1<<CS);
5650  00f0 72195005      	bres	_PB_ODR,#4
5651  00f4               L5563:
5652                     ; 152         uint8_t rm=value % 10;
5654  00f4 1e03          	ldw	x,(OFST-1,sp)
5655  00f6 a60a          	ld	a,#10
5656  00f8 62            	div	x,a
5657  00f9 5f            	clrw	x
5658  00fa 97            	ld	xl,a
5659  00fb 01            	rrwa	x,a
5660  00fc 6b01          	ld	(OFST-3,sp),a
5661  00fe 02            	rlwa	x,a
5663                     ; 154 		if (first) {
5665  00ff 0d02          	tnz	(OFST-2,sp)
5666  0101 271a          	jreq	L3663
5667                     ; 155 		  ht1621_send(0xa0, 3);
5669  0103 aea003        	ldw	x,#40963
5670  0106 ad79          	call	_ht1621_send
5672                     ; 156 		  ht1621_send(0, 6);
5674  0108 ae0006        	ldw	x,#6
5675  010b ad74          	call	_ht1621_send
5677                     ; 157 		  ht1621_send(digits[rm], 8);		 
5679  010d 7b01          	ld	a,(OFST-3,sp)
5680  010f 5f            	clrw	x
5681  0110 97            	ld	xl,a
5682  0111 e600          	ld	a,(L7643_digits,x)
5683  0113 ae0008        	ldw	x,#8
5684  0116 95            	ld	xh,a
5685  0117 ad68          	call	_ht1621_send
5687                     ; 158 		  first=0;
5689  0119 0f02          	clr	(OFST-2,sp)
5692  011b 200c          	jra	L5663
5693  011d               L3663:
5694                     ; 160 		  ht1621_send(digits[rm], 8);		 
5696  011d 7b01          	ld	a,(OFST-3,sp)
5697  011f 5f            	clrw	x
5698  0120 97            	ld	xl,a
5699  0121 e600          	ld	a,(L7643_digits,x)
5700  0123 ae0008        	ldw	x,#8
5701  0126 95            	ld	xh,a
5702  0127 ad58          	call	_ht1621_send
5704  0129               L5663:
5705                     ; 162         value=value/10;
5707  0129 1e03          	ldw	x,(OFST-1,sp)
5708  012b a60a          	ld	a,#10
5709  012d 62            	div	x,a
5710  012e 1f03          	ldw	(OFST-1,sp),x
5712                     ; 163     } while (value>0);  
5714  0130 1e03          	ldw	x,(OFST-1,sp)
5715  0132 26c0          	jrne	L5563
5716                     ; 165 	if (num < 0) 
5718  0134 9c            	rvf
5719  0135 1e05          	ldw	x,(OFST+1,sp)
5720  0137 2e05          	jrsge	L7663
5721                     ; 166 	  ht1621_send(MINUS,8);  
5723  0139 ae0208        	ldw	x,#520
5724  013c ad43          	call	_ht1621_send
5726  013e               L7663:
5727                     ; 169 	PB_ODR |=  (1<<CS);
5729  013e 72185005      	bset	_PB_ODR,#4
5730                     ; 170 }
5733  0142 5b06          	addw	sp,#6
5734  0144 81            	ret
5779                     ; 172 void ht1621_send_data(uint8_t adr, uint8_t value) {
5780                     	switch	.FLASH_CODE
5781  0145               _ht1621_send_data:
5783  0145 89            	pushw	x
5784       00000000      OFST:	set	0
5787                     ; 173 	adr <<=2;
5789  0146 0801          	sll	(OFST+1,sp)
5790  0148 0801          	sll	(OFST+1,sp)
5791                     ; 174     PB_ODR &= ~(1<<CS);
5793  014a 72195005      	bres	_PB_ODR,#4
5794                     ; 175     ht1621_send(0xa0, 3);
5796  014e aea003        	ldw	x,#40963
5797  0151 ad2e          	call	_ht1621_send
5799                     ; 176     ht1621_send(adr, 6);
5801  0153 7b01          	ld	a,(OFST+1,sp)
5802  0155 ae0006        	ldw	x,#6
5803  0158 95            	ld	xh,a
5804  0159 ad26          	call	_ht1621_send
5806                     ; 177     ht1621_send(value, 8);
5808  015b 7b02          	ld	a,(OFST+2,sp)
5809  015d ae0008        	ldw	x,#8
5810  0160 95            	ld	xh,a
5811  0161 ad1e          	call	_ht1621_send
5813                     ; 178     PB_ODR |=  (1<<CS);
5815  0163 72185005      	bset	_PB_ODR,#4
5816                     ; 179 }
5819  0167 85            	popw	x
5820  0168 81            	ret
5856                     ; 181 void ht1621_send_cmd(uint8_t cmd) {
5857                     	switch	.FLASH_CODE
5858  0169               _ht1621_send_cmd:
5860  0169 88            	push	a
5861       00000000      OFST:	set	0
5864                     ; 182 	PB_ODR &= ~(1<<CS);
5866  016a 72195005      	bres	_PB_ODR,#4
5867                     ; 183 	ht1621_send(0x80,4);
5869  016e ae8004        	ldw	x,#32772
5870  0171 ad0e          	call	_ht1621_send
5872                     ; 184 	ht1621_send(cmd,8);
5874  0173 7b01          	ld	a,(OFST+1,sp)
5875  0175 ae0008        	ldw	x,#8
5876  0178 95            	ld	xh,a
5877  0179 ad06          	call	_ht1621_send
5879                     ; 185 	PB_ODR |=  (1<<CS);
5881  017b 72185005      	bset	_PB_ODR,#4
5882                     ; 186 }
5885  017f 84            	pop	a
5886  0180 81            	ret
5939                     ; 188 void ht1621_send(uint8_t data, uint8_t len) {
5940                     	switch	.FLASH_CODE
5941  0181               _ht1621_send:
5943  0181 89            	pushw	x
5944  0182 88            	push	a
5945       00000001      OFST:	set	1
5948                     ; 190 	for(i=0;i<len;i++) {
5950  0183 0f01          	clr	(OFST+0,sp)
5953  0185 2021          	jra	L3673
5954  0187               L7573:
5955                     ; 191         PB_ODR=(data & 0x80) ? PB_ODR | (1<<DATA) : PB_ODR & ~(1<<DATA);
5957  0187 7b02          	ld	a,(OFST+1,sp)
5958  0189 a580          	bcp	a,#128
5959  018b 2707          	jreq	L03
5960  018d c65005        	ld	a,_PB_ODR
5961  0190 aa40          	or	a,#64
5962  0192 2005          	jra	L23
5963  0194               L03:
5964  0194 c65005        	ld	a,_PB_ODR
5965  0197 a4bf          	and	a,#191
5966  0199               L23:
5967  0199 c75005        	ld	_PB_ODR,a
5968                     ; 192 		PB_ODR &= ~(1<<WR);
5970  019c 721b5005      	bres	_PB_ODR,#5
5971                     ; 193 		PB_ODR |= (1<<WR);
5973  01a0 721a5005      	bset	_PB_ODR,#5
5974                     ; 194         data=(data<<1);
5976  01a4 0802          	sll	(OFST+1,sp)
5977                     ; 190 	for(i=0;i<len;i++) {
5979  01a6 0c01          	inc	(OFST+0,sp)
5981  01a8               L3673:
5984  01a8 7b01          	ld	a,(OFST+0,sp)
5985  01aa 1103          	cp	a,(OFST+2,sp)
5986  01ac 25d9          	jrult	L7573
5987                     ; 196 }
5990  01ae 5b03          	addw	sp,#3
5991  01b0 81            	ret
6101                     ; 199 void main_loop(void) {
6102                     	switch	.FLASH_CODE
6103  01b1               _main_loop:
6105  01b1 520c          	subw	sp,#12
6106       0000000c      OFST:	set	12
6109                     ; 200 	uint8_t prev=0;
6111  01b3 0f04          	clr	(OFST-8,sp)
6113                     ; 201 	uint8_t btn_press=0;
6115                     ; 202 	uint16_t i=40;
6117  01b5 ae0028        	ldw	x,#40
6118  01b8 1f08          	ldw	(OFST-4,sp),x
6120                     ; 203 	uint8_t btn=0;	
6122                     ; 204     uint16_t tick=0;
6124  01ba 5f            	clrw	x
6125  01bb 1f06          	ldw	(OFST-6,sp),x
6127                     ; 205 	uint16_t time=1000;
6129  01bd ae03e8        	ldw	x,#1000
6130  01c0 1f0b          	ldw	(OFST-1,sp),x
6132                     ; 206 	uint8_t blink=0;
6134  01c2 0f05          	clr	(OFST-7,sp)
6136                     ; 207 	ht1621_init();	
6138  01c4 cd0000        	call	_ht1621_init
6140                     ; 208     ht1621_clear();  
6142  01c7 cd0024        	call	_ht1621_clear
6144                     ; 211     CLK_SWCR |= (1<<1);                     // set SWEN flag
6146  01ca 721250c9      	bset	_CLK_SWCR,#1
6147                     ; 212     CLK_ECKCR |= (1<<2);                    // set LSEON flag
6149  01ce 721450c6      	bset	_CLK_ECKCR,#2
6151  01d2               L5404:
6152                     ; 213     while (!(CLK_ECKCR & 0x08));            // wait LSERDY flag
6154  01d2 c650c6        	ld	a,_CLK_ECKCR
6155  01d5 a508          	bcp	a,#8
6156  01d7 27f9          	jreq	L5404
6157                     ; 214     CLK_SWR= 0x08;                          // set LSE
6159  01d9 350850c8      	mov	_CLK_SWR,#8
6161  01dd               L5504:
6162                     ; 215 	while (CLK_SWCR & 0x01);				// wait to reset SWBSY flag
6164  01dd c650c9        	ld	a,_CLK_SWCR
6165  01e0 a501          	bcp	a,#1
6166  01e2 26f9          	jrne	L5504
6167                     ; 216     CLK_ICKCR &= ~(1<<0);                   // reset HSION flag
6169  01e4 721150c2      	bres	_CLK_ICKCR,#0
6170                     ; 217 	CLK_DIVR = 0;         					// SYSCLK=LSE
6172  01e8 725f50c0      	clr	_CLK_DIVR
6173                     ; 219     FLASH_CR1 |= (1<<3);                    // set EEPM flag
6175  01ec 72165050      	bset	_FLASH_CR1,#3
6177  01f0               L3604:
6178                     ; 220     while (--i > 0);                        // wait after switch off flash memory
6180  01f0 1e08          	ldw	x,(OFST-4,sp)
6181  01f2 1d0001        	subw	x,#1
6182  01f5 1f08          	ldw	(OFST-4,sp),x
6184  01f7 26f7          	jrne	L3604
6185                     ; 222     CLK_REGCSR |= (1<<1);                   // set REGOFF flag
6187  01f9 721250cf      	bset	_CLK_REGCSR,#1
6188                     ; 225 	TIM4_CR1  |= 0x01;
6190  01fd 721052e0      	bset	_TIM4_CR1,#0
6191                     ; 226 	ht1621_time(time,0);
6193  0201 4b00          	push	#0
6194  0203 1e0c          	ldw	x,(OFST+0,sp)
6195  0205 cd0000        	call	L3453_ht1621_time
6197  0208 84            	pop	a
6198  0209               L7604:
6199                     ; 229 	  btn=PB_IDR;
6201  0209 c65006        	ld	a,_PB_IDR
6202  020c 6b0a          	ld	(OFST-2,sp),a
6204                     ; 230 	  btn &= 0x2;
6206  020e 7b0a          	ld	a,(OFST-2,sp)
6207  0210 a402          	and	a,#2
6208  0212 6b0a          	ld	(OFST-2,sp),a
6210                     ; 231 	  if (btn == 0) {
6212  0214 0d0a          	tnz	(OFST-2,sp)
6213  0216 2637          	jrne	L3704
6214                     ; 232 		menu = (menu<2) ? menu +1 : 0;
6216  0218 b60a          	ld	a,_menu
6217  021a a102          	cp	a,#2
6218  021c 2405          	jruge	L63
6219  021e b60a          	ld	a,_menu
6220  0220 4c            	inc	a
6221  0221 2001          	jra	L04
6222  0223               L63:
6223  0223 4f            	clr	a
6224  0224               L04:
6225  0224 b70a          	ld	_menu,a
6226                     ; 233 		TIM2_CNTRL=0;
6228  0226 725f525d      	clr	_TIM2_CNTRL
6229                     ; 234 		TIM2_CNTRH=0;				
6231  022a 725f525c      	clr	_TIM2_CNTRH
6232  022e               L5704:
6233                     ; 237 		  btn=PB_IDR;
6235  022e c65006        	ld	a,_PB_IDR
6236  0231 6b0a          	ld	(OFST-2,sp),a
6238                     ; 238 		  btn &= 0x2;		  
6240  0233 7b0a          	ld	a,(OFST-2,sp)
6241  0235 a402          	and	a,#2
6242  0237 6b0a          	ld	(OFST-2,sp),a
6244                     ; 239 		  for(i=0; i<300;i++) {
6246  0239 5f            	clrw	x
6247  023a 1f08          	ldw	(OFST-4,sp),x
6249  023c               L3014:
6250                     ; 240 			_asm("nop");
6253  023c 9d            nop
6255                     ; 239 		  for(i=0; i<300;i++) {
6257  023d 1e08          	ldw	x,(OFST-4,sp)
6258  023f 1c0001        	addw	x,#1
6259  0242 1f08          	ldw	(OFST-4,sp),x
6263  0244 1e08          	ldw	x,(OFST-4,sp)
6264  0246 a3012c        	cpw	x,#300
6265  0249 25f1          	jrult	L3014
6266                     ; 242 		} while (btn == 0);
6268  024b 0d0a          	tnz	(OFST-2,sp)
6269  024d 27df          	jreq	L5704
6270  024f               L3704:
6271                     ; 246 	  if (menu) {
6273  024f 3d0a          	tnz	_menu
6274  0251 2733          	jreq	L1114
6275                     ; 247 		count=(uint16_t)TIM2_CNTRH;
6277  0253 c6525c        	ld	a,_TIM2_CNTRH
6278  0256 5f            	clrw	x
6279  0257 97            	ld	xl,a
6280  0258 bf0b          	ldw	_count,x
6281                     ; 248 		count = count <<8;
6283  025a be0b          	ldw	x,_count
6284  025c 4f            	clr	a
6285  025d 02            	rlwa	x,a
6286  025e bf0b          	ldw	_count,x
6287                     ; 249 		count |=(uint16_t)TIM2_CNTRL;
6289  0260 c6525d        	ld	a,_TIM2_CNTRL
6290  0263 5f            	clrw	x
6291  0264 97            	ld	xl,a
6292  0265 1f01          	ldw	(OFST-11,sp),x
6294  0267 be0b          	ldw	x,_count
6295  0269 01            	rrwa	x,a
6296  026a 1a02          	or	a,(OFST-10,sp)
6297  026c 01            	rrwa	x,a
6298  026d 1a01          	or	a,(OFST-11,sp)
6299  026f 01            	rrwa	x,a
6300  0270 bf0b          	ldw	_count,x
6301                     ; 250 		count = count >>1;
6303  0272 be0b          	ldw	x,_count
6304  0274 57            	sraw	x
6305  0275 bf0b          	ldw	_count,x
6306                     ; 252 		if (count >= 125) {
6308  0277 9c            	rvf
6309  0278 be0b          	ldw	x,_count
6310  027a a3007d        	cpw	x,#125
6311  027d 2f07          	jrslt	L1114
6312                     ; 253 		  count = (count - 250);
6314  027f be0b          	ldw	x,_count
6315  0281 1d00fa        	subw	x,#250
6316  0284 bf0b          	ldw	_count,x
6317  0286               L1114:
6318                     ; 257 	  if (menu==2 && prev == 1) {
6320  0286 b60a          	ld	a,_menu
6321  0288 a102          	cp	a,#2
6322  028a 260c          	jrne	L5114
6324  028c 7b04          	ld	a,(OFST-8,sp)
6325  028e a101          	cp	a,#1
6326  0290 2606          	jrne	L5114
6327                     ; 258 		time =offset;	
6329  0292 be0d          	ldw	x,_offset
6330  0294 1f0b          	ldw	(OFST-1,sp),x
6332                     ; 259 		time=time;
6335  0296 203c          	jra	L7114
6336  0298               L5114:
6337                     ; 260 	  } else if (menu == 0 && prev == 2){
6339  0298 3d0a          	tnz	_menu
6340  029a 261f          	jrne	L1214
6342  029c 7b04          	ld	a,(OFST-8,sp)
6343  029e a102          	cp	a,#2
6344  02a0 2619          	jrne	L1214
6345                     ; 261 		TIM4_CR1&=~(0x01);	// disable timer
6347  02a2 721152e0      	bres	_TIM4_CR1,#0
6348                     ; 262 		TIM4_PSCR  = 0x0c;
6350  02a6 350c52e8      	mov	_TIM4_PSCR,#12
6351                     ; 263 		TIM4_CNTR=0;
6353  02aa 725f52e7      	clr	_TIM4_CNTR
6354                     ; 264 		TIM4_CR1 |= (0x01);	// enable timer
6356  02ae 721052e0      	bset	_TIM4_CR1,#0
6357                     ; 265 		time =offset;
6359  02b2 be0d          	ldw	x,_offset
6360  02b4 1f0b          	ldw	(OFST-1,sp),x
6362                     ; 266 		tick=0;
6364  02b6 5f            	clrw	x
6365  02b7 1f06          	ldw	(OFST-6,sp),x
6368  02b9 2019          	jra	L7114
6369  02bb               L1214:
6370                     ; 267 	  } else if (menu == 1 && prev == 0) {
6372  02bb b60a          	ld	a,_menu
6373  02bd a101          	cp	a,#1
6374  02bf 2613          	jrne	L7114
6376  02c1 0d04          	tnz	(OFST-8,sp)
6377  02c3 260f          	jrne	L7114
6378                     ; 268 		TIM4_CR1&=~(0x01);	// disable timer
6380  02c5 721152e0      	bres	_TIM4_CR1,#0
6381                     ; 269 		TIM4_PSCR  = 0x0b;
6383  02c9 350b52e8      	mov	_TIM4_PSCR,#11
6384                     ; 270 		TIM4_CR1 |= (0x01);	// enable timer
6386  02cd 721052e0      	bset	_TIM4_CR1,#0
6387                     ; 271 		tick=0;
6389  02d1 5f            	clrw	x
6390  02d2 1f06          	ldw	(OFST-6,sp),x
6392  02d4               L7114:
6393                     ; 275 	  if (!(++tick%4)) {		// last 1 sec
6395  02d4 1e06          	ldw	x,(OFST-6,sp)
6396  02d6 1c0001        	addw	x,#1
6397  02d9 1f06          	ldw	(OFST-6,sp),x
6399  02db 7b07          	ld	a,(OFST-5,sp)
6400  02dd a503          	bcp	a,#3
6401  02df 2703cc0362    	jrne	L7214
6402                     ; 276 		blink=!blink;
6404  02e4 0d05          	tnz	(OFST-7,sp)
6405  02e6 2604          	jrne	L24
6406  02e8 a601          	ld	a,#1
6407  02ea 2001          	jra	L44
6408  02ec               L24:
6409  02ec 4f            	clr	a
6410  02ed               L44:
6411  02ed 6b05          	ld	(OFST-7,sp),a
6413                     ; 277 		switch (menu) {
6415  02ef b60a          	ld	a,_menu
6417                     ; 293 		  default:
6417                     ; 294 			ht1621_time(time,blink);	
6418  02f1 4a            	dec	a
6419  02f2 275b          	jreq	L1773
6420  02f4 4a            	dec	a
6421  02f5 270b          	jreq	L7673
6422  02f7               L3773:
6425  02f7 7b05          	ld	a,(OFST-7,sp)
6426  02f9 88            	push	a
6427  02fa 1e0c          	ldw	x,(OFST+0,sp)
6428  02fc cd0000        	call	L3453_ht1621_time
6430  02ff 84            	pop	a
6431  0300 2060          	jra	L7214
6432  0302               L7673:
6433                     ; 278 		  case 2:
6433                     ; 279 		    offset = time/60;
6435  0302 1e0b          	ldw	x,(OFST-1,sp)
6436  0304 a63c          	ld	a,#60
6437  0306 62            	div	x,a
6438  0307 bf0d          	ldw	_offset,x
6439                     ; 280 			offset += count;
6441  0309 be0d          	ldw	x,_offset
6442  030b 72bb000b      	addw	x,_count
6443  030f bf0d          	ldw	_offset,x
6444                     ; 281 			offset = get_abs(offset);
6446  0311 9c            	rvf
6447  0312 be0d          	ldw	x,_offset
6448  0314 2e05          	jrsge	L64
6449  0316 be0d          	ldw	x,_offset
6450  0318 50            	negw	x
6451  0319 2002          	jra	L05
6452  031b               L64:
6453  031b be0d          	ldw	x,_offset
6454  031d               L05:
6455  031d bf0d          	ldw	_offset,x
6456                     ; 282 			offset = (offset<<4)-offset;
6458  031f be0d          	ldw	x,_offset
6459  0321 58            	sllw	x
6460  0322 58            	sllw	x
6461  0323 58            	sllw	x
6462  0324 58            	sllw	x
6463  0325 72b0000d      	subw	x,_offset
6464  0329 bf0d          	ldw	_offset,x
6465                     ; 283 			offset <<=2;
6467  032b be0d          	ldw	x,_offset
6468  032d 58            	sllw	x
6469  032e 58            	sllw	x
6470  032f bf0d          	ldw	_offset,x
6471                     ; 284 			offset += (time%60);			
6473  0331 1e0b          	ldw	x,(OFST-1,sp)
6474  0333 a63c          	ld	a,#60
6475  0335 62            	div	x,a
6476  0336 5f            	clrw	x
6477  0337 97            	ld	xl,a
6478  0338 1f01          	ldw	(OFST-11,sp),x
6480  033a be0d          	ldw	x,_offset
6481  033c 72fb01        	addw	x,(OFST-11,sp)
6482  033f bf0d          	ldw	_offset,x
6483                     ; 285 			ht1621_time(offset,blink);
6485  0341 7b05          	ld	a,(OFST-7,sp)
6486  0343 88            	push	a
6487  0344 be0d          	ldw	x,_offset
6488  0346 cd0000        	call	L3453_ht1621_time
6490  0349 84            	pop	a
6491                     ; 286 			tick=0;
6493  034a 5f            	clrw	x
6494  034b 1f06          	ldw	(OFST-6,sp),x
6496                     ; 287 			break;
6498  034d 2013          	jra	L7214
6499  034f               L1773:
6500                     ; 288 		  case 1:
6500                     ; 289 			offset = time+count;			
6502  034f be0b          	ldw	x,_count
6503  0351 72fb0b        	addw	x,(OFST-1,sp)
6504  0354 bf0d          	ldw	_offset,x
6505                     ; 290 			ht1621_time(offset,blink);
6507  0356 7b05          	ld	a,(OFST-7,sp)
6508  0358 88            	push	a
6509  0359 be0d          	ldw	x,_offset
6510  035b cd0000        	call	L3453_ht1621_time
6512  035e 84            	pop	a
6513                     ; 291 			tick=0;
6515  035f 5f            	clrw	x
6516  0360 1f06          	ldw	(OFST-6,sp),x
6518                     ; 292 			break;
6520  0362               L3314:
6521  0362               L7214:
6522                     ; 301 	  if (!menu) {	  
6524  0362 3d0a          	tnz	_menu
6525  0364 261b          	jrne	L5314
6526                     ; 302 		if (tick == 240) {		// if 1 minute
6528  0366 1e06          	ldw	x,(OFST-6,sp)
6529  0368 a300f0        	cpw	x,#240
6530  036b 260a          	jrne	L7314
6531                     ; 304 		  ++time;
6533  036d 1e0b          	ldw	x,(OFST-1,sp)
6534  036f 1c0001        	addw	x,#1
6535  0372 1f0b          	ldw	(OFST-1,sp),x
6537                     ; 305 		  tick=0;
6539  0374 5f            	clrw	x
6540  0375 1f06          	ldw	(OFST-6,sp),x
6542  0377               L7314:
6543                     ; 307 		if (time == 1440)		// if 24 hour
6545  0377 1e0b          	ldw	x,(OFST-1,sp)
6546  0379 a305a0        	cpw	x,#1440
6547  037c 2603          	jrne	L5314
6548                     ; 308 		  time=0;		  
6550  037e 5f            	clrw	x
6551  037f 1f0b          	ldw	(OFST-1,sp),x
6553  0381               L5314:
6554                     ; 312 	  _asm("wfe");
6557  0381 728f          wfe
6559                     ; 313 	  TIM4_SR1 =0;	
6561  0383 725f52e5      	clr	_TIM4_SR1
6562                     ; 315 	  prev=menu; 
6564  0387 b60a          	ld	a,_menu
6565  0389 6b04          	ld	(OFST-8,sp),a
6568  038b ac090209      	jpf	L7604
6620                     	xdef	_ht1621_print
6621                     	xdef	_ht1621_clear
6622                     	xdef	_ht1621_off
6623                     	xdef	_ht1621_init
6624                     	xdef	_ht1621_send_data
6625                     	xdef	_ht1621_send
6626                     	xdef	_ht1621_send_cmd
6627                     	xdef	_offset
6628                     	xdef	_count
6629                     	xdef	_menu
6630                     	xdef	_main_loop
6649                     	end
