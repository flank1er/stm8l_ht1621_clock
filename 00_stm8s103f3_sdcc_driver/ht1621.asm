;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 4.1.0 #12072 (Linux)
;--------------------------------------------------------
	.module ht1621
	.optsdcc -mstm8
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _digits
	.globl _ht1621_off
	.globl _ht1621_init
	.globl _ht1621_clear
	.globl _ht1621_print_num
	.globl _ht1621_send_digit
	.globl _ht1621_send_data
	.globl _ht1621_send_cmd
	.globl _ht1621_send
	.globl _ht1621_char_to_seg_bits
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area DATA
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area INITIALIZED
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area DABS (ABS)

; default segment ordering for linker
	.area HOME
	.area GSINIT
	.area GSFINAL
	.area CONST
	.area INITIALIZER
	.area CODE

;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area HOME
	.area GSINIT
	.area GSFINAL
	.area GSINIT
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area HOME
	.area HOME
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area CODE
;	src/ht1621.c: 14: void ht1621_init() {
;	-----------------------------------------
;	 function ht1621_init
;	-----------------------------------------
_ht1621_init:
;	src/ht1621.c: 15: ht1621_send_cmd(BIAS);
	push	#0x52
	call	_ht1621_send_cmd
	pop	a
;	src/ht1621.c: 16: ht1621_send_cmd(RC256);
	push	#0x30
	call	_ht1621_send_cmd
	pop	a
;	src/ht1621.c: 17: ht1621_send_cmd(SYSDIS);
	push	#0x00
	call	_ht1621_send_cmd
	pop	a
;	src/ht1621.c: 18: ht1621_send_cmd(WDTDIS1);
	push	#0x0a
	call	_ht1621_send_cmd
	pop	a
;	src/ht1621.c: 19: ht1621_send_cmd(SYSEN);
	push	#0x02
	call	_ht1621_send_cmd
	pop	a
;	src/ht1621.c: 20: ht1621_send_cmd(LCDON);
	push	#0x06
	call	_ht1621_send_cmd
	pop	a
;	src/ht1621.c: 21: }
	ret
;	src/ht1621.c: 23: void ht1621_off() {
;	-----------------------------------------
;	 function ht1621_off
;	-----------------------------------------
_ht1621_off:
;	src/ht1621.c: 24: ht1621_send_cmd(LCDOFF);
	push	#0x04
	call	_ht1621_send_cmd
	pop	a
;	src/ht1621.c: 25: }
	ret
;	src/ht1621.c: 27: void ht1621_clear() {
;	-----------------------------------------
;	 function ht1621_clear
;	-----------------------------------------
_ht1621_clear:
;	src/ht1621.c: 28: for(uint16_t i=0;i<16;i++) {
	clrw	x
00103$:
	ldw	y, x
	cpw	y, #0x0010
	jrc	00118$
	ret
00118$:
;	src/ht1621.c: 29: ht1621_send_data((i<<1),0x0);
	ld	a, xl
	sll	a
	pushw	x
	push	#0x00
	push	a
	call	_ht1621_send_data
	addw	sp, #2
	popw	x
;	src/ht1621.c: 28: for(uint16_t i=0;i<16;i++) {
	incw	x
	jra	00103$
;	src/ht1621.c: 31: }
	ret
;	src/ht1621.c: 33: void ht1621_print_num(uint16_t num){
;	-----------------------------------------
;	 function ht1621_print_num
;	-----------------------------------------
_ht1621_print_num:
	sub	sp, #3
;	src/ht1621.c: 35: do {
	clr	(0x03, sp)
00101$:
;	src/ht1621.c: 36: uint8_t rm=num % 10;
	ldw	y, (0x06, sp)
	ldw	(0x01, sp), y
	ldw	x, y
	ldw	y, #0x000a
	divw	x, y
	ldw	x, y
;	src/ht1621.c: 37: ht1621_send_digit(pos++,rm);
	ld	a, (0x03, sp)
	inc	(0x03, sp)
	pushw	x
	addw	sp, #1
	push	a
	call	_ht1621_send_digit
	addw	sp, #2
;	src/ht1621.c: 38: num=num/10;
	ldw	x, (0x01, sp)
	ldw	y, #0x000a
	divw	x, y
;	src/ht1621.c: 39: } while (num>0);
	ldw	(0x06, sp), x
	jrne	00101$
;	src/ht1621.c: 40: }
	addw	sp, #3
	ret
;	src/ht1621.c: 42: void ht1621_send_digit(uint8_t pos,uint8_t digit) {
;	-----------------------------------------
;	 function ht1621_send_digit
;	-----------------------------------------
_ht1621_send_digit:
;	src/ht1621.c: 43: ht1621_send_data((pos<<1),digits[digit]);
	clrw	x
	ld	a, (0x04, sp)
	ld	xl, a
	addw	x, #(_digits+0)
	ld	a, (x)
	ld	xl, a
	ld	a, (0x03, sp)
	sll	a
	pushw	x
	addw	sp, #1
	push	a
	call	_ht1621_send_data
	addw	sp, #2
;	src/ht1621.c: 44: }
	ret
;	src/ht1621.c: 46: void ht1621_send_data(uint8_t adr, uint8_t value) {
;	-----------------------------------------
;	 function ht1621_send_data
;	-----------------------------------------
_ht1621_send_data:
;	src/ht1621.c: 47: adr <<=2;
	ld	a, (0x03, sp)
	sll	a
	sll	a
	ld	(0x03, sp), a
;	src/ht1621.c: 48: PC_ODR &= ~(1<<CS);
	bres	20490, #4
;	src/ht1621.c: 49: ht1621_send(0xa0, 3);
	push	#0x03
	push	#0xa0
	call	_ht1621_send
	addw	sp, #2
;	src/ht1621.c: 50: ht1621_send(adr, 6);
	push	#0x06
	ld	a, (0x04, sp)
	push	a
	call	_ht1621_send
	addw	sp, #2
;	src/ht1621.c: 51: ht1621_send(value, 8);
	push	#0x08
	ld	a, (0x05, sp)
	push	a
	call	_ht1621_send
	addw	sp, #2
;	src/ht1621.c: 52: PC_ODR |=  (1<<CS);
	bset	20490, #4
;	src/ht1621.c: 53: }
	ret
;	src/ht1621.c: 55: void ht1621_send_cmd(uint8_t cmd) {
;	-----------------------------------------
;	 function ht1621_send_cmd
;	-----------------------------------------
_ht1621_send_cmd:
;	src/ht1621.c: 56: PC_ODR &= ~(1<<CS);
	bres	20490, #4
;	src/ht1621.c: 57: ht1621_send(0x80,4);
	push	#0x04
	push	#0x80
	call	_ht1621_send
	addw	sp, #2
;	src/ht1621.c: 58: ht1621_send(cmd,8);
	push	#0x08
	ld	a, (0x04, sp)
	push	a
	call	_ht1621_send
	addw	sp, #2
;	src/ht1621.c: 59: PC_ODR |=  (1<<CS);
	bset	20490, #4
;	src/ht1621.c: 60: }
	ret
;	src/ht1621.c: 62: void ht1621_send(uint8_t data, uint8_t len) {
;	-----------------------------------------
;	 function ht1621_send
;	-----------------------------------------
_ht1621_send:
;	src/ht1621.c: 63: for(uint8_t i=0;i<len;i++) {
	clrw	x
00103$:
	ld	a, xl
	cp	a, (0x04, sp)
	jrc	00125$
	ret
00125$:
;	src/ht1621.c: 64: PC_ODR=(data & 0x80) ? PC_ODR | (1<<DATA) : PC_ODR & ~(1<<DATA);
	ld	a, (0x03, sp)
	ld	xh, a
	ld	a, 0x500a
	tnzw	x
	jrpl	00107$
	or	a, #0x40
	jra	00108$
00107$:
	and	a, #0xbf
00108$:
;	src/ht1621.c: 65: PC_ODR &= ~(1<<WR);
	ld	0x500a, a
	and	a, #0xdf
;	src/ht1621.c: 66: PC_ODR |= (1<<WR);
	ld	0x500a, a
	or	a, #0x20
	ld	0x500a, a
;	src/ht1621.c: 67: data=(data<<1);
	sll	(0x03, sp)
;	src/ht1621.c: 63: for(uint8_t i=0;i<len;i++) {
	incw	x
	jra	00103$
;	src/ht1621.c: 69: }
	ret
;	src/ht1621.c: 72: uint8_t ht1621_char_to_seg_bits(uint8_t ch) {
;	-----------------------------------------
;	 function ht1621_char_to_seg_bits
;	-----------------------------------------
_ht1621_char_to_seg_bits:
;	src/ht1621.c: 73: switch (ch) {
	ld	a, (0x03, sp)
	cp	a, #0x20
	jrnc	00173$
	jp	00159$
00173$:
	ld	a, (0x03, sp)
	cp	a, #0x7c
	jrule	00174$
	jp	00159$
00174$:
	ld	a, (0x03, sp)
	sub	a, #0x20
	clrw	x
	ld	xl, a
	sllw	x
	ldw	x, (#00175$, x)
	jp	(x)
00175$:
	.dw	#00159$
	.dw	#00159$
	.dw	#00159$
	.dw	#00159$
	.dw	#00159$
	.dw	#00159$
	.dw	#00159$
	.dw	#00159$
	.dw	#00159$
	.dw	#00159$
	.dw	#00101$
	.dw	#00159$
	.dw	#00159$
	.dw	#00103$
	.dw	#00159$
	.dw	#00159$
	.dw	#00105$
	.dw	#00106$
	.dw	#00107$
	.dw	#00108$
	.dw	#00109$
	.dw	#00110$
	.dw	#00111$
	.dw	#00112$
	.dw	#00113$
	.dw	#00114$
	.dw	#00159$
	.dw	#00159$
	.dw	#00159$
	.dw	#00159$
	.dw	#00159$
	.dw	#00159$
	.dw	#00159$
	.dw	#00116$
	.dw	#00118$
	.dw	#00120$
	.dw	#00122$
	.dw	#00124$
	.dw	#00126$
	.dw	#00128$
	.dw	#00130$
	.dw	#00132$
	.dw	#00134$
	.dw	#00159$
	.dw	#00136$
	.dw	#00138$
	.dw	#00140$
	.dw	#00159$
	.dw	#00143$
	.dw	#00145$
	.dw	#00147$
	.dw	#00149$
	.dw	#00151$
	.dw	#00153$
	.dw	#00159$
	.dw	#00159$
	.dw	#00159$
	.dw	#00155$
	.dw	#00157$
	.dw	#00159$
	.dw	#00159$
	.dw	#00159$
	.dw	#00159$
	.dw	#00104$
	.dw	#00159$
	.dw	#00116$
	.dw	#00118$
	.dw	#00120$
	.dw	#00122$
	.dw	#00124$
	.dw	#00126$
	.dw	#00128$
	.dw	#00130$
	.dw	#00132$
	.dw	#00134$
	.dw	#00159$
	.dw	#00136$
	.dw	#00138$
	.dw	#00140$
	.dw	#00141$
	.dw	#00143$
	.dw	#00145$
	.dw	#00147$
	.dw	#00149$
	.dw	#00151$
	.dw	#00153$
	.dw	#00159$
	.dw	#00159$
	.dw	#00159$
	.dw	#00155$
	.dw	#00157$
	.dw	#00159$
	.dw	#00102$
;	src/ht1621.c: 74: case '*': // For degree for now
00101$:
;	src/ht1621.c: 75: return 0b0110011;
	ld	a, #0x33
	ret
;	src/ht1621.c: 76: case '|':
00102$:
;	src/ht1621.c: 77: return 0b0000101;
	ld	a, #0x05
	ret
;	src/ht1621.c: 78: case '-':
00103$:
;	src/ht1621.c: 79: return 0b0000010;
	ld	a, #0x02
	ret
;	src/ht1621.c: 80: case '_':
00104$:
;	src/ht1621.c: 81: return 0b0001000;
	ld	a, #0x08
	ret
;	src/ht1621.c: 82: case '0':
00105$:
;	src/ht1621.c: 83: return 0b1111101;
	ld	a, #0x7d
	ret
;	src/ht1621.c: 84: case '1':
00106$:
;	src/ht1621.c: 85: return 0b1100000;
	ld	a, #0x60
	ret
;	src/ht1621.c: 86: case '2':
00107$:
;	src/ht1621.c: 87: return 0b111110;
	ld	a, #0x3e
	ret
;	src/ht1621.c: 88: case '3':
00108$:
;	src/ht1621.c: 89: return 0b1111010;
	ld	a, #0x7a
	ret
;	src/ht1621.c: 90: case '4':
00109$:
;	src/ht1621.c: 91: return 0b1100011;
	ld	a, #0x63
	ret
;	src/ht1621.c: 92: case '5':
00110$:
;	src/ht1621.c: 93: return 0b1011011;
	ld	a, #0x5b
	ret
;	src/ht1621.c: 94: case '6':
00111$:
;	src/ht1621.c: 95: return 0b1011111;
	ld	a, #0x5f
	ret
;	src/ht1621.c: 96: case '7':
00112$:
;	src/ht1621.c: 97: return 0b1110000;
	ld	a, #0x70
	ret
;	src/ht1621.c: 98: case '8':
00113$:
;	src/ht1621.c: 99: return 0b1111111;
	ld	a, #0x7f
	ret
;	src/ht1621.c: 100: case '9':
00114$:
;	src/ht1621.c: 101: return 0b1111011;
	ld	a, #0x7b
	ret
;	src/ht1621.c: 103: case 'a':
00116$:
;	src/ht1621.c: 104: return 0b1110111;
	ld	a, #0x77
	ret
;	src/ht1621.c: 106: case 'B':
00118$:
;	src/ht1621.c: 107: return 0b1001111;
	ld	a, #0x4f
	ret
;	src/ht1621.c: 110: case 'C':
00120$:
;	src/ht1621.c: 111: return 0b0011101;
	ld	a, #0x1d
	ret
;	src/ht1621.c: 113: case 'D':
00122$:
;	src/ht1621.c: 114: return 0b1101110;
	ld	a, #0x6e
	ret
;	src/ht1621.c: 117: case 'E':
00124$:
;	src/ht1621.c: 118: return 0b0011111;
	ld	a, #0x1f
	ret
;	src/ht1621.c: 121: case 'F':
00126$:
;	src/ht1621.c: 122: return 0b0010111;
	ld	a, #0x17
	ret
;	src/ht1621.c: 124: case 'g':
00128$:
;	src/ht1621.c: 125: return 0b1011101;
	ld	a, #0x5d
	ret
;	src/ht1621.c: 128: case 'H':
00130$:
;	src/ht1621.c: 129: return 0b1100111;
	ld	a, #0x67
	ret
;	src/ht1621.c: 132: case 'I':
00132$:
;	src/ht1621.c: 133: return 0b1100000;
	ld	a, #0x60
	ret
;	src/ht1621.c: 135: case 'j':
00134$:
;	src/ht1621.c: 136: return 0b1101000;
	ld	a, #0x68
	ret
;	src/ht1621.c: 139: case 'L':
00136$:
;	src/ht1621.c: 140: return 0b0001101;
	ld	a, #0x0d
	ret
;	src/ht1621.c: 142: case 'M':
00138$:
;	src/ht1621.c: 143: return 0b1010100;
	ld	a, #0x54
	ret
;	src/ht1621.c: 145: case 'N':
00140$:
;	src/ht1621.c: 146: return 0b1000110;
	ld	a, #0x46
	ret
;	src/ht1621.c: 147: case 'o':
00141$:
;	src/ht1621.c: 148: return 0b1001110;
	ld	a, #0x4e
	ret
;	src/ht1621.c: 150: case 'p':
00143$:
;	src/ht1621.c: 151: return 0b0110111;
	ld	a, #0x37
	ret
;	src/ht1621.c: 153: case 'Q':
00145$:
;	src/ht1621.c: 154: return 0b1110011;
	ld	a, #0x73
	ret
;	src/ht1621.c: 156: case 'R':
00147$:
;	src/ht1621.c: 157: return 0b0000110;
	ld	a, #0x06
	ret
;	src/ht1621.c: 159: case 's':
00149$:
;	src/ht1621.c: 160: return 0b1011011;
	ld	a, #0x5b
	ret
;	src/ht1621.c: 162: case 'T':
00151$:
;	src/ht1621.c: 163: return 0b0001111;
	ld	a, #0x0f
	ret
;	src/ht1621.c: 166: case 'U':
00153$:
;	src/ht1621.c: 167: return 0b1101101;
	ld	a, #0x6d
	ret
;	src/ht1621.c: 169: case 'y':
00155$:
;	src/ht1621.c: 170: return 0b1101011;
	ld	a, #0x6b
	ret
;	src/ht1621.c: 172: case 'Z':
00157$:
;	src/ht1621.c: 173: return 0b0111110;
	ld	a, #0x3e
	ret
;	src/ht1621.c: 175: default:
00159$:
;	src/ht1621.c: 176: return 0b0000000;
	clr	a
;	src/ht1621.c: 177: }
;	src/ht1621.c: 178: }
	ret
	.area CODE
	.area CONST
_digits:
	.db #0x7d	; 125
	.db #0x60	; 96
	.db #0x3e	; 62
	.db #0x7a	; 122	'z'
	.db #0x63	; 99	'c'
	.db #0x5b	; 91
	.db #0x5f	; 95
	.db #0x70	; 112	'p'
	.db #0x7f	; 127
	.db #0x7b	; 123
	.area INITIALIZER
	.area CABS (ABS)
