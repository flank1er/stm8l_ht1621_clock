#include <stdint.h>
#include "stm8s103f.h"
#include "stm8s_tim4.h"
#include "ht1621.h"

#define LED (1<<5)
uint16_t count;
void tim4_irq(void) __interrupt(23) {
	if (count)
		--count;

	TIM4_SR=0;
}

void delay_ms(uint16_t ms)
{
    count = ms;
    TIM4_SR   = 0x0;                        // Clear Pending Bit
    TIM4_PSCR = TIM4_PRESCALER_128;         // =7, prescaler =128
    TIM4_ARR  = 15;                         // freq Timer IRQ ~1kHz
    TIM4_IER  = (uint8_t)TIM4_IT_UPDATE;    // =1, enable interrupt
    TIM4_CR1  = TIM4_CR1_CEN;               // =1, enable counter
	while(count) {
    	wfi();                                  // goto sleep
	}
    TIM4_CR1  = 0x0;                        //  disable counter
}

int main() {
	CLK_CKDIVR=(uint8_t)0x18;				// HSI= 2MHz
	// Setup GPIO
    PB_DDR|=(LED);
    PB_CR1|=(LED);
	PC_DDR = ((1<<CS) | (1<<WR) | (1<<DATA)); // Push-Pull Mode
	PC_CR1 = ((1<<CS) | (1<<WR) | (1<<DATA)); //
	PC_CR2 = ((1<<CS) | (1<<WR) | (1<<DATA)); // Speed up 10 MHz
    // main loop
	ht1621_init();
	ht1621_clear();
	enableInterrupts();
	uint16_t tick=0;
    for(;;)
	{
        PB_ODR |= (LED);
		delay_ms(500);
        PB_ODR &= ~(LED);
		delay_ms(500);
		ht1621_print_num(tick++);
	}
}

