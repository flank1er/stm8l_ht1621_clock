#ifndef __HT1621_H__
#define __HT1621_H__

#define  BIAS     0x52             //0b1000 0101 0010  1/3duty 4com
#define  SYSDIS   0X00             //0b1000 0000 0000  关振系统荡器和LCD偏压发生器
#define  SYSEN    0X02             //0b1000 0000 0010 打开系统振荡器
#define  LCDOFF   0X04             //0b1000 0000 0100  关LCD偏压
#define  LCDON    0X06             //0b1000 0000 0110  打开LCD偏压
#define  XTAL     0x28             //0b1000 0010 1000 外部接时钟
#define  RC256    0X30             //0b1000 0011 0000  内部时钟
#define  TONEON   0X12             //0b1000 0001 0010  打开声音输出
#define  TONEOFF  0X10             //0b1000 0001 0000 关闭声音输出
#define  WDTDIS1  0X0A             //0b1000 0000 1010  禁止看门狗
#define  BUFFERSIZE 12

#define  WR	   5					// Clock signal
#define  CS	   4					// [C]hip [S]elect
#define  DATA  6					// MOSI

void ht1621_clear();
void ht1621_init();
void ht1621_send_digit(uint8_t pos,uint8_t digit);
void ht1621_print_num(uint16_t num);
#endif 		// __HT1621_H__
