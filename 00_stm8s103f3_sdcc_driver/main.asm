;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 4.1.0 #12072 (Linux)
;--------------------------------------------------------
	.module main
	.optsdcc -mstm8
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _main
	.globl _delay_ms
	.globl _tim4_irq
	.globl _ht1621_print_num
	.globl _ht1621_init
	.globl _ht1621_clear
	.globl _count
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area DATA
_count::
	.ds 2
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area INITIALIZED
;--------------------------------------------------------
; Stack segment in internal ram 
;--------------------------------------------------------
	.area	SSEG
__start__stack:
	.ds	1

;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area DABS (ABS)

; default segment ordering for linker
	.area HOME
	.area GSINIT
	.area GSFINAL
	.area CONST
	.area INITIALIZER
	.area CODE

;--------------------------------------------------------
; interrupt vector 
;--------------------------------------------------------
	.area HOME
__interrupt_vect:
	int s_GSINIT ; reset
	int 0x000000 ; trap
	int 0x000000 ; int0
	int 0x000000 ; int1
	int 0x000000 ; int2
	int 0x000000 ; int3
	int 0x000000 ; int4
	int 0x000000 ; int5
	int 0x000000 ; int6
	int 0x000000 ; int7
	int 0x000000 ; int8
	int 0x000000 ; int9
	int 0x000000 ; int10
	int 0x000000 ; int11
	int 0x000000 ; int12
	int 0x000000 ; int13
	int 0x000000 ; int14
	int 0x000000 ; int15
	int 0x000000 ; int16
	int 0x000000 ; int17
	int 0x000000 ; int18
	int 0x000000 ; int19
	int 0x000000 ; int20
	int 0x000000 ; int21
	int 0x000000 ; int22
	int _tim4_irq ; int23
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area HOME
	.area GSINIT
	.area GSFINAL
	.area GSINIT
__sdcc_init_data:
; stm8_genXINIT() start
	ldw x, #l_DATA
	jreq	00002$
00001$:
	clr (s_DATA - 1, x)
	decw x
	jrne	00001$
00002$:
	ldw	x, #l_INITIALIZER
	jreq	00004$
00003$:
	ld	a, (s_INITIALIZER - 1, x)
	ld	(s_INITIALIZED - 1, x), a
	decw	x
	jrne	00003$
00004$:
; stm8_genXINIT() end
	.area GSFINAL
	jp	__sdcc_program_startup
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area HOME
	.area HOME
__sdcc_program_startup:
	jp	_main
;	return from main will return to caller
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area CODE
;	main.c: 8: void tim4_irq(void) __interrupt(23) {
;	-----------------------------------------
;	 function tim4_irq
;	-----------------------------------------
_tim4_irq:
;	main.c: 9: if (count)
;	main.c: 10: --count;
	ldw	x, _count+0
	jreq	00102$
	decw	x
	ldw	_count+0, x
00102$:
;	main.c: 12: TIM4_SR=0;
	mov	0x5344+0, #0x00
;	main.c: 13: }
	iret
;	main.c: 15: void delay_ms(uint16_t ms)
;	-----------------------------------------
;	 function delay_ms
;	-----------------------------------------
_delay_ms:
;	main.c: 17: count = ms;
	ldw	x, (0x03, sp)
	ldw	_count+0, x
;	main.c: 18: TIM4_SR   = 0x0;                        // Clear Pending Bit
	mov	0x5344+0, #0x00
;	main.c: 19: TIM4_PSCR = TIM4_PRESCALER_128;         // =7, prescaler =128
	mov	0x5347+0, #0x07
;	main.c: 20: TIM4_ARR  = 15;                         // freq Timer IRQ ~1kHz
	mov	0x5348+0, #0x0f
;	main.c: 21: TIM4_IER  = (uint8_t)TIM4_IT_UPDATE;    // =1, enable interrupt
	mov	0x5343+0, #0x01
;	main.c: 22: TIM4_CR1  = TIM4_CR1_CEN;               // =1, enable counter
	mov	0x5340+0, #0x01
;	main.c: 23: while(count) {
00101$:
	ldw	x, _count+0
	jreq	00103$
;	main.c: 24: wfi();                                  // goto sleep
	wfi;	
	jra	00101$
00103$:
;	main.c: 26: TIM4_CR1  = 0x0;                        //  disable counter
	mov	0x5340+0, #0x00
;	main.c: 27: }
	ret
;	main.c: 29: int main() {
;	-----------------------------------------
;	 function main
;	-----------------------------------------
_main:
	sub	sp, #2
;	main.c: 30: CLK_CKDIVR=(uint8_t)0x18;				// HSI= 2MHz
	mov	0x50c6+0, #0x18
;	main.c: 32: PB_DDR|=(LED);
	bset	20487, #5
;	main.c: 33: PB_CR1|=(LED);
	ld	a, 0x5008
	or	a, #0x20
	ld	0x5008, a
;	main.c: 34: PC_DDR = ((1<<CS) | (1<<WR) | (1<<DATA)); // Push-Pull Mode
	mov	0x500c+0, #0x70
;	main.c: 35: PC_CR1 = ((1<<CS) | (1<<WR) | (1<<DATA)); //
	mov	0x500d+0, #0x70
;	main.c: 36: PC_CR2 = ((1<<CS) | (1<<WR) | (1<<DATA)); // Speed up 10 MHz
	mov	0x500e+0, #0x70
;	main.c: 38: ht1621_init();
	call	_ht1621_init
;	main.c: 39: ht1621_clear();
	call	_ht1621_clear
;	main.c: 40: enableInterrupts();
	rim;	
;	main.c: 41: uint16_t tick=0;
	clrw	x
	ldw	(0x01, sp), x
00102$:
;	main.c: 44: PB_ODR |= (LED);
	bset	20485, #5
;	main.c: 45: delay_ms(500);
	push	#0xf4
	push	#0x01
	call	_delay_ms
	addw	sp, #2
;	main.c: 46: PB_ODR &= ~(LED);
	bres	20485, #5
;	main.c: 47: delay_ms(500);
	push	#0xf4
	push	#0x01
	call	_delay_ms
	addw	sp, #2
;	main.c: 48: ht1621_print_num(tick++);
	ldw	x, (0x01, sp)
	ldw	y, (0x01, sp)
	incw	y
	ldw	(0x01, sp), y
	pushw	x
	call	_ht1621_print_num
	addw	sp, #2
	jra	00102$
;	main.c: 50: }
	addw	sp, #2
	ret
	.area CODE
	.area CONST
	.area INITIALIZER
	.area CABS (ABS)
