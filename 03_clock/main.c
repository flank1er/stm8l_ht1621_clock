#include <stdint.h>
#include "iostm8l152x.h"
#include "ht1621.h"
#include "main.h"

extern void delay_ms(uint16_t value);

main()
{
	int count=0;
    int offset=0;
	uint8_t btn=0;		
	uint8_t menu=0;
	uint8_t prev=menu;
	uint8_t blink=0;
	uint16_t tick=0;	
	uint16_t time=1000;
	uint8_t btn_press=0;
    // ------- CLK Setup ---------------
    CLK_DIVR = 4;         	// SYSCLK=1MHz
	CLK_PCKENR1 |= 0x02;    // Enable TIM3
	CLK_PCKENR1 |= 0x01;	// Enable TIM2		

    // parking GPIO
    // to ground Port A
    PA_DDR = 0xFF; PA_CR1 = 0xFF; PA_ODR = 0;
    // to ground Port B
    PB_DDR = 0xFF; PB_CR1 = 0xFF; PB_ODR = 0;
    // to ground Port C
    PC_DDR = 0xFF; PC_CR1 = 0xFF; PC_ODR = 0;
    // to ground Port D
    PD_DDR = 0xFF; PD_CR1 = 0xFF; PD_ODR = 0;
    // to ground Port E
    PE_DDR = 0xFF; PE_CR1 = 0xFF; PE_ODR = 0;
    // to ground Port F
    PF_DDR = 0xFF; PF_CR1 = 0xFF; PF_ODR = 0;
	
    // ------- GPIO setup --------------
	// Soft SPI GPIO Setup
	PB_DDR = ((1<<CS) | (1<<WR) | (1<<DATA)); // Push-Pull Mode
    PB_CR1 = ((1<<CS) | (1<<WR) | (1<<DATA)); //
    PB_CR2 = ((1<<CS) | (1<<WR) | (1<<DATA)); // Speed up 10 MHz
	// Encoder
	PB_DDR &= ~((1<<ENC_L) | (1<<ENC_R));
    PB_CR1 |=  ((1<<ENC_L) | (1<<ENC_R));
	// Button
	PB_DDR &= ~(1<<ENC_BTN);    // pullup
    PB_CR1 |= (1<<ENC_BTN);
	
	// ====== TIM3 Setup =========
    TIM3_SR1   = 0x0;                       // Clear Pending Bit
    TIM3_CR1   = 0x0;                       // Clear TIM1_CR1
    TIM3_CR2   = 0x0;                       // Clear TIM1_CR2
    TIM3_PSCR  = 6;     					// Prescaler 2^6= 64
    TIM3_ARRH  = 0x0c; TIM3_ARRL  = 0x35;   // (10^6)/64(prescaler) =15625 -> 0x7A12 -> freq Timer IRQ =1Hz
    TIM3_IER   = 0x01;                      // set UIE flag, enable interrupt
	TIM3_CNTRH=0;							// clear counter
	TIM3_CNTRL=0;							// clear counter
	WFE_CR3   |= 0x1;						// enable event
    TIM3_CR1  |= 0x01;                      // set CEN flag, start timer	
	// ======= TIM2 Setup =========
	TIM2_CCER1|= 0x22; 	// CC2P,CC1P: Capture/compare 2 output polarity
	TIM2_CCMR1|= 0x01; 	// CC1 channel is configured as input, IC1 is mapped on TI1FP1
	TIM2_CCMR2|= 0x01;	// CC2 channel is configured as input, IC2 is mapped on TI2FP2
	TIM2_SMCR |= 0x01; 	// encoder mode 1
	TIM2_ARRH=1;		// max value counter =500
	TIM2_ARRL=0xf4;		// max value counter =500
	TIM2_CNTRH=0;		// clear counter
	TIM2_CNTRL=0;		// clear counter
	TIM2_CR1|=0x1; 		// enable counter
    // main loop
    ht1621_init();	
    ht1621_clear();
	ht1621_print(0);
	FLASH_CR1 |= (1<<2); // set WAITM flag
    for(;;) {
	  // if press button		
	  btn=PB_IDR;
	  btn &= 0x2;
	  if (btn == 0) {
		menu = (menu<2) ? menu +1 : 0;
		TIM2_CNTRL=0;
		TIM2_CNTRH=0;				
		//ht1621_print((int)menu);
		
		do {
		  btn=PB_IDR;
		  btn &= 0x2;		  
		  delay_ms(300);	  
		} while (btn == 0);
	  }

	  // get encoder data
	  if (menu) {
		count=(uint16_t)TIM2_CNTRH;
		count = count <<8;
		count |=(uint16_t)TIM2_CNTRL;
		count = count >>1;
	  
		if (count >= 125) {
		  count = (count - 250);
		}
	  }

	  if (menu==2 && prev == 1) {
		time =offset;	
	  } else if (menu == 0 && prev == 2){
		TIM3_CR1&=~(0x01);	// disable timer
		TIM3_ARRH  = 0x0c; 
		TIM3_ARRL  = 0x35;
		TIM3_CNTRH=0;		// clear counter
		TIM3_CNTRL=0;		// clear counter
		TIM3_CR1 |= (0x01);	// enable timer
		time =offset;
		tick=0;
	  } else if (menu == 1 && prev == 0) {
		TIM3_CR1&=~(0x01);	// disable timer
		TIM3_ARRH  = 0x03; 
		TIM3_ARRL  = 0xe8;
		TIM3_CNTRH=0;		// clear counter
		TIM3_CNTRL=0;		// clear counter
		TIM3_CR1 |= (0x01);	// enable timer
		tick=0;
	  }

	  //tick++;
	  if (!(++tick%5)) {		// last 1 sec
		blink=!blink;
		switch (menu) {
		  case 1:
		    offset = count * 60;
			offset += (time/60)*60;
			offset = get_abs(offset);			
			offset += time%60;
			offset %= 1440;
			ht1621_hour(offset,blink);
			tick=0;
			break;
		  case 2:
			offset = time+count;			
			ht1621_minute(offset,blink);
			tick=0;
			break;
		  default:
			ht1621_time(time,blink);			
		}
	  }
		
	  // time count	  
	  if (!menu) {	  
		if (tick == 300) {		// if 1 minute
		  ++time;
		  tick=0;
		}		
		if (time == 1440)		// if 24 hour
		  time=0;		  
	  }

	  
	  prev=menu;
	  _asm("wfe");
	  TIM3_SR1 =0;
/*	  
	  if (menu)		

		delay_ms(50);
	  else {  
		_asm("wfe");
		TIM3_SR1 =0;
	  }
*/	  
    }
}