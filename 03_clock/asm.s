    switch .text
    ; ------------------
    xref _delay_ms
	xref _btn_press
	xdef _tim1_handler
	xdef _exti1_handler
    include "stm8l151c.s"
	
_delay_ms:
    pushw x
    pushw y
start_delay:
    ldw y, #250
loop:
    subw y,#1
    jrne loop
    decw x
    jrne start_delay
    popw y
    popw x
    ret

    end
	