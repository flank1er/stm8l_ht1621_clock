#ifndef __MAIN_H__
#define __MAIN_H__

#define ENC_L 0		// PB0
#define ENC_R 2		// PB2
#define ENC_BTN 1	// PB1

#define EnableInterrupt()   _asm("rim\n");
#define DisableInterrupt()  _asm("sim\n");
#define wfi()               _asm("wfi\n");

#endif
