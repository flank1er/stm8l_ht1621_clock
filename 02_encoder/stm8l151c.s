; STM8L151C.h 

; Copyright (c) 2003-2017 STMicroelectronics 

;#ifndef __STM8L151C__
;#define __STM8L151C__

; STM8L151C 

	; Check MCU name 
;	#ifdef MCU_NAME
;		#define STM8L151C 1
;		#if (MCU_NAME != STM8L151C)
;		#error "wrong include file STM8L151C.h for chosen MCU!"
;		#endif
;	#endif

;	#ifdef __CSMC__
;		#define .equ NAME,ADDRESS) volatile unsigned char NAME @ADDRESS
;		#define .equ NAME,ADDRESS) volatile unsigned int NAME @ADDRESS
;	#endif

;	#ifdef __RAISONANCE__
;		#define .equ NAME,ADDRESS) at ADDRESS hreg NAME
;		#define .equ NAME,ADDRESS) at ADDRESS hreg16 NAME
;	#endif

; Port A 
;***************************************************************

P0: equ 0
P1: equ 1
P2: equ 2
P3: equ 3
P4: equ 4
P5: equ 5
P6: equ 6
P7: equ 7


; Port A data output latch register 
PA_ODR: equ $5000

; Port A input pin value register 
PA_IDR: equ $5001

; Port A data direction register 
PA_DDR: equ $5002

; Port A control register 1 
PA_CR1: equ $5003

; Port A control register 2 
PA_CR2: equ $5004

; Port B 
;***************************************************************

; Port B data output latch register 
PB_ODR: equ $5005

; Port B input pin value register 
PB_IDR: equ $5006

; Port B data direction register 
PB_DDR: equ $5007

; Port B control register 1 
PB_CR1: equ $5008

; Port B control register 2 
PB_CR2: equ $5009

; Port C 
;***************************************************************

; Port C data output latch register 
PC_ODR: equ $500a

; Port C input pin value register 
PC_IDR: equ $500b

; Port C data direction register 
PC_DDR: equ $500c

; Port C control register 1 
PC_CR1: equ $500d

; Port C control register 2 
PC_CR2: equ $500e

; Port D 
;***************************************************************

; Port D data output latch register 
PD_ODR: equ $500f

; Port D input pin value register 
PD_IDR: equ $5010

; Port D data direction register 
PD_DDR: equ $5011

; Port D control register 1 
PD_CR1: equ $5012

; Port D control register 2 
PD_CR2: equ $5013

; Port E 
;***************************************************************

; Port E data output latch register 
PE_ODR: equ $5014

; Port E input pin value register 
PE_IDR: equ $5015

; Port E data direction register 
PE_DDR: equ $5016

; Port E control register 1 
PE_CR1: equ $5017

; Port E control register 2 
PE_CR2: equ $5018

; Port F 
;***************************************************************

; Port F data output latch register 
PF_ODR: equ $5019

; Port F input pin value register 
PF_IDR: equ $501a

; Port F data direction register 
PF_DDR: equ $501b

; Port F control register 1 
PF_CR1: equ $501c

; Port F control register 2 
PF_CR2: equ $501d

; Flash 
;***************************************************************

; Flash control register 1 
FLASH_CR1: equ $5050

; Flash control register 2 
FLASH_CR2: equ $5051

; Flash Program memory unprotection register 
FLASH_PUKR: equ $5052

; Data EEPROM unprotection register 
FLASH_DUKR: equ $5053

; Flash in-application programming status register 
FLASH_IAPSR: equ $5054

; Direct memory access controller 1 (DMA1) 
;***************************************************************

; DMA1 global configuration & status register 
DMA1_GCSR: equ $5070

; DMA1 global interrupt register 1 
DMA1_GIR1: equ $5071

; DMA1 channel 0 configuration register 
DMA1_C0CR: equ $5075

; DMA1 channel 0 status & priority register 
DMA1_C0SPR: equ $5076

; DMA1 number of data to transfer register (channel 0) 
DMA1_C0NDTR: equ $5077

; DMA1 peripheral address register (channel 0) 
DMA1_C0PAR: equ $5078
; DMA peripheral address high register (channel 0) 
DMA1_C0PARH: equ $5078
; DMA peripheral address low register (channel 0) 
DMA1_C0PARL: equ $5079

; DMA1 memory 0 address register (channel 0) 
DMA1_C0M0AR: equ $507b
; DMA memory address high register (channel 0) 
DMA1_C0M0ARH: equ $507b
; DMA memory address low register (channel 0) 
DMA1_C0M0ARL: equ $507c

; DMA1 channel 1 configuration register 
DMA1_C1CR: equ $507f

; DMA1 channel 1 status & priority register 
DMA1_C1SPR: equ $5080

; DMA1 number of data to transfer register (channel 1) 
DMA1_C1NDTR: equ $5081

; DMA1 peripheral address register (channel 1) 
DMA1_C1PAR: equ $5082
; DMA peripheral address high register (channel 1) 
DMA1_C1PARH: equ $5082
; DMA peripheral address low register (channel 1) 
DMA1_C1PARL: equ $5083

; DMA1 memory 0 address register (channel 1) 
DMA1_C1M0AR: equ $5085
; DMA memory address high register (channel 1) 
DMA1_C1M0ARH: equ $5085
; DMA memory address low register (channel 1) 
DMA1_C1M0ARL: equ $5086

; DMA1 channel 2 configuration register 
DMA1_C2CR: equ $5089

; DMA1 channel 2 status & priority register 
DMA1_C2SPR: equ $508a

; DMA1 number of data to transfer register (channel 2) 
DMA1_C2NDTR: equ $508b

; DMA1 peripheral address register (channel 2) 
DMA1_C2PAR: equ $508c
; DMA peripheral address high register (channel 2) 
DMA1_C2PARH: equ $508c
; DMA peripheral address low register (channel 2) 
DMA1_C2PARL: equ $508d

; DMA1 memory 0 address register (channel 2) 
DMA1_C2M0AR: equ $508f
; DMA memory address high register (channel 2) 
DMA1_C2M0ARH: equ $508f
; DMA memory address low register (channel 2) 
DMA1_C2M0ARL: equ $5090

; DMA1 channel 3 configuration register 
DMA1_C3CR: equ $5093

; DMA1 channel 3 status & priority register 
DMA1_C3SPR: equ $5094

; DMA1 number of data to transfer register (channel 3) 
DMA1_C3NDTR: equ $5095

; DMA1 peripheral address register (channel 3) 
DMA1_C3PAR_C3M1AR: equ $5096
; DMA1 peripheral address high register (channel 3) 
DMA1_C3PARH_C3M1ARH: equ $5096
; DMA1 peripheral address low register (channel 3) 
DMA1_C3PARL_C3M1ARL: equ $5097

; DMA channel 3 memory 0 extended address register 
DMA_C3M0EAR: equ $5098

; DMA1 memory 0 address register (channel 3) 
DMA1_C3M0AR: equ $5099
; DMA memory address high register (channel 3) 
DMA1_C3M0ARH: equ $5099
; DMA memory address low register (channel 3) 
DMA1_C3M0ARL: equ $509a

; System configuration (SYSCFG) 
;***************************************************************

; Remapping register 3 
SYSCFG_RMPCR3: equ $509d

; Remapping register 1 
SYSCFG_RMPCR1: equ $509e

; Remapping register 2 
SYSCFG_RMPCR2: equ $509f

; External Interrupt Control Register (ITC) 
;***************************************************************

; External interrupt control register 1 
EXTI_CR1: equ $50a0

; External interrupt control register 2 
EXTI_CR2: equ $50a1

; External interrupt control register 3 
EXTI_CR3: equ $50a2

; External interrupt status register 1 
EXTI_SR1: equ $50a3

; External interrupt status register 2 
EXTI_SR2: equ $50a4

; External interrupt port select register 1 
EXTI_CONF1: equ $50a5

; Wait For Event (WFE) 
;***************************************************************

; WFE control register 1 
WFE_CR1: equ $50a6

; WFE control register 2 
WFE_CR2: equ $50a7

; WFE control register 3 
WFE_CR3: equ $50a8

; WFE control register 4 
WFE_CR4: equ $50a9

; External Interrupt Control Register (ITC) 
;***************************************************************

; External interrupt control register 4 
EXTI_CR4: equ $50aa

; External interrupt port select register 2 
EXTI_CONF2: equ $50ab

; Reset (RST) 
;***************************************************************

; Reset control register 
RST_CR: equ $50b0

; Reset status register 
RST_SR: equ $50b1

; Power control (PWR) 
;***************************************************************

; Power control and status register 1 
PWR_CSR1: equ $50b2

; Power control and status register 2 
PWR_CSR2: equ $50b3

; Clock Control (CLK) 
;***************************************************************

; System clock divider register 
CLK_CKDIVR: equ $50c0

; Clock RTC register 
CLK_CRTCR: equ $50c1

; Internal clock control register 
CLK_ICKCR: equ $50c2

; Peripheral clock gating register 1 
CLK_PCKENR1: equ $50c3

; Peripheral clock gating register 2 
CLK_PCKENR2: equ $50c4

; Configurable clock control register 
CLK_CCOR: equ $50c5

; External clock control register 
CLK_ECKCR: equ $50c6

; System clock status register 
CLK_SCSR: equ $50c7

; System clock switch register 
CLK_SWR: equ $50c8

; Clock switch control register 
CLK_SWCR: equ $50c9

; Clock security system register 
CLK_CSSR: equ $50ca

; Clock BEEP register 
CLK_CBEEPR: equ $50cb

; HSI calibration register 
CLK_HSICALR: equ $50cc

; HSI clock calibration trimming register 
CLK_HSITRIMR: equ $50cd

; HSI unlock register 
CLK_HSIUNLCKR: equ $50ce

; Main regulator control status register 
CLK_REGCSR: equ $50cf

; Peripheral clock gating register 3 
CLK_PCKENR3: equ $50d0

; Window Watchdog (WWDG) 
;***************************************************************

; WWDG Control Register 
WWDG_CR: equ $50d3

; WWDR Window Register 
WWDG_WR: equ $50d4

; Independent Watchdog (IWDG) 
;***************************************************************

; IWDG Key Register 
IWDG_KR: equ $50e0

; IWDG Prescaler Register 
IWDG_PR: equ $50e1

; IWDG Reload Register 
IWDG_RLR: equ $50e2

; Beeper (BEEP) 
;***************************************************************

; BEEP Control/Status Register 1 
BEEP_CSR1: equ $50f0

; BEEP Control/Status Register 2 
BEEP_CSR2: equ $50f3

; Real-time clock (RTC) 
;***************************************************************

; Time Register 1 
RTC_TR1: equ $5140

; Time Register 2 
RTC_TR2: equ $5141

; Time Register 3 
RTC_TR3: equ $5142

; Date Register 1 
RTC_DR1: equ $5144

; Date Register 2 
RTC_DR2: equ $5145

; Date Register 3 
RTC_DR3: equ $5146

; Control Register 1 
RTC_CR1: equ $5148

; Control Register 2 
RTC_CR2: equ $5149

; Control Register 3 
RTC_CR3: equ $514a

; Initialization and Status Register 1 
RTC_ISR1: equ $514c

; Initialization and Status Register 2 
RTC_ISR2: equ $514d

; Synchronous Prescaler Register 
RTC_SPRER: equ $5150
; Synchronous Prescaler Register High 
RTC_SPRERH: equ $5150
; Synchronous Prescaler Register Low 
RTC_SPRERL: equ $5151

; Asynchronous Prescaler Register 
RTC_APRER: equ $5152

; Wakeup Timer Register 
RTC_WUTR: equ $5154
; Wakeup Timer Register High 
RTC_WUTRH: equ $5154
; Wakeup Timer Register Low 
RTC_WUTRL: equ $5155

; Subsecond Register 
RTC_SSR: equ $5157
; Subsecond Register High 
RTC_SSRH: equ $5157
; Subsecond Register Low 
RTC_SSRL: equ $5158

; Write Protection Register 
RTC_WPR: equ $5159

; Shift Register 
RTC_SHIFTR: equ $515a
; Shift Register High 
RTC_SHIFTRH: equ $515a
; Shift Register Low 
RTC_SHIFTRL: equ $515b

; Alarm A Register 1 
RTC_ALRMAR1: equ $515c

; Alarm A Register 2 
RTC_ALRMAR2: equ $515d

; Alarm A Register 3 
RTC_ALRMAR3: equ $515e

; Alarm A Register 4 
RTC_ALRMAR4: equ $515f

; Alarm A subsecond Register 
RTC_ALRMASSR: equ $5164
; Shift Register High 
RTC_ALRMASSRH: equ $5164
; Shift Register Low 
RTC_ALRMASSRL: equ $5165

; Alarm A masking Register 
RTC_ALRMASSMSKR: equ $5166

; Calibration Register 
RTC_CALR: equ $516a
; Shift Register High 
RTC_CALRH: equ $516a
; Shift Register Low 
RTC_CALRL: equ $516b

; Tamper Control Register 1 
RTC_TCR1: equ $516c

; Tamper Control Register 2 
RTC_TCR2: equ $516d

; Clock Security System (LSE_CSS) 
;***************************************************************

; CSS on LSE Control and Status Register 
CSS_LSE_CSR: equ $5190

; Serial Peripheral Interface 1 (SPI1) 
;***************************************************************

; SPI1 Control Register 1 
SPI1_CR1: equ $5200

; SPI1 Control Register 2 
SPI1_CR2: equ $5201

; SPI1 Interrupt Control Register 
SPI1_ICR: equ $5202

; SPI1 Status Register 
SPI1_SR: equ $5203

; SPI1 Data Register 
SPI1_DR: equ $5204

; SPI1 CRC Polynomial Register 
SPI1_CRCPR: equ $5205

; SPI1 Rx CRC Register 
SPI1_RXCRCR: equ $5206

; SPI1 Tx CRC Register 
SPI1_TXCRCR: equ $5207

; I2C Bus Interface 1 (I2C1) 
;***************************************************************

; I2C1 control register 1 
I2C1_CR1: equ $5210

; I2C1 control register 2 
I2C1_CR2: equ $5211

; I2C1 frequency register 
I2C1_FREQR: equ $5212

; I2C1 Own address register low 
I2C1_OARL: equ $5213

; I2C1 Own address register high 
I2C1_OARH: equ $5214

; I2C1 Own address register for dual mode 
I2C1_OAR2: equ $5215

; I2C1 data register 
I2C1_DR: equ $5216

; I2C1 status register 1 
I2C1_SR1: equ $5217

; I2C1 status register 2 
I2C1_SR2: equ $5218

; I2C1 status register 3 
I2C1_SR3: equ $5219

; I2C1 interrupt control register 
I2C1_ITR: equ $521a

; I2C1 Clock control register low 
I2C1_CCRL: equ $521b

; I2C1 Clock control register high 
I2C1_CCRH: equ $521c

; I2C1 TRISE register 
I2C1_TRISER: equ $521d

; I2C1 packet error checking register 
I2C1_PECR: equ $521e

; Universal synch/asynch receiver transmitter 1 (USART1) 
;***************************************************************

; USART1 Status Register 
USART1_SR: equ $5230

; USART1 Data Register 
USART1_DR: equ $5231

; USART1 Baud Rate Register 1 
USART1_BRR1: equ $5232

; USART1 Baud Rate Register 2 
USART1_BRR2: equ $5233

; USART1 Control Register 1 
USART1_CR1: equ $5234

; USART1 Control Register 2 
USART1_CR2: equ $5235

; USART1 Control Register 3 
USART1_CR3: equ $5236

; USART1 Control Register 4 
USART1_CR4: equ $5237

; USART1 Control Register 5 
USART1_CR5: equ $5238

; USART1 Guard time Register 
USART1_GTR: equ $5239

; USART1 Prescaler Register 
USART1_PSCR: equ $523a

; 16-Bit Timer 2 (TIM2) 
;***************************************************************

; TIM2 Control register 1 
TIM2_CR1: equ $5250

; TIM2 Control register 2 
TIM2_CR2: equ $5251

; TIM2 Slave Mode Control register 
TIM2_SMCR: equ $5252

; TIM2 External trigger register 
TIM2_ETR: equ $5253

; TIM2 DMA request enable register 
TIM2_DER: equ $5254

; TIM2 Interrupt enable register 
TIM2_IER: equ $5255

; TIM2 Status register 1 
TIM2_SR1: equ $5256

; TIM2 Status register 2 
TIM2_SR2: equ $5257

; TIM2 Event Generation register 
TIM2_EGR: equ $5258

; TIM2 Capture/Compare mode register 1 
TIM2_CCMR1: equ $5259

; TIM2 Capture/Compare mode register 2 
TIM2_CCMR2: equ $525a

; TIM2 Capture/Compare enable register 1 
TIM2_CCER1: equ $525b

; TIM2 Counter 
TIM2_CNTR: equ $525c
; TIM2 Counter High 
TIM2_CNTRH: equ $525c
; TIM2 Counter Low 
TIM2_CNTRL: equ $525d

; TIM2 Prescaler register 
TIM2_PSCR: equ $525e

; TIM2 Auto-reload register 
TIM2_ARR: equ $525f
; TIM2 Auto-Reload Register High 
TIM2_ARRH: equ $525f
; TIM2 Auto-Reload Register Low 
TIM2_ARRL: equ $5260

; TIM2 Capture/Compare register 1 
TIM2_CCR1: equ $5261
; TIM2 Capture/Compare Register 1 High 
TIM2_CCR1H: equ $5261
; TIM2 Capture/Compare Register 1 Low 
TIM2_CCR1L: equ $5262

; TIM2 Capture/Compare register 2 
TIM2_CCR2: equ $5263
; TIM2 Capture/Compare Register 2 High 
TIM2_CCR2H: equ $5263
; TIM2 Capture/Compare Register 2 Low 
TIM2_CCR2L: equ $5264

; TIM2 Break register 
TIM2_BKR: equ $5265

; TIM2 Output idle state register 
TIM2_OISR: equ $5266

; 16-Bit Timer 3 (TIM3) 
;***************************************************************

; TIM3 Control register 1 
TIM3_CR1: equ $5280

; TIM3 Control register 2 
TIM3_CR2: equ $5281

; TIM3 Slave Mode Control register 
TIM3_SMCR: equ $5282

; TIM3 External trigger register 
TIM3_ETR: equ $5283

; TIM3 DMA request enable register 
TIM3_DER: equ $5284

; TIM3 Interrupt enable register 
TIM3_IER: equ $5285

; TIM3 Status register 1 
TIM3_SR1: equ $5286

; TIM3 Status register 2 
TIM3_SR2: equ $5287

; TIM3 Event Generation register 
TIM3_EGR: equ $5288

; TIM3 Capture/Compare mode register 1 
TIM3_CCMR1: equ $5289

; TIM3 Capture/Compare mode register 2 
TIM3_CCMR2: equ $528a

; TIM3 Capture/Compare enable register 1 
TIM3_CCER1: equ $528b

; TIM3 Counter 
TIM3_CNTR: equ $528c
; TIM3 Counter High 
TIM3_CNTRH: equ $528c
; TIM3 Counter Low 
TIM3_CNTRL: equ $528d

; TIM3 Prescaler register 
TIM3_PSCR: equ $528e

; TIM3 Auto-reload register 
TIM3_ARR: equ $528f
; TIM3 Auto-Reload Register High 
TIM3_ARRH: equ $528f
; TIM3 Auto-Reload Register Low 
TIM3_ARRL: equ $5290

; TIM3 Capture/Compare register 1 
TIM3_CCR1: equ $5291
; TIM3 Capture/Compare Register 1 High 
TIM3_CCR1H: equ $5291
; TIM3 Capture/Compare Register 1 Low 
TIM3_CCR1L: equ $5292

; TIM3 Capture/Compare register 2 
TIM3_CCR2: equ $5293
; TIM3 Capture/Compare Register 2 High 
TIM3_CCR2H: equ $5293
; TIM3 Capture/Compare Register 2 Low 
TIM3_CCR2L: equ $5294

; TIM3 Break register 
TIM3_BKR: equ $5295

; TIM3 Output idle state register 
TIM3_OISR: equ $5296

; 16-Bit Timer 1 (TIM1) 
;***************************************************************

; TIM1 Control register 1 
TIM1_CR1: equ $52b0

; TIM1 Control register 2 
TIM1_CR2: equ $52b1

; TIM1 Slave Mode Control register 
TIM1_SMCR: equ $52b2

; TIM1 external trigger register 
TIM1_ETR: equ $52b3

; TIM1 DMA request enable register 
TIM1_DER: equ $52b4

; TIM1 Interrupt enable register 
TIM1_IER: equ $52b5

; TIM1 Status register 1 
TIM1_SR1: equ $52b6

; TIM1 Status register 2 
TIM1_SR2: equ $52b7

; TIM1 Event Generation register 
TIM1_EGR: equ $52b8

; TIM1 Capture/Compare mode register 1 
TIM1_CCMR1: equ $52b9

; TIM1 Capture/Compare mode register 2 
TIM1_CCMR2: equ $52ba

; TIM1 Capture/Compare mode register 3 
TIM1_CCMR3: equ $52bb

; TIM1 Capture/Compare mode register 4 
TIM1_CCMR4: equ $52bc

; TIM1 Capture/Compare enable register 1 
TIM1_CCER1: equ $52bd

; TIM1 Capture/Compare enable register 2 
TIM1_CCER2: equ $52be

; TIM1 Counter 
TIM1_CNTR: equ $52bf
; TIM1 Counter High 
TIM1_CNTRH: equ $52bf
; TIM1 Counter Low 
TIM1_CNTRL: equ $52c0

; TIM1 Prescaler register 
TIM1_PSCR: equ $52c1
; TIM1 Prescaler Register High 
TIM1_PSCRH: equ $52c1
; TIM1 Prescaler Register Low 
TIM1_PSCRL: equ $52c2

; TIM1 Auto-reload register 
TIM1_ARR: equ $52c3
; TIM1 Auto-Reload Register High 
TIM1_ARRH: equ $52c3
; TIM1 Auto-Reload Register Low 
TIM1_ARRL: equ $52c4

; TIM1 Repetition counter register 
TIM1_RCR: equ $52c5

; TIM1 Capture/Compare register 1 
TIM1_CCR1: equ $52c6
; TIM1 Capture/Compare Register 1 High 
TIM1_CCR1H: equ $52c6
; TIM1 Capture/Compare Register 1 Low 
TIM1_CCR1L: equ $52c7

; TIM1 Capture/Compare register 2 
TIM1_CCR2: equ $52c8
; TIM1 Capture/Compare Register 2 High 
TIM1_CCR2H: equ $52c8
; TIM1 Capture/Compare Register 2 Low 
TIM1_CCR2L: equ $52c9

; TIM1 Capture/Compare register 3 
TIM1_CCR3: equ $52ca
; TIM1 Capture/Compare Register 3 High 
TIM1_CCR3H: equ $52ca
; TIM1 Capture/Compare Register 3 Low 
TIM1_CCR3L: equ $52cb

; TIM1 Capture/Compare register 4 
TIM1_CCR4: equ $52cc
; TIM1 Capture/Compare Register 4 High 
TIM1_CCR4H: equ $52cc
; TIM1 Capture/Compare Register 4 Low 
TIM1_CCR4L: equ $52cd

; TIM1 Break register 
TIM1_BKR: equ $52ce

; TIM1 Dead-time register 
TIM1_DTR: equ $52cf

; TIM1 Output idle state register 
TIM1_OISR: equ $52d0

; TIM1 DMA1 control register 1 
TIM1_DCR1: equ $52d1

; TIM1 DMA1 control register 2 
TIM1_DCR2: equ $52d2

; TIM1 DMA1 address for burst mode 
TIM1_DMA1R: equ $52d3

; 8-Bit  Timer 4 (TIM4) 
;***************************************************************

; TIM4 Control Register 1 
TIM4_CR1: equ $52e0

; TIM4 Control Register 2 
TIM4_CR2: equ $52e1

; TIM4 Slave Mode Control Register 
TIM4_SMCR: equ $52e2

; TIM4 DMA request Enable Register 
TIM4_DER: equ $52e3

; TIM4 Interrupt Enable Register 
TIM4_IER: equ $52e4

; TIM4 Status Register 1 
TIM4_SR1: equ $52e5

; TIM4 Event Generation Register 
TIM4_EGR: equ $52e6

; TIM4 Counter 
TIM4_CNTR: equ $52e7

; TIM4 Prescaler Register 
TIM4_PSCR: equ $52e8

; TIM4 Auto-Reload Register 
TIM4_ARR: equ $52e9

; Infra Red Interface (IR) 
;***************************************************************

; Infra-red control register 
IR_CR: equ $52ff

; 16-Bit  Timer 5 (TIM5) 
;***************************************************************

; TIM5 Control register 1 
TIM5_CR1: equ $5300

; TIM5 Control register 2 
TIM5_CR2: equ $5301

; TIM5 Slave Mode Control register 
TIM5_SMCR: equ $5302

; TIM5 External trigger register 
TIM5_ETR: equ $5303

; TIM5 DMA request enable register 
TIM5_DER: equ $5304

; TIM5 Interrupt enable register 
TIM5_IER: equ $5305

; TIM5 Status register 1 
TIM5_SR1: equ $5306

; TIM5 Status register 2 
TIM5_SR2: equ $5307

; TIM5 Event Generation register 
TIM5_EGR: equ $5308

; TIM5 Capture/Compare mode register 1 
TIM5_CCMR1: equ $5309

; TIM5 Capture/Compare mode register 2 
TIM5_CCMR2: equ $530a

; TIM5 Capture/Compare enable register 1 
TIM5_CCER1: equ $530b

; TIM5 Counter 
TIM5_CNTR: equ $530c
; TIM5 Counter High 
TIM5_CNTRH: equ $530c
; TIM5 Counter Low 
TIM5_CNTRL: equ $530d

; TIM5 Prescaler register 
TIM5_PSCR: equ $530e

; TIM5 Auto-reload register 
TIM5_ARR: equ $530f
; TIM5 Auto-Reload Register High 
TIM5_ARRH: equ $530f
; TIM5 Auto-Reload Register Low 
TIM5_ARRL: equ $5310

; TIM5 Capture/Compare register 1 
TIM5_CCR1: equ $5311
; TIM5 Capture/Compare Register 1 High 
TIM5_CCR1H: equ $5311
; TIM5 Capture/Compare Register 1 Low 
TIM5_CCR1L: equ $5312

; TIM5 Capture/Compare register 2 
TIM5_CCR2: equ $5313
; TIM5 Capture/Compare Register 2 High 
TIM5_CCR2H: equ $5313
; TIM5 Capture/Compare Register 2 Low 
TIM5_CCR2L: equ $5314

; TIM5 Break register 
TIM5_BKR: equ $5315

; TIM5 Output idle state register 
TIM5_OISR: equ $5316

; Analog to digital converter 1 (ADC1) 
;***************************************************************

; ADC1 Configuration register 1 
ADC1_CR1: equ $5340

; ADC1 Configuration register 2 
ADC1_CR2: equ $5341

; ADC1 Configuration register 3 
ADC1_CR3: equ $5342

; ADC1 status register 
ADC1_SR: equ $5343

; ADC1 Data register 
ADC1_DR: equ $5344
; ADC Data Register High 
ADC1_DRH: equ $5344
; ADC Data Register Low 
ADC1_DRL: equ $5345

; ADC1 high threshold register 
ADC1_HTR: equ $5346
; ADC High Threshold Register High 
ADC1_HTRH: equ $5346
; ADC High Threshold Register Low 
ADC1_HTRL: equ $5347

; ADC1 low threshold register 
ADC1_LTR: equ $5348
; ADC Low Threshold Register High 
ADC1_LTRH: equ $5348
; ADC Low Threshold Register Low 
ADC1_LTRL: equ $5349

; ADC1 channel sequence 1 register 
ADC1_SQR1: equ $534a

; ADC1 channel sequence 2 register 
ADC1_SQR2: equ $534b

; ADC1 channel sequence 3 register 
ADC1_SQR3: equ $534c

; ADC1 channel sequence 4 register 
ADC1_SQR4: equ $534d

; ADC1 Trigger disable 1 
ADC1_TRIGR1: equ $534e

; ADC1 Trigger disable 2 
ADC1_TRIGR2: equ $534f

; ADC1 Trigger disable 3 
ADC1_TRIGR3: equ $5350

; ADC1 Trigger disable 4 
ADC1_TRIGR4: equ $5351

; Digital to analog converter (DAC) 
;***************************************************************

; DAC channel 1 control register 1 
DAC_CH1CR1: equ $5380

; DAC channel 1 control register 2 
DAC_CH1CR2: equ $5381

; DAC channel 2 control register 1 
DAC_CH2CR1: equ $5382

; DAC channel 2 control register 2 
DAC_CH2CR2: equ $5383

; DAC software trigger register 
DAC_SWTRIGR: equ $5384

; DAC status register 
DAC_SR: equ $5385

; DAC channel 1 right aligned data holding register 
DAC_CH1RDHR: equ $5388
; DAC channel 1 right aligned data holding register high 
DAC_CH1RDHRH: equ $5388
; DAC channel 1 right aligned data holding register low 
DAC_CH1RDHRL: equ $5389

; DAC channel 1 left aligned data holding register 
DAC_CH1LDHR: equ $538c
; DAC channel 1 left aligned data holding register high 
DAC_CH1LDHRH: equ $538c
; DAC channel 1 left aligned data holding register low 
DAC_CH1LDHRL: equ $538d

; DAC channel 1 8-bit data holding register 
DAC_CH1DHR8: equ $5390

; DAC channel 2 right aligned data holding register 
DAC_CH2RDHR: equ $5394
; DAC channel 2 right aligned data holding register high 
DAC_CH2RDHRH: equ $5394
; DAC channel 2 right aligned data holding register low 
DAC_CH2RDHRL: equ $5395

; DAC channel 2 left aligned data holding register 
DAC_CH2LDHR: equ $5398
; DAC channel 2 left aligned data holding register high 
DAC_CH2LDHRH: equ $5398
; DAC channel 2 left aligned data holding register low 
DAC_CH2LDHRL: equ $5399

; DAC channel 2 8-bit data holding register 
DAC_CH2DHR8: equ $539c

; DAC channel 1 right aligned data holding register 
DAC_DCH1RDHR: equ $53a0
; DAC channel 1 right aligned data holding register high 
DAC_DCH1RDHRH: equ $53a0
; DAC channel 1 right aligned data holding register low 
DAC_DCH1RDHRL: equ $53a1

; DAC channel 2 right aligned data holding register 
DAC_DCH2RDHR: equ $53a2
; DAC channel 2 right aligned data holding register high 
DAC_DCH2RDHRH: equ $53a2
; DAC channel 2 right aligned data holding register low 
DAC_DCH2RDHRL: equ $53a3

; DAC channel 1 left aligned data holding register 
DAC_DCH1LDHR: equ $53a4
; DAC channel 1 left aligned data holding register high 
DAC_DCH1LDHRH: equ $53a4
; DAC channel 1 left aligned data holding register low 
DAC_DCH1LDHRL: equ $53a5

; DAC channel 2 left aligned data holding register 
DAC_DCH2LDHR: equ $53a6
; DAC channel 2 left aligned data holding register high 
DAC_DCH2LDHRH: equ $53a6
; DAC channel 2 left aligned data holding register low 
DAC_DCH2LDHRL: equ $53a7

; DAC channel 1 8-bit data holding register 
DAC_DCH1DHR8: equ $53a8

; DAC channel 2 8-bit data holding register 
DAC_DCH2DHR8: equ $53a9

; DAC channel 1 data output register 
DAC_CH1DOR: equ $53ac
; DAC channel 1 data output register high 
DAC_CH1DORH: equ $53ac
; DAC channel 1 data output register low 
DAC_CH1DORL: equ $53ad

; DAC channel 2 data output register 
DAC_CH2DOR: equ $53b0
; DAC channel 2 data output register high 
DAC_CH2DORH: equ $53b0
; DAC channel 2 data output register low 
DAC_CH2DORL: equ $53b1

; Serial Peripheral Interface 2 (SPI2) 
;***************************************************************

; SPI2 Control Register 1 
SPI2_CR1: equ $53c0

; SPI2 Control Register 2 
SPI2_CR2: equ $53c1

; SPI2 Interrupt Control Register 
SPI2_ICR: equ $53c2

; SPI2 Status Register 
SPI2_SR: equ $53c3

; SPI2 Data Register 
SPI2_DR: equ $53c4

; SPI2 CRC Polynomial Register 
SPI2_CRCPR: equ $53c5

; SPI2 Rx CRC Register 
SPI2_RXCRCR: equ $53c6

; SPI2 Tx CRC Register 
SPI2_TXCRCR: equ $53c7

; Universal synch/asynch receiver transmitter 2 (USART2) 
;***************************************************************

; USART2 Status Register 
USART2_SR: equ $53e0

; USART2 Data Register 
USART2_DR: equ $53e1

; USART2 Baud Rate Register 1 
USART2_BRR1: equ $53e2

; USART2 Baud Rate Register 2 
USART2_BRR2: equ $53e3

; USART2 Control Register 1 
USART2_CR1: equ $53e4

; USART2 Control Register 2 
USART2_CR2: equ $53e5

; USART2 Control Register 3 
USART2_CR3: equ $53e6

; USART2 Control Register 4 
USART2_CR4: equ $53e7

; USART2 Control Register 5 
USART2_CR5: equ $53e8

; USART2 Guard time Register 
USART2_GTR: equ $53e9

; USART2 Prescaler Register 
USART2_PSCR: equ $53ea

; Universal synch/asynch receiver transmitter 3 (USART3) 
;***************************************************************

; USART3 Status Register 
USART3_SR: equ $53f0

; USART3 Data Register 
USART3_DR: equ $53f1

; USART3 Baud Rate Register 1 
USART3_BRR1: equ $53f2

; USART3 Baud Rate Register 2 
USART3_BRR2: equ $53f3

; USART3 Control Register 1 
USART3_CR1: equ $53f4

; USART3 Control Register 2 
USART3_CR2: equ $53f5

; USART3 Control Register 3 
USART3_CR3: equ $53f6

; USART3 Control Register 4 
USART3_CR4: equ $53f7

; USART3 Control Register 5 
USART3_CR5: equ $53f8

; USART3 Guard time Register 
USART3_GTR: equ $53f9

; USART3 Prescaler Register 
USART3_PSCR: equ $53fa

; Routing interface (RI) 
;***************************************************************

; Timer input capture routing register 1 
RI_ICR1: equ $5431

; Timer input capture routing register 2 
RI_ICR2: equ $5432

; I/O input register 1 
RI_IOIR1: equ $5433

; I/O input register 2 
RI_IOIR2: equ $5434

; I/O input register 3 
RI_IOIR3: equ $5435

; I/O control mode register 1 
RI_IOCMR1: equ $5436

; I/O control mode register 2 
RI_IOCMR2: equ $5437

; I/O control mode register 3 
RI_IOCMR3: equ $5438

; I/O switch register 1 
RI_IOSR1: equ $5439

; I/O switch register 2 
RI_IOSR2: equ $543a

; I/O switch register 3 
RI_IOSR3: equ $543b

; I/O group control register 
RI_IOGCR: equ $543c

; Analog switch register 1 
RI_ASCR1: equ $543d

; Analog switch register 2 
RI_ASCR2: equ $543e

; Resistor control register 
RI_RCR: equ $543f

; Comparators (COMP) 
;***************************************************************

; Comparator control and status register 1 
COMP_CSR1: equ $5440

; Comparator control and status register 2 
COMP_CSR2: equ $5441

; Comparator control and status register 3 
COMP_CSR3: equ $5442

; Comparator control and status register 4 
COMP_CSR4: equ $5443

; Comparator control and status register 5 
COMP_CSR5: equ $5444

; Global configuration register (CFG) 
;***************************************************************

; CFG Global configuration register 
CFG_GCR: equ $7f60

; Interrupt Software Priority Registers (ITC) 
;***************************************************************

; Interrupt Software priority register 1 
ITC_SPR1: equ $7f70

; Interrupt Software priority register 2 
ITC_SPR2: equ $7f71

; Interrupt Software priority register 3 
ITC_SPR3: equ $7f72

; Interrupt Software priority register 4 
ITC_SPR4: equ $7f73

; Interrupt Software priority register 5 
ITC_SPR5: equ $7f74

; Interrupt Software priority register 6 
ITC_SPR6: equ $7f75

; Interrupt Software priority register 7 
ITC_SPR7: equ $7f76

; Interrupt Software priority register 8 
ITC_SPR8: equ $7f77

;#endif ; __STM8L151C__ 
