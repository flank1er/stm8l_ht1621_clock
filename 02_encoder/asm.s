    switch .text
    ; ------------------
    xref _delay_ms
	xdef _tim1_handler
    include "stm8l151c.s"
	
    ; --- TIM4 HANDLER -----
_tim1_handler:
    bres TIM1_SR1,#0        ; clear pending flag (UIF)
    iret

_delay_ms:
    pushw x
    pushw y
start_delay:
    ldw y, #250
loop:
    subw y,#1
    jrne loop
    decw x
    jrne start_delay
    popw y
    popw x
    ret

    end
	