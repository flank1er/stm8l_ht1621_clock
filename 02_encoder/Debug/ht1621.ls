   1                     ; C Compiler for STM8 (COSMIC Software)
   2                     ; Parser V4.12.6 - 16 Dec 2021
   3                     ; Generator (Limited) V4.5.4 - 16 Dec 2021
5087                     .const:	section	.text
5088  0000               _digits:
5089  0000 7d            	dc.b	125
5090  0001 60            	dc.b	96
5091  0002 3e            	dc.b	62
5092  0003 7a            	dc.b	122
5093  0004 63            	dc.b	99
5094  0005 5b            	dc.b	91
5095  0006 5f            	dc.b	95
5096  0007 70            	dc.b	112
5097  0008 7f            	dc.b	127
5098  0009 7b            	dc.b	123
5128                     ; 16 void ht1621_init(void) {
5130                     	switch	.text
5131  0000               _ht1621_init:
5135                     ; 17     ht1621_send_cmd(BIAS);
5137  0000 a652          	ld	a,#82
5138  0002 cd0104        	call	_ht1621_send_cmd
5140                     ; 18     ht1621_send_cmd(RC256);
5142  0005 a630          	ld	a,#48
5143  0007 cd0104        	call	_ht1621_send_cmd
5145                     ; 19     ht1621_send_cmd(SYSDIS);
5147  000a 4f            	clr	a
5148  000b cd0104        	call	_ht1621_send_cmd
5150                     ; 20     ht1621_send_cmd(WDTDIS1);
5152  000e a60a          	ld	a,#10
5153  0010 cd0104        	call	_ht1621_send_cmd
5155                     ; 21     ht1621_send_cmd(SYSEN);
5157  0013 a602          	ld	a,#2
5158  0015 cd0104        	call	_ht1621_send_cmd
5160                     ; 22     ht1621_send_cmd(LCDON);
5162  0018 a606          	ld	a,#6
5163  001a cd0104        	call	_ht1621_send_cmd
5165                     ; 23 }
5168  001d 81            	ret
5192                     ; 25 void ht1621_off() {
5193                     	switch	.text
5194  001e               _ht1621_off:
5198                     ; 26 	ht1621_send_cmd(LCDOFF);
5200  001e a604          	ld	a,#4
5201  0020 cd0104        	call	_ht1621_send_cmd
5203                     ; 27 }
5206  0023 81            	ret
5241                     ; 29 void ht1621_clear(void) {
5242                     	switch	.text
5243  0024               _ht1621_clear:
5245  0024 88            	push	a
5246       00000001      OFST:	set	1
5249                     ; 31 	for(i=0;i<16;i++) {
5251  0025 0f01          	clr	(OFST+0,sp)
5253  0027               L3353:
5254                     ; 32 		ht1621_send_data((i<<1),0x0);
5256  0027 7b01          	ld	a,(OFST+0,sp)
5257  0029 48            	sll	a
5258  002a 5f            	clrw	x
5259  002b 95            	ld	xh,a
5260  002c cd00e0        	call	_ht1621_send_data
5262                     ; 31 	for(i=0;i<16;i++) {
5264  002f 0c01          	inc	(OFST+0,sp)
5268  0031 7b01          	ld	a,(OFST+0,sp)
5269  0033 a110          	cp	a,#16
5270  0035 25f0          	jrult	L3353
5271                     ; 34 }
5274  0037 84            	pop	a
5275  0038 81            	ret
5328                     ; 36 void ht1621_print_num(uint16_t num){
5329                     	switch	.text
5330  0039               _ht1621_print_num:
5332  0039 89            	pushw	x
5333  003a 89            	pushw	x
5334       00000002      OFST:	set	2
5337                     ; 37 	uint8_t pos=0;
5339  003b 0f02          	clr	(OFST+0,sp)
5341  003d               L7653:
5342                     ; 39         uint8_t rm=num % 10;
5344  003d 1e03          	ldw	x,(OFST+1,sp)
5345  003f a60a          	ld	a,#10
5346  0041 62            	div	x,a
5347  0042 5f            	clrw	x
5348  0043 97            	ld	xl,a
5349  0044 01            	rrwa	x,a
5350  0045 6b01          	ld	(OFST-1,sp),a
5351  0047 02            	rlwa	x,a
5353                     ; 40 		ht1621_send_digit(pos++,rm);
5355  0048 7b01          	ld	a,(OFST-1,sp)
5356  004a 97            	ld	xl,a
5357  004b 7b02          	ld	a,(OFST+0,sp)
5358  004d 0c02          	inc	(OFST+0,sp)
5360  004f 95            	ld	xh,a
5361  0050 ad0e          	call	_ht1621_send_digit
5363                     ; 41         num=num/10;
5365  0052 1e03          	ldw	x,(OFST+1,sp)
5366  0054 a60a          	ld	a,#10
5367  0056 62            	div	x,a
5368  0057 1f03          	ldw	(OFST+1,sp),x
5369                     ; 42     } while (num>0);
5371  0059 1e03          	ldw	x,(OFST+1,sp)
5372  005b 26e0          	jrne	L7653
5373                     ; 43 }
5376  005d 5b04          	addw	sp,#4
5377  005f 81            	ret
5422                     ; 45 void ht1621_send_digit(uint8_t pos,uint8_t digit) {
5423                     	switch	.text
5424  0060               _ht1621_send_digit:
5426  0060 89            	pushw	x
5427       00000000      OFST:	set	0
5430                     ; 46 	ht1621_send_data((pos<<1),digits[digit]);
5432  0061 9f            	ld	a,xl
5433  0062 5f            	clrw	x
5434  0063 97            	ld	xl,a
5435  0064 d60000        	ld	a,(_digits,x)
5436  0067 97            	ld	xl,a
5437  0068 7b01          	ld	a,(OFST+1,sp)
5438  006a 48            	sll	a
5439  006b 95            	ld	xh,a
5440  006c ad72          	call	_ht1621_send_data
5442                     ; 47 }
5445  006e 85            	popw	x
5446  006f 81            	ret
5511                     ; 49 void ht1621_print(int num) {
5512                     	switch	.text
5513  0070               _ht1621_print:
5515  0070 89            	pushw	x
5516  0071 5204          	subw	sp,#4
5517       00000004      OFST:	set	4
5520                     ; 50     uint16_t value=get_abs(num);
5522  0073 9c            	rvf
5523  0074 a30000        	cpw	x,#0
5524  0077 2e03          	jrsge	L02
5525  0079 50            	negw	x
5526  007a 2000          	jra	L22
5527  007c               L02:
5528  007c               L22:
5529  007c 1f03          	ldw	(OFST-1,sp),x
5531                     ; 51     uint8_t first=1;
5533  007e a601          	ld	a,#1
5534  0080 6b02          	ld	(OFST-2,sp),a
5536                     ; 53 	if (num <= 0) 
5538  0082 9c            	rvf
5539  0083 1e05          	ldw	x,(OFST+1,sp)
5540  0085 2c02          	jrsgt	L1563
5541                     ; 54 	  ht1621_clear();
5543  0087 ad9b          	call	_ht1621_clear
5545  0089               L1563:
5546                     ; 56 	PB_ODR &= ~(1<<CS);
5548  0089 72195005      	bres	_PB_ODR,#4
5549  008d               L3563:
5550                     ; 58         uint8_t rm=value % 10;
5552  008d 1e03          	ldw	x,(OFST-1,sp)
5553  008f a60a          	ld	a,#10
5554  0091 62            	div	x,a
5555  0092 5f            	clrw	x
5556  0093 97            	ld	xl,a
5557  0094 01            	rrwa	x,a
5558  0095 6b01          	ld	(OFST-3,sp),a
5559  0097 02            	rlwa	x,a
5561                     ; 60 		if (first) {
5563  0098 0d02          	tnz	(OFST-2,sp)
5564  009a 271b          	jreq	L1663
5565                     ; 61 		  ht1621_send(0xa0, 3);
5567  009c aea003        	ldw	x,#40963
5568  009f ad7b          	call	_ht1621_send
5570                     ; 62 		  ht1621_send(0, 6);
5572  00a1 ae0006        	ldw	x,#6
5573  00a4 ad76          	call	_ht1621_send
5575                     ; 63 		  ht1621_send(digits[rm], 8);		 
5577  00a6 7b01          	ld	a,(OFST-3,sp)
5578  00a8 5f            	clrw	x
5579  00a9 97            	ld	xl,a
5580  00aa d60000        	ld	a,(_digits,x)
5581  00ad ae0008        	ldw	x,#8
5582  00b0 95            	ld	xh,a
5583  00b1 ad69          	call	_ht1621_send
5585                     ; 64 		  first=0;
5587  00b3 0f02          	clr	(OFST-2,sp)
5590  00b5 200d          	jra	L3663
5591  00b7               L1663:
5592                     ; 66 		  ht1621_send(digits[rm], 8);		 
5594  00b7 7b01          	ld	a,(OFST-3,sp)
5595  00b9 5f            	clrw	x
5596  00ba 97            	ld	xl,a
5597  00bb d60000        	ld	a,(_digits,x)
5598  00be ae0008        	ldw	x,#8
5599  00c1 95            	ld	xh,a
5600  00c2 ad58          	call	_ht1621_send
5602  00c4               L3663:
5603                     ; 68         value=value/10;
5605  00c4 1e03          	ldw	x,(OFST-1,sp)
5606  00c6 a60a          	ld	a,#10
5607  00c8 62            	div	x,a
5608  00c9 1f03          	ldw	(OFST-1,sp),x
5610                     ; 69     } while (value>0);  
5612  00cb 1e03          	ldw	x,(OFST-1,sp)
5613  00cd 26be          	jrne	L3563
5614                     ; 71 	if (num < 0) 
5616  00cf 9c            	rvf
5617  00d0 1e05          	ldw	x,(OFST+1,sp)
5618  00d2 2e05          	jrsge	L5663
5619                     ; 72 	  ht1621_send(0x2,8);  
5621  00d4 ae0208        	ldw	x,#520
5622  00d7 ad43          	call	_ht1621_send
5624  00d9               L5663:
5625                     ; 75 	PB_ODR |=  (1<<CS);
5627  00d9 72185005      	bset	_PB_ODR,#4
5628                     ; 76 }
5631  00dd 5b06          	addw	sp,#6
5632  00df 81            	ret
5677                     ; 78 void ht1621_send_data(uint8_t adr, uint8_t value) {
5678                     	switch	.text
5679  00e0               _ht1621_send_data:
5681  00e0 89            	pushw	x
5682       00000000      OFST:	set	0
5685                     ; 79 	adr <<=2;
5687  00e1 0801          	sll	(OFST+1,sp)
5688  00e3 0801          	sll	(OFST+1,sp)
5689                     ; 80     PB_ODR &= ~(1<<CS);
5691  00e5 72195005      	bres	_PB_ODR,#4
5692                     ; 81     ht1621_send(0xa0, 3);
5694  00e9 aea003        	ldw	x,#40963
5695  00ec ad2e          	call	_ht1621_send
5697                     ; 82     ht1621_send(adr, 6);
5699  00ee 7b01          	ld	a,(OFST+1,sp)
5700  00f0 ae0006        	ldw	x,#6
5701  00f3 95            	ld	xh,a
5702  00f4 ad26          	call	_ht1621_send
5704                     ; 83     ht1621_send(value, 8);
5706  00f6 7b02          	ld	a,(OFST+2,sp)
5707  00f8 ae0008        	ldw	x,#8
5708  00fb 95            	ld	xh,a
5709  00fc ad1e          	call	_ht1621_send
5711                     ; 84     PB_ODR |=  (1<<CS);
5713  00fe 72185005      	bset	_PB_ODR,#4
5714                     ; 85 }
5717  0102 85            	popw	x
5718  0103 81            	ret
5754                     ; 87 void ht1621_send_cmd(uint8_t cmd) {
5755                     	switch	.text
5756  0104               _ht1621_send_cmd:
5758  0104 88            	push	a
5759       00000000      OFST:	set	0
5762                     ; 88 	PB_ODR &= ~(1<<CS);
5764  0105 72195005      	bres	_PB_ODR,#4
5765                     ; 89 	ht1621_send(0x80,4);
5767  0109 ae8004        	ldw	x,#32772
5768  010c ad0e          	call	_ht1621_send
5770                     ; 90 	ht1621_send(cmd,8);
5772  010e 7b01          	ld	a,(OFST+1,sp)
5773  0110 ae0008        	ldw	x,#8
5774  0113 95            	ld	xh,a
5775  0114 ad06          	call	_ht1621_send
5777                     ; 91 	PB_ODR |=  (1<<CS);
5779  0116 72185005      	bset	_PB_ODR,#4
5780                     ; 92 }
5783  011a 84            	pop	a
5784  011b 81            	ret
5837                     ; 94 void ht1621_send(uint8_t data, uint8_t len) {
5838                     	switch	.text
5839  011c               _ht1621_send:
5841  011c 89            	pushw	x
5842  011d 88            	push	a
5843       00000001      OFST:	set	1
5846                     ; 96 	for(i=0;i<len;i++) {
5848  011e 0f01          	clr	(OFST+0,sp)
5851  0120 2021          	jra	L1673
5852  0122               L5573:
5853                     ; 97         PB_ODR=(data & 0x80) ? PB_ODR | (1<<DATA) : PB_ODR & ~(1<<DATA);
5855  0122 7b02          	ld	a,(OFST+1,sp)
5856  0124 a580          	bcp	a,#128
5857  0126 2707          	jreq	L23
5858  0128 c65005        	ld	a,_PB_ODR
5859  012b aa40          	or	a,#64
5860  012d 2005          	jra	L43
5861  012f               L23:
5862  012f c65005        	ld	a,_PB_ODR
5863  0132 a4bf          	and	a,#191
5864  0134               L43:
5865  0134 c75005        	ld	_PB_ODR,a
5866                     ; 98 		PB_ODR &= ~(1<<WR);
5868  0137 721b5005      	bres	_PB_ODR,#5
5869                     ; 99 		PB_ODR |= (1<<WR);
5871  013b 721a5005      	bset	_PB_ODR,#5
5872                     ; 100         data=(data<<1);
5874  013f 0802          	sll	(OFST+1,sp)
5875                     ; 96 	for(i=0;i<len;i++) {
5877  0141 0c01          	inc	(OFST+0,sp)
5879  0143               L1673:
5882  0143 7b01          	ld	a,(OFST+0,sp)
5883  0145 1103          	cp	a,(OFST+2,sp)
5884  0147 25d9          	jrult	L5573
5885                     ; 102 }
5888  0149 5b03          	addw	sp,#3
5889  014b 81            	ret
5914                     	xdef	_ht1621_off
5915                     	xdef	_ht1621_send_data
5916                     	xdef	_ht1621_send
5917                     	xdef	_ht1621_send_cmd
5918                     	xdef	_digits
5919                     	xdef	_ht1621_print_num
5920                     	xdef	_ht1621_send_digit
5921                     	xdef	_ht1621_init
5922                     	xdef	_ht1621_clear
5923                     	xdef	_ht1621_print
5942                     	end
