#ifndef __MAIN_H__
#define __MAIN_H__

#define LED 0       // PB0
#define ENC_L 0		// PB0
#define ENC_R 2		// PB2

#define EnableInterrupt()   _asm("rim\n");
#define DisableInterrupt()  _asm("sim\n");
#define wfi()               _asm("wfi\n");

#endif
