#include <stdint.h>
#include "iostm8l152x.h"
#include "ht1621.h"
#include "main.h"

//extern void delay_ms(uint16_t value);

main()
{
  	int count=0;
	uint8_t tick=0;
    // ------- CLK Setup ---------------
    CLK_DIVR = 4;         	// SYSCLK=1MHz
	CLK_PCKENR2 |= 0x02;    // Enable TIM1
	CLK_PCKENR1 |= 0x01;	// Enable TIM2		
	
	// parking GPIO
    // to ground Port A
    PA_DDR = 0xFF; PA_CR1 = 0xFF; PA_ODR = 0;
    // to ground Port B
    PB_DDR = 0xFF; PB_CR1 = 0xFF; PB_ODR = 0;
    // to ground Port C
    PC_DDR = 0xFF; PC_CR1 = 0xFF; PC_ODR = 0;
    // to ground Port D
    //PD_DDR = 0xFF; PD_CR1 = 0xFF; PD_ODR = 0;
    // to ground Port E
    PE_DDR = 0xFF; PE_CR1 = 0xFF; PE_ODR = 0;
    // to ground Port F
    PF_DDR = 0xFF; PF_CR1 = 0xFF; PF_ODR = 0;
	
    // ------- GPIO setup --------------
	// Soft SPI GPIO Setup
	PB_DDR = ((1<<CS) | (1<<WR) | (1<<DATA)); // Push-Pull Mode
    PB_CR1 = ((1<<CS) | (1<<WR) | (1<<DATA)); //
    PB_CR2 = ((1<<CS) | (1<<WR) | (1<<DATA)); // Speed up 10 MHz
	// Encoder
	PB_DDR &= ~((1<<ENC_L) | (1<<ENC_R));
    PB_CR1 |=  ((1<<ENC_L) | (1<<ENC_R));

	// ====== TIM1 Setup =========
    TIM1_SR1   = 0x0;                       // Clear Pending Bit
    TIM1_CR1   = 0x0;                       // Clear TIM1_CR1
    TIM1_CR2   = 0x0;                       // Clear TIM1_CR2
    TIM1_PSCRH = 0x0;  TIM1_PSCRL = 3;     // Prescaler = 64
    TIM1_ARRH  = 0x7a; TIM1_ARRL  = 0x12;   // (10^6)/prescaler(=64) =31250 -> 0x7A12 -> freq Timer IRQ =1Hz
    TIM1_IER   = 0x01;                      // set UIE flag, enable interrupt
    TIM1_CR1  |= 0x01;                      // set CEN flag, start timer
	// ======= TIM2 Setup =========
	TIM2_CCER1|= 0x22; 	// CC2P,CC1P: Capture/compare 2 output polarity
	TIM2_CCMR1|= 0x01; 	// CC1 channel is configured as input, IC1 is mapped on TI1FP1
	TIM2_CCMR2|= 0x01;	// CC2 channel is configured as input, IC2 is mapped on TI2FP2
	TIM2_SMCR |= 0x01; 	// encoder mode 1
	TIM2_ARRH=1;		// max value counter =500
	TIM2_ARRL=0xf4;		// max value counter =500
	TIM2_CNTRH=0;		// clear counter
	TIM2_CNTRL=0;		// clear counter
	TIM2_CR1|=0x1; 		// enable counter
    // main loop
    ht1621_init();	
    ht1621_clear();
	ht1621_print(0);
    for(;;) {
		if (tick == 20 ) {
		  //PB_ODR ^=(1<<LED);
		  tick=0;		  
		} else {
		  tick++;
		}	  
				
		count=(uint16_t)TIM2_CNTRH;
		count = count <<8;
		count |=(uint16_t)TIM2_CNTRL;
		count = count >>1;
		if (count >= 125) {
		  count = (count - 250);
		}

		ht1621_print(count);

        //delay_ms(50);
		_asm("wfi");
    }
}
